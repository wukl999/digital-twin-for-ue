package com.vf.cloud.rendering.server.dispatch.handler;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.vf.cloud.rendering.common.util.DispatchUtil;
import com.vf.cloud.rendering.common.util.OSUtil;
import io.netty.channel.*;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Handler extends SimpleChannelInboundHandler<Object> {

	private WebSocketClientHandshaker handshaker;
	private ChannelPromise handshakeFuture;

	@Override
	public void channelActive(ChannelHandlerContext channelHandlerContext) {
		handlerAdded(channelHandlerContext);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		log.error(ctx.channel()+"异常:", cause);
		ctx.close();
	}

	public void setHandshaker(WebSocketClientHandshaker handshaker) {
		this.handshaker = handshaker;
	}

	public void handlerAdded(ChannelHandlerContext ctx) {
		this.handshakeFuture = ctx.newPromise();
	}

	public ChannelFuture handshakeFuture() {
		return this.handshakeFuture;
	}

	protected void channelRead0(ChannelHandlerContext ctx, Object o) throws Exception {
		// 握手协议返回，设置结束握手
		if (!this.handshaker.isHandshakeComplete()) {
			FullHttpResponse response = (FullHttpResponse) o;
			this.handshaker.finishHandshake(ctx.channel(), response);
			this.handshakeFuture.setSuccess();
			return;
		} else if (o instanceof TextWebSocketFrame) {

			TextWebSocketFrame textWebSocketFrame = (TextWebSocketFrame) o;
			JSONObject rawMsg = null;
			try {
				rawMsg = JSONObject.parseObject(textWebSocketFrame.text());
			} catch (Exception e) {
				e.printStackTrace();
				log.error(String.format("[%s]>> cannot parse Pass message:%s", 1008, e));
				ctx.close();
			}
			if (rawMsg == null) {
				return;
			}

			if (!rawMsg.containsKey("type")) {
				log.error("Invalid Pass message:" + textWebSocketFrame.text());
				return;
			}

			String type = rawMsg.getString("type");
			if (StrKit.equals(type, "pong")) {
				Map<String, Object> monitorConfig = new HashMap<String, Object>();
				monitorConfig.put("type", "onRenderingMonitor");
				monitorConfig.put("data", OSUtil.getSysInfo());
				monitorConfig.put("time", System.currentTimeMillis());
				DispatchUtil.send(ctx, JsonKit.toJson(monitorConfig));
				return;
			}

			if (!rawMsg.containsKey("event")) {
				return;
			}else {
				log.error("unsupported Pass message type:%s", type);
				ctx.close();
			}

		}
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		if (evt instanceof IdleStateEvent) {
			IdleStateEvent event = (IdleStateEvent) evt;
			if (event.state().equals(IdleState.READER_IDLE)) {
				DispatchUtil.sendPing(ctx, System.currentTimeMillis());
			} else if (event.state().equals(IdleState.WRITER_IDLE)) {
				DispatchUtil.sendPing(ctx, System.currentTimeMillis());
			} else if (event.state().equals(IdleState.ALL_IDLE)) {
				DispatchUtil.sendPing(ctx, System.currentTimeMillis());
			}
		}
	}
	
	//SysInfo

}