package com.vf.cloud.rendering.common.constant;

public interface SecurityConstant {
	
	String PROJECT_AUTH_TOKEN = "micros_auth:token:";
	
    /**
     * Authorization参数头
     */
    String HEADER = "Authorization";

    /**
     * token分割
     */
    String TOKEN_SPLIT = "Bearer ";

    /**
     * token中自定义权限标识
     */
    String AUTHORITIES = "AUTHORITIES_";
    
    /**
     * token失效时间
     */
    Integer TOKEN_EXPIRATION_TIME = 30*60*100000;

    /**
     * Token 发行人
     */
    String TOKEN_ISSUER = "micros";

    /**
     * JWT签名加密key
     */
    String TOKEN_SECRET = "micros";

    /**
     * 刷新Token时间
     */
    Integer TOKEN_REFRESH_EXPTIME = 720;
    
    String TOKEN_KEY_PREFIX="MICROS_KEY_";
    String TOKEN_AUTH_PREFIX="MICROS_AUTH_";
    
	/**
	 * 默认登录URL
	 */
	String OAUTH_TOKEN_URL = "/login";

	/**
	 * grant_type
	 */
	String REFRESH_TOKEN = "refresh_token";

	/**
	 * 内部
	 */
	String FROM_IN = "Y";

	/**
	 * 标志
	 */
	String FROM = "from";
	
	/**
	 * 验证码
	 */
	String  VERIFY_CODE_PREFIX="CODE_";
	
	Integer ERIFY_CODE_EXPIRATION_TIME = 5*60*100000;
}
