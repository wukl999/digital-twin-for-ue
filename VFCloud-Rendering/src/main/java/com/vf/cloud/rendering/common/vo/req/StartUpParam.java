package com.vf.cloud.rendering.common.vo.req;

public class StartUpParam {
	
	private String sid;
	private String eio;
	private int width;
	private int height;
	private String uuid;
	private int index;
	private int block;
	private boolean debug;
	private String exeFilePath;
	private String parameter;
	
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	
	public String getEio() {
		return eio;
	}
	public void setEio(String eio) {
		this.eio = eio;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getBlock() {
		return block;
	}
	public void setBlock(int block) {
		this.block = block;
	}
	public boolean isDebug() {
		return debug;
	}
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	public String getExeFilePath() {
		return exeFilePath;
	}
	public void setExeFilePath(String exeFilePath) {
		this.exeFilePath = exeFilePath;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
}
