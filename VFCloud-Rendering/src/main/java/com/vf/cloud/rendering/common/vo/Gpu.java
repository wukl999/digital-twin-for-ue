package com.vf.cloud.rendering.common.vo;

public class Gpu {

	private String mac;
	private String name;
	private String uuid;
	private String boardId;
	private String vbiosVersion;
	private double memoryTotal;
	private String memoryReserved;
	private String memoryUsed;
	private String memoryFree;
	
	private String gpuUtil;
	private String memoryUtil;
	private String encoderUtil;
	private String decoderUtil;
	
	private int moduleId;
	private int limit;
	private int index;
	private int used;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getVbiosVersion() {
		return vbiosVersion;
	}

	public void setVbiosVersion(String vbiosVersion) {
		this.vbiosVersion = vbiosVersion;
	}

	public double getMemoryTotal() {
		return memoryTotal;
	}

	public void setMemoryTotal(double memoryTotal) {
		this.memoryTotal = memoryTotal;
	}

	public String getMemoryReserved() {
		return memoryReserved;
	}

	public void setMemoryReserved(String memoryReserved) {
		this.memoryReserved = memoryReserved;
	}

	public String getMemoryUsed() {
		return memoryUsed;
	}

	public void setMemoryUsed(String memoryUsed) {
		this.memoryUsed = memoryUsed;
	}

	public String getMemoryFree() {
		return memoryFree;
	}

	public void setMemoryFree(String memoryFree) {
		this.memoryFree = memoryFree;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getUsed() {
		return used;
	}

	public void setUsed(int used) {
		this.used = used;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getGpuUtil() {
		return gpuUtil;
	}

	public void setGpuUtil(String gpuUtil) {
		this.gpuUtil = gpuUtil;
	}

	public String getMemoryUtil() {
		return memoryUtil;
	}

	public void setMemoryUtil(String memoryUtil) {
		this.memoryUtil = memoryUtil;
	}

	public String getEncoderUtil() {
		return encoderUtil;
	}

	public void setEncoderUtil(String encoderUtil) {
		this.encoderUtil = encoderUtil;
	}

	public String getDecoderUtil() {
		return decoderUtil;
	}

	public void setDecoderUtil(String decoderUtil) {
		this.decoderUtil = decoderUtil;
	}
	

}
