package com.vf.cloud.rendering.server.signalling;

import com.jfinal.kit.StrKit;
import com.vf.cloud.rendering.common.constant.Cache;
import com.vf.cloud.rendering.common.vo.Signalling;
import com.vf.cloud.rendering.server.signalling.server.Server;

public class SignallingServer {

	private static volatile SignallingServer INSYANCE;

	private Server signallingServer;

	private SignallingServer() {
		signallingServer = new Server();
		signallingServer.reconnect();
	}

	public static SignallingServer getInstance() {
		if (null == INSYANCE) {
			synchronized (SignallingServer.class) {
				if (null == INSYANCE) {
					INSYANCE = new SignallingServer();
				}
			}
		}
		return INSYANCE;
	}

	public void run(Signalling signalling) {
		Cache.signalling = signalling;
	}

	public void run() {
		if (signallingServer != null && Cache.signalling != null) {
			signallingServer.run();
		}
	}

	public void restart() {
	}

	public void stop() {
		if (signallingServer != null) {
			signallingServer.stop();
		}
	}

	public void destroy() {
		if (signallingServer != null) {
			signallingServer.destroy();
		}
	}

	public int getStatus() {
		if (signallingServer != null) {
			return signallingServer.getStatus();
		}
		return -1;
	}

	public boolean verify(Signalling signalling) {
		if (signalling == null) {
			return false;
		}

		if (StrKit.isBlank(signalling.getIp())) {
			return false;
		}
		if (signalling.getPort() == 0) {
			return false;
		}

		if (StrKit.isBlank(signalling.getInnerIp())) {
			return false;
		}
		if (signalling.getInnerPort() == 0) {
			return false;
		}
		return true;
	}

}
