package com.vf.cloud.rendering.common.vo;

public class Signalling {

	private String ip;
	private int port;
	private String innerIp;
	private int innerPort;
	private String sslEnable;
	private String sslKeyAbsolutePath;
	private String sslCertAbsolutePath;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getInnerIp() {
		return innerIp;
	}

	public void setInnerIp(String innerIp) {
		this.innerIp = innerIp;
	}

	public int getInnerPort() {
		return innerPort;
	}

	public void setInnerPort(int innerPort) {
		this.innerPort = innerPort;
	}


	public String getSslEnable() {
		return sslEnable;
	}

	public void setSslEnable(String sslEnable) {
		this.sslEnable = sslEnable;
	}

	public String getSslKeyAbsolutePath() {
		return sslKeyAbsolutePath;
	}

	public void setSslKeyAbsolutePath(String sslKeyAbsolutePath) {
		this.sslKeyAbsolutePath = sslKeyAbsolutePath;
	}

	public String getSslCertAbsolutePath() {
		return sslCertAbsolutePath;
	}

	public void setSslCertAbsolutePath(String sslCertAbsolutePath) {
		this.sslCertAbsolutePath = sslCertAbsolutePath;
	}

}
