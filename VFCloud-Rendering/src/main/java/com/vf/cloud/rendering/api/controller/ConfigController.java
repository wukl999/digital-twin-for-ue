package com.vf.cloud.rendering.api.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jfinal.kit.JsonKit;
import com.vf.cloud.common.util.R;
import com.vf.cloud.rendering.common.constant.Cache;
import com.vf.cloud.rendering.common.util.FileUtil;
import com.vf.cloud.rendering.common.vo.Config;

@RestController
public class ConfigController {

	@RequestMapping(value = "/config", method = RequestMethod.POST)
	public R<String> config(@RequestBody Config config) {

		if (!Cache.isRunning) {
			return R.failed("渲染服务暂未启动.");
		}

		if (FileUtil.writeConfig(JsonKit.toJson(config))) {
			return R.ok();
		}
		return R.failed();
	}

}
