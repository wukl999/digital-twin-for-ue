package com.vf.cloud.rendering.common.vo;

import java.util.List;

public class Renderer {
	
	private List<Gpu> gpus;

	public List<Gpu> getGpus() {
		return gpus;
	}

	public void setGpus(List<Gpu> gpus) {
		this.gpus = gpus;
	}

}
