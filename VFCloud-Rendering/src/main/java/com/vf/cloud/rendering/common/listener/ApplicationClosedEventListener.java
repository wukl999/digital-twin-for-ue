package com.vf.cloud.rendering.common.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;
import com.vf.cloud.common.pool.ProssPool;
import com.vf.cloud.rendering.server.dispatch.DispatchServer;
import com.vf.cloud.rendering.server.relay.RelayServer;
import com.vf.cloud.rendering.server.signalling.SignallingServer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ApplicationClosedEventListener implements ApplicationListener<ContextClosedEvent>{
	@Override
	public void onApplicationEvent(ContextClosedEvent event) {
		
		log.info("销毁信令服务器.");
		SignallingServer.getInstance().destroy();
		
		DispatchServer.getInstance().destroy();
		
		RelayServer.getInstance().destroy();
		
		ProssPool.stop();
		
	}


}
