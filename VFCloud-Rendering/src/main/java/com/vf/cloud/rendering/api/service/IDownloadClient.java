package com.vf.cloud.rendering.api.service;

public interface IDownloadClient {

	public void download(String sceneId);

}
