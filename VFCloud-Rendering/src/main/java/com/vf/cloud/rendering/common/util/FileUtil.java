package com.vf.cloud.rendering.common.util;

import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

import com.vf.cloud.rendering.common.constant.SoftPath;


public class FileUtil {

	/**
	 * 创建文件顺便创建父目录
	 *
	 * @param path 文件字符串路径例如d:/fulld/why/ass/a/asd
	 */
	public static void createFile(String path) {
		createFile(new File(path));
	}

	/**
	 * 创建文件顺便创建父目录
	 *
	 * @param file file类
	 */
	private static void createFile(File file) {
		if (file.exists() && file.isFile()) {
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		File parentFile = file.getParentFile();
		if (parentFile.exists()) {
			if (parentFile.isFile()) {
				parentFile.delete();
				parentFile.mkdirs();
			}
		} else {
			parentFile.mkdirs();
		}
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 创建文件夹顺便创建父目录
	 *
	 * @param path 文件夹的路径字符串例如d:/fulld/why/ass/a/asd/
	 * @return 如果本来就存在或者创建成功，那么就返回true
	 */
	public static void mkdirs(String path) {
		mkdirs(new File(path));
	}

	/**
	 * 创建文件夹顺便创建父目录
	 *
	 * @param file file类
	 */
	public static void mkdirs(File file) {
		if (file.exists() && file.isDirectory()) {
			return;
		}
		if (file.exists()) {
			file.delete();
			file.mkdirs();
		} else {
			file.mkdirs();
		}
	}

	public static String readConfig(){
		FileInputStream fis = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		try {
			fis = new FileInputStream(String.format("%s/RenderingServer/config/server.ini", SoftPath.path));
			isr = new InputStreamReader(fis, "UTF-8");
			br = new BufferedReader(isr);
			String line = "";
			StringBuffer sb = new StringBuffer();
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			return sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
				}
			if (isr != null)
				try {
					isr.close();
				} catch (IOException e) {
				}
			if (fis != null)
				try {
					fis.close();
				} catch (IOException e) {
				}
		}
		return "";
	}

	/**
	 * 写入配置文件
	 */
	public static boolean writeConfig( String text) {
		BufferedWriter out = null;
		try {
			File f = new File(String.format("%s/RenderingServer/config/server.ini", SoftPath.path));
			if (f.exists()) {
				f.delete();
			}
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, true)));
			out.write(text);
			try {
				out.close();
			} catch (IOException e) {
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
			}
		}
		return false;
	}
	
	/**
	 *   
	 * @param text
	 * @return
	 */
	public static boolean writeData( String text) {
		BufferedWriter out = null;
		try {
			File f = new File(String.format("%s/RenderingServer/config/server.ini", SoftPath.path));
			if (f.exists()) {
				f.delete();
			}
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, true)));
			out.write(text);
			try {
				out.close();
			} catch (IOException e) {
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
			}
		}
		return false;
	}

    /**
     * 删除目录
     * @param rootDir 目录
     */
    public static void deleteDir(String rootDir) throws IOException {
        Files.walkFileTree(Paths.get(rootDir), new FileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.deleteIfExists(Paths.get(file.getParent() + File.separator + file.getFileName()));
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                // 删除目录
                Files.deleteIfExists(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

	
	
	public static void main(String[] args) throws Exception {
	}
}
