package com.vf.cloud.rendering.common.scheduled;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.vf.cloud.rendering.common.factory.UEFactory;


@Component
@EnableScheduling
public class GPUScheduled {
	
    @Scheduled(fixedRate = 5000)
    @Async 
    public void scheduled() throws Exception {
    	UEFactory.getInstance().check();
    }

}
