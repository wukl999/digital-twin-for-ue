package com.vf.cloud.rendering.common.util;

import java.math.BigDecimal;
import java.util.List;

import com.jfinal.kit.JsonKit;
import com.vf.cloud.rendering.common.sysinfo.MonitorInfo;
import com.vf.cloud.rendering.common.sysinfo.MonitorService;
import com.vf.cloud.rendering.common.vo.Gpu;
import com.vf.cloud.rendering.common.vo.SysInfo;

public class OSUtil {
	public static void main(String[] args) {
		SysInfo info=getSysInfo();
		System.out.println(JsonKit.toJson(info));
	}
	
	public static SysInfo getSysInfo() {
		
		try {
			SysInfo info=new SysInfo();
			
	        MonitorService service = new MonitorService();
	        MonitorInfo monitorInfo = service.getMonitorInfoBean();
//	         System.out.println("JVM可使用内存=" + monitorInfo.getTotalMemory() +
//	         "MB");
//	         System.out.println("JVM剩余内存=" + monitorInfo.getFreeMemory() + "MB");
//	         System.out.println("JVM最大可使用内存=" + monitorInfo.getMaxMemory() +
//	         "MB");
//
//	        System.out.println("操作系统=" + monitorInfo.getOsName());
//	        System.out.println("核心数=" + monitorInfo.getProcessors());
//	        System.out.println("总的物理内存=" + monitorInfo.getTotalMemorySize() + "GB");
//	        System.out.println("剩余的物理内存=" + monitorInfo.getFreeMemorySize() + "GB");
//	        System.out
//	                .println("已使用的物理内存=" + monitorInfo.getUsedMemorySize() + "GB");
	        
	        info.setOsName(monitorInfo.getOsName());
	        info.setProcessors(monitorInfo.getProcessors());
	        info.setMemoryTotal(monitorInfo.getTotalMemorySize());
	        info.setMemoryUsed(monitorInfo.getUsedMemorySize());
	        
	    	String gpuModel="";
	    	int gpuNum = 0;
	    	double gpuMemoryTotal = 0;
	    	double gpuMemoryUsed = 0;
	        
	        List<Gpu> list=GpuUtil.getGPUs();
	        for(Gpu g:list) {
	        	if(!gpuModel.contains(g.getName())) {
	        		gpuModel+=g.getName();
	        	}
	        	gpuMemoryTotal+=g.getMemoryTotal();
	        	gpuMemoryUsed+=Double.parseDouble(g.getMemoryUsed().replace("MiB", ""));
	        	gpuNum+=1;
	        }
	        
	        
	        info.setGpuModel(gpuModel);
	        info.setGpuMemoryTotal(getIntValue(gpuMemoryTotal/1024));
	        info.setGpuMemoryUsed(getIntValue(gpuMemoryUsed/1024));
	        info.setGpuNum(gpuNum);
	        info.setMemoryUsedPercentage((getDoubleValue(monitorInfo.getUsedMemorySize()/monitorInfo.getTotalMemorySize())*100));
	       // info.setGpuMemoryUsedPercentage((getIntValue(info.getGpuMemoryUsed()/info.getGpuMemoryTotal())*100));
	        return info;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
    private static double getIntValue(double d) {
        return new BigDecimal(d).setScale(1, BigDecimal.ROUND_HALF_UP)
                .doubleValue();
    }
    
    private static double getDoubleValue(double d) {
        return new BigDecimal(d).setScale(2, BigDecimal.ROUND_HALF_UP)
                .doubleValue();
    }
	

}
