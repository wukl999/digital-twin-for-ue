package com.vf.cloud.rendering.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vf.cloud.common.util.R;
import com.vf.cloud.rendering.api.service.IDownloadClient;

@RestController
public class DownloadClient {
	
	
	@Autowired
	private IDownloadClient iDownloadClient;
	

	@RequestMapping(value = "/downloadFile", method = RequestMethod.POST)
	public R<String> downloadFile(@RequestBody String sceneId) throws Exception {
		iDownloadClient.download(sceneId);
		return R.ok();
	}

	
}