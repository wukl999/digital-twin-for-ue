package com.vf.cloud.rendering.common.listener;

import java.io.File;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.vf.cloud.rendering.common.constant.Cache;
import com.vf.cloud.rendering.common.constant.SoftPath;
import com.vf.cloud.rendering.common.util.FileUtil;
import com.vf.cloud.rendering.common.vo.Dispatch;
import com.vf.cloud.rendering.common.vo.Local;
import com.vf.cloud.rendering.common.vo.Relay;
import com.vf.cloud.rendering.common.vo.Signalling;
import com.vf.cloud.rendering.server.dispatch.DispatchServer;
import com.vf.cloud.rendering.server.relay.RelayServer;
import com.vf.cloud.rendering.server.signalling.SignallingServer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ApplicationReadyEventListener implements ApplicationListener<ApplicationReadyEvent> {

	@Override
	public void onApplicationEvent(ApplicationReadyEvent contextReadyEvent) {

		String configPath = String.format("%s/RenderingServer/config/server.ini", SoftPath.path);
		File f = new File(configPath);
		if (!f.exists()) {
			log.warn("渲染服务未设置配置信息.");
			Cache.isRunning = true;
			return;
		}

		String text = FileUtil.readConfig();
		if (StrKit.isBlank(text)) {
			log.warn("渲染服务未设置配置信息.");
			Cache.isRunning = true;
			return;
		}

		try {
			JSONObject object = JSONObject.parseObject(text);
			if (object == null) {
				log.warn("渲染服务未设置配置信息.");
				System.exit(0);
				return;
			}

			if (!object.containsKey("local")) {
				log.warn("渲染服务未设置配置信息.");
				System.exit(0);
				return;
			}

			if (object.containsKey("local")) {
				Cache.local = JSONObject.parseObject(object.getString("local"), Local.class);
			}

			if (Cache.local == null || StrKit.isBlank(Cache.local.getIp()) || StrKit.isBlank(Cache.local.getMac())
					|| StrKit.isBlank(Cache.local.getProjectPath())) {
				log.warn("请配置local信息.");
				System.exit(0);
				return;
			}

			if (object.containsKey("signalling")) {
				Cache.signalling = JSONObject.parseObject(object.getString("signalling"), Signalling.class);
				SignallingServer.getInstance().stop();
			}

			if (object.containsKey("dispatch")) {
				Cache.dispatch = JSONObject.parseObject(object.getString("dispatch"), Dispatch.class);
				DispatchServer.getInstance().stop();
			} else {
				log.warn("未配置调度接入点...");
			}

			if (object.containsKey("relay")) {
				Cache.relay = JSONObject.parseObject(object.getString("relay"), Relay.class);
				RelayServer.getInstance().restart();
			}

			if (object.containsKey("projectPath")) {
				Cache.projectPath = object.getString("projectPath");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			Cache.isRunning = true;
		}
	}

}
