package com.vf.cloud.rendering.common.vo;


public class Config {

	public Local local;
	public Dispatch dispatch;
	public Signalling signalling;
	public Relay relay;
	public Local getLocal() {
		return local;
	}
	public void setLocal(Local local) {
		this.local = local;
	}
	public Dispatch getDispatch() {
		return dispatch;
	}
	public void setDispatch(Dispatch dispatch) {
		this.dispatch = dispatch;
	}
	public Signalling getSignalling() {
		return signalling;
	}
	public void setSignalling(Signalling signalling) {
		this.signalling = signalling;
	}
	public Relay getRelay() {
		return relay;
	}
	public void setRelay(Relay relay) {
		this.relay = relay;
	}
	

}
