package com.vf.cloud.rendering.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IpUtil {

	/** * 判断是否为合法IP * @return the ip */
	public static boolean isIp(String ipAddress) {
		String ip = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";
		Pattern pattern = Pattern.compile(ip);
		Matcher matcher = pattern.matcher(ipAddress);
		return matcher.matches();
	}

	public static boolean isPort(String port) {
		// 端口号验证 1 ~ 65535
		String regex = "^([1-9]|[1-9]\\d{1,3}|[1-6][0-5][0-5][0-3][0-5])$";
		boolean flag = Pattern.matches(regex, port);
		return flag;
	}

}
