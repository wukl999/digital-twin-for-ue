package com.vf.cloud.rendering.common.vo;

public class SysInfo {

	private String osName;
	private int processors;
	private double memoryTotal;
	private double memoryUsed;
	private String gpuModel;
	private int gpuNum;
	private double gpuMemoryTotal;
	private double gpuMemoryUsed;
	
	
	private double memoryUsedPercentage;
	private double gpuMemoryUsedPercentage;
	
	
	

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public int getProcessors() {
		return processors;
	}

	public void setProcessors(int processors) {
		this.processors = processors;
	}

	public double getMemoryTotal() {
		return memoryTotal;
	}

	public void setMemoryTotal(double memoryTotal) {
		this.memoryTotal = memoryTotal;
	}

	public double getMemoryUsed() {
		return memoryUsed;
	}

	public void setMemoryUsed(double memoryUsed) {
		this.memoryUsed = memoryUsed;
	}

	public String getGpuModel() {
		return gpuModel;
	}

	public void setGpuModel(String gpuModel) {
		this.gpuModel = gpuModel;
	}

	public int getGpuNum() {
		return gpuNum;
	}

	public void setGpuNum(int gpuNum) {
		this.gpuNum = gpuNum;
	}

	public double getGpuMemoryTotal() {
		return gpuMemoryTotal;
	}

	public void setGpuMemoryTotal(double gpuMemoryTotal) {
		this.gpuMemoryTotal = gpuMemoryTotal;
	}

	public double getGpuMemoryUsed() {
		return gpuMemoryUsed;
	}

	public void setGpuMemoryUsed(double gpuMemoryUsed) {
		this.gpuMemoryUsed = gpuMemoryUsed;
	}

	public double getMemoryUsedPercentage() {
		return memoryUsedPercentage;
	}

	public void setMemoryUsedPercentage(double memoryUsedPercentage) {
		this.memoryUsedPercentage = memoryUsedPercentage;
	}

	public double getGpuMemoryUsedPercentage() {
		return gpuMemoryUsedPercentage;
	}

	public void setGpuMemoryUsedPercentage(double gpuMemoryUsedPercentage) {
		this.gpuMemoryUsedPercentage = gpuMemoryUsedPercentage;
	}


}
