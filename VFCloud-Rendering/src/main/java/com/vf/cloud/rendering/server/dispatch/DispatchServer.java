package com.vf.cloud.rendering.server.dispatch;

import java.util.HashMap;
import java.util.Map;
import com.jfinal.kit.StrKit;
import com.vf.cloud.rendering.common.constant.Cache;
import com.vf.cloud.rendering.server.dispatch.server.Server;

public class DispatchServer {

	public static void main(String[] args) {
		DispatchServer.getInstance().run();
	}

	private static volatile DispatchServer INSYANCE;

	private Server server;

	private DispatchServer() {
		server = new Server();
		server.reconnect();
	}

	public static DispatchServer getInstance() {
		if (null == INSYANCE) {
			synchronized (DispatchServer.class) {
				if (null == INSYANCE) {
					INSYANCE = new DispatchServer();
				}
			}
		}
		return INSYANCE;
	}

	public void run() {
		if (server != null) {
			server.run();
		}
	}

	public void restart() {
		if (server != null) {
			server.restart();
		}
	}

	public void stop() {
		if (server != null) {
			server.stop();
		}
	}

	public void destroy() {
		if (server != null) {
			server.destroy();
		}
	}

	public int getStatus() {
		if (server != null) {
			return server.getStatus();
		}
		return -1;
	}

	public boolean verify() {
		if (Cache.dispatch == null) {
			return false;
		}
		if (StrKit.isBlank(Cache.dispatch.getIp())) {
			return false;
		}
		if (StrKit.isBlank(Cache.dispatch.getPort())) {
			return false;
		}
		if (StrKit.isBlank(Cache.dispatch.getSecretKey())) {
			return false;
		}
		return true;
	}

	public void send(Map<String, Object> message) {
		server.send(message);
	}
	
	public void sendStreamerConnected(String EIO) {
		Map<String, Object> streamerConnected = new HashMap<String, Object>();
		streamerConnected.put("type", "onStreamerConnected");
		streamerConnected.put("data", Cache.local);
		streamerConnected.put("time", System.currentTimeMillis());
	}
	
	
	public void onStreamerDisconnected(String EIO) {
		Map<String, Object> streamerDisconnected = new HashMap<String, Object>();
		streamerDisconnected.put("type", "onStreamerDisconnected");
		streamerDisconnected.put("data", Cache.local);
		streamerDisconnected.put("time", System.currentTimeMillis());
	}

}
