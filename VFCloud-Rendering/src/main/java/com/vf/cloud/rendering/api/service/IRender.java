package com.vf.cloud.rendering.api.service;

import java.util.List;

import com.vf.cloud.common.util.R;
import com.vf.cloud.rendering.common.vo.Dispatch;
import com.vf.cloud.rendering.common.vo.Gpu;
import com.vf.cloud.rendering.common.vo.Order;
import com.vf.cloud.rendering.common.vo.Relay;
import com.vf.cloud.rendering.common.vo.Renderer;
import com.vf.cloud.rendering.common.vo.Server;
import com.vf.cloud.rendering.common.vo.Signalling;
import com.vf.cloud.rendering.common.vo.req.AnyInfoParam;
import com.vf.cloud.rendering.common.vo.req.StartUpParam;

public interface IRender {
	
	public R<Order> AnyOrder(StartUpParam param);
	
	public R<Renderer> AnyInfo(AnyInfoParam param);

	public R<Dispatch> AnyDispatchInfo();

	public R<Dispatch> AnyDispatchUpdate(Dispatch param);

	public R<List<Server>> AnyServerList();

	public R<Signalling> AnySignallingInfo();

	public R<Signalling> AnySignallingUpdate(Signalling param);

	public R<Relay> AnyRelayInfo();

	public R<Relay> AnyRelayUpdate(Relay param);

	public R<List<Gpu>> AnyGpuList();

	public R<Order> startUp(StartUpParam param);

}
