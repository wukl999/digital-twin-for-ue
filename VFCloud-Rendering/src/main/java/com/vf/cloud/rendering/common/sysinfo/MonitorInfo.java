package com.vf.cloud.rendering.common.sysinfo;

public class MonitorInfo {
	/** jvm可使用内存. */
	private double totalMemory;

	/** jvm剩余内存. */
	private double freeMemory;

	/** jvm最大可使用内存. */
	private double maxMemory;

	/** 操作系统. */
	private String osName;

	/** 总的物理内存. */
	private double totalMemorySize;

	/** 剩余的物理内存. */
	private double freeMemorySize;

	/** 已使用的物理内存. */
	private double usedMemorySize;

	/** 核心数. */
	private int processors;

	public double getTotalMemory() {
		return totalMemory;
	}

	public void setTotalMemory(double totalMemory) {
		this.totalMemory = totalMemory;
	}

	public double getFreeMemory() {
		return freeMemory;
	}

	public void setFreeMemory(double freeMemory) {
		this.freeMemory = freeMemory;
	}

	public double getMaxMemory() {
		return maxMemory;
	}

	public void setMaxMemory(double maxMemory) {
		this.maxMemory = maxMemory;
	}

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public double getTotalMemorySize() {
		return totalMemorySize;
	}

	public void setTotalMemorySize(double totalMemorySize) {
		this.totalMemorySize = totalMemorySize;
	}

	public double getFreeMemorySize() {
		return freeMemorySize;
	}

	public void setFreeMemorySize(double freeMemorySize) {
		this.freeMemorySize = freeMemorySize;
	}

	public double getUsedMemorySize() {
		return usedMemorySize;
	}

	public void setUsedMemorySize(double usedMemorySize) {
		this.usedMemorySize = usedMemorySize;
	}

	public int getProcessors() {
		return processors;
	}

	public void setProcessors(int processors) {
		this.processors = processors;
	}

	
}
