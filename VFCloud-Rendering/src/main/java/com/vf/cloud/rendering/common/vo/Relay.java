package com.vf.cloud.rendering.common.vo;

public class Relay {

	private String ip;
	private String stunPort;
	private String turnPort;
	private String username;
	private String password;
	private String enabled;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getStunPort() {
		return stunPort;
	}

	public void setStunPort(String stunPort) {
		this.stunPort = stunPort;
	}

	public String getTurnPort() {
		return turnPort;
	}

	public void setTurnPort(String turnPort) {
		this.turnPort = turnPort;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	
	

}
