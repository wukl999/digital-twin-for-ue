package com.vf.cloud.rendering.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.vf.cloud.common.util.R;
import com.vf.cloud.rendering.api.service.IRender;
import com.vf.cloud.rendering.common.vo.Dispatch;
import com.vf.cloud.rendering.common.vo.Gpu;
import com.vf.cloud.rendering.common.vo.Order;
import com.vf.cloud.rendering.common.vo.Relay;
import com.vf.cloud.rendering.common.vo.Renderer;
import com.vf.cloud.rendering.common.vo.Server;
import com.vf.cloud.rendering.common.vo.Signalling;
import com.vf.cloud.rendering.common.vo.req.AnyInfoParam;
import com.vf.cloud.rendering.common.vo.req.StartUpParam;

@RestController
public class RenderController {

	@Autowired
	private IRender iRender;

	@RequestMapping(value = "/Renderers/Any/order", method = RequestMethod.POST)
	public R<Order> AnyOrder(@RequestBody StartUpParam param) {
		return iRender.AnyOrder(param);
	}
	
	
	@RequestMapping(value = "/Renderers/Any/info", method = RequestMethod.POST)
	public R<Renderer> AnyInfo(@RequestBody AnyInfoParam param) {
		return iRender.AnyInfo(param);
	}
	
	
	@RequestMapping(value = "/Renderers/Any/Gpu/list", method = RequestMethod.POST)
	public R<List<Gpu>> AnyGpuList() {
		return iRender.AnyGpuList();
	}
	
	
	@RequestMapping(value = "/Renderers/Any/Server/list", method = RequestMethod.POST)
	public R<List<Server>> AnyServerList() {
		return iRender.AnyServerList();
	}
	
	@RequestMapping(value = "/Renderers/Any/Dispatch/info", method = RequestMethod.POST)
	public R<Dispatch> AnyDispatchInfo() {
		return iRender.AnyDispatchInfo();
	}
	
	@RequestMapping(value = "/Renderers/Any/Dispatch/update", method = RequestMethod.POST)
	public R<Dispatch> AnyDispatchUpdate(@RequestBody Dispatch param) {
		return iRender.AnyDispatchUpdate(param);
	}
	
	@RequestMapping(value = "/Renderers/Any/Signalling/info", method = RequestMethod.POST)
	public R<Signalling> AnySignallingInfo() {
		return iRender.AnySignallingInfo();
	}
	
	@RequestMapping(value = "/Renderers/Any/Signalling/update", method = RequestMethod.POST)
	public R<Signalling> AnySignallingUpdate(@RequestBody Signalling param) {
		return iRender.AnySignallingUpdate(param);
	}
	
	@RequestMapping(value = "/Renderers/Any/Relay/info", method = RequestMethod.POST)
	public R<Relay> AnyRelayInfo() {
		return iRender.AnyRelayInfo();
	}
	
	@RequestMapping(value = "/Renderers/Any/Relay/update", method = RequestMethod.POST)
	public R<Relay> AnyRelayUpdate(@RequestBody Relay param) {
		return iRender.AnyRelayUpdate(param);
	}
	
}
