package com.vf.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.scheduling.annotation.EnableAsync;

import com.vf.cloud.rendering.common.constant.Cache;
import com.vf.cloud.rendering.common.constant.SoftPath;

import java.io.File;
import java.util.UUID;

@EnableAsync
@SpringBootApplication
public class VfCloudRenderingServerApplication {

	public static void main(String[] args) {
		//getSoftPath(); //发布时启用
		Cache.isRunning=false;
		SpringApplication.run(VfCloudRenderingServerApplication.class, args);
	}

	public static void getSoftPath() {
		ApplicationHome ah = new ApplicationHome(VfCloudRenderingServerApplication.class);
		File file = ah.getSource();
		SoftPath.path = file.getParentFile().getParent();
		SoftPath.path = SoftPath.path.replace("\\", "/");
	}

	public static int getOrderNo() {
		Integer uuid = UUID.randomUUID().toString().replaceAll("-", "").hashCode();
		uuid = uuid < 0 ? -uuid : uuid;// String.hashCode() 值会为空

		return uuid;
	}

}
