package com.vf.cloud.common.util;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.vf.cloud.rendering.common.constant.SecurityConstant;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
 
public class JWTUtil {
    
    
    /**
     * 获取令牌
     * @param userId   用户Id
     * @param username 用户名
     * @param userNick 用户昵称
     * @return 令牌
     */
    public static String generateToken(String userId, String username) {
        long currentTimeMillis=System.currentTimeMillis();
        //设置头部信息
        Map<String,Object> header = new HashMap<>();
        header.put("typ","JWT");
        header.put("alg","HS256");
        
        return JWT
                .create()
                .withHeader(header)
                // 签发对象
                .withAudience(SecurityConstant.TOKEN_ISSUER)
                // 发行时间
                .withIssuedAt(new Date(currentTimeMillis))
                // 过期时间
                .withExpiresAt(new Date(currentTimeMillis+SecurityConstant.TOKEN_EXPIRATION_TIME))
                // 载荷
                .withClaim("userId", userId)
                .withClaim("username", username)
                // 签名
                .sign(
                        // 使用HMAC SHA256签名算法。
                        // HMAC（全称Hash-based Message Authentication Code），基于Hash函数和密钥的消息认证码。
                        Algorithm.HMAC256(userId + SecurityConstant.TOKEN_SECRET)
                );
    }
 
    /**
     * 检验令牌
     * @param token 令牌
     * @param secretPrefix 密钥前缀
     */
    public static void verifyToken(String token, String secretPrefix) {
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secretPrefix + SecurityConstant.TOKEN_SECRET)).build();
        verifier.verify(token);
    }
 
    /**
     * 获取签发对象
     *
     * @param token 令牌
     * @return 签发对象
     */
    public static String getAudience(String token) {
        List<String> audiences = JWT.decode(token).getAudience();
        String audience = audiences.get(0);
        
        
        
        return audience;
    }
 
    /**
     * 通过载荷名称获取载荷值
     * @param token     令牌
     * @param claimName 载荷名称
     * @return 载荷值
     */
    public static Claim getClainByName(String token, String claimName)throws Exception{
        return JWT.decode(token).getClaim(claimName);
    }
    
    
    public static Date getExpiresAt(String token) {
        return JWT.decode(token).getExpiresAt();
    }
    
 
    /**
     * 测试
     */
    public static void main(String[] args) throws InterruptedException {
//        String token = JWTUtil.generateToken("100001", "juyoujing");
//        System.out.println("令牌：" + token);
// 
//        JWTUtil.verifyToken(token,"100001");
// 
//        String audience = JWTUtil.getAudience(token);
//        System.out.println("签发对象：" + audience);
//        
//        Date date=JWTUtil.getExpiresAt(token);
//        System.out.println("过期时间：" +DateKit.toStr(date, "yyyy-MM-dd hh:mm:ss"));
//        Claim claim = JWTUtil.getClainByName(token, "userNick");
//        System.out.println("载荷：" + claim.asString());
 
    }
}

