package com.vf.cloud.common.pool;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import com.jfinal.kit.StrKit;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProssPool {

	private static ConcurrentHashMap<String, String> ProssClients = new ConcurrentHashMap<String, String>();

	public static void put(String key, String value) {
		ProssClients.put(key, value);
	}
	
	
	public static void kill(String key) {
		kill( key,ProssClients.get(key));
	}

	public static void stop() {
		Iterator<String> it = ProssClients.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			System.out.println(key+">>"+ProssClients.get(key));
			kill(key, ProssClients.get(key));
		}
	}

	public static void kill(String key, String pid) {
		if (!StrKit.isBlank(pid)) {
			try {
				Runtime runtime = Runtime.getRuntime();
				runtime.exec(String.format("cmd /c taskkill/F /PID %s ", pid));
				log.info(String.format("Kill %s >> %s.", key, pid));
			} catch (Exception e) {
			}
		}
		ProssClients.remove(key);
	}
	
	
	public static void killByPid(String pid) {
		if (!StrKit.isBlank(pid)) {
			try {
				Runtime runtime = Runtime.getRuntime();
				runtime.exec(String.format("cmd /c taskkill/F /PID %s ", pid));
				log.info(String.format("Kill >> %s.", pid));
			} catch (Exception e) {
			}
		}
	}
	
	
	public static boolean isExistsServer(String serverName) {
		try {
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec("cmd /c sc query |findstr " + serverName);
			BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] s = Pattern.compile("\\s+").matcher(line).replaceAll(";").split(";");
				if (s != null && s[0].contains("DISPLAY_NAME") && s[1].contains(serverName)) {
					return true;
				}
			}
		} catch (Exception e) {
		}
		return false;
	}

	public static String getPid(String port) {
		try {
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec("cmd /c netstat -ano | findstr " + port);
			BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF-8"));
			String line = null;
			String pid = null;
			while ((line = br.readLine()) != null) {
				String[] s = Pattern.compile("\\s+").matcher(line).replaceAll(";").split(";");
				if (s != null && s[2].contains(port)) {
					pid = s[s.length - 1];
					break;
				}
			}
			if (!StrKit.isBlank(pid)) {
				return pid;
			}
		} catch (Exception e) {
		}
		return "";
	}
	
	
	public static void killByPort(String port) {
		try {
			List<String> pids=new LinkedList<String>();
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec("cmd /c netstat -ano | findstr " + port);
			BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] s = Pattern.compile("\\s+").matcher(line).replaceAll(";").split(";");
				if (s != null) {
					System.out.println(line);
				    if(!StrKit.isBlank(s[s.length - 1])) {
				    	pids.add(s[s.length - 1]);
				    }
				    
				}
			}
			if(pids.size()>0) {
				for(int i=pids.size()-1;i>0;i--) {
					kill(port, pids.get(i));
				}
			}
		} catch (Exception e) {
		}
	}

}
