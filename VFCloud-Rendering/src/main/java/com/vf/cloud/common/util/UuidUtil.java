package com.vf.cloud.common.util;

import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

public class UuidUtil {

	public static String getUUID() {
		UUID uuid = UUID.randomUUID();
		String str = uuid.toString();
		str = str.replaceAll("-", "");
		return str;
	}

	public static String genCode() {
		StringBuilder uid = new StringBuilder();
		// 产生16位的强随机数
		Random rd = new SecureRandom();
		for (int i = 0; i < 16; i++) {
			// 产生0-2的3位随机数
			int type = rd.nextInt(3);
			switch (type) {
			case 0:
				// 0-9的随机数
				uid.append(rd.nextInt(10));
				/*
				 * int random = ThreadLocalRandom.current().ints(0, 10)
				 * .distinct().limit(1).findFirst().getAsInt();
				 */
				break;
			case 1:
				// ASCII在65-90之间为大写,获取大写随机
				uid.append((char) (rd.nextInt(25) + 65));
				break;
			case 2:
				// ASCII在97-122之间为小写，获取小写随机
				uid.append((char) (rd.nextInt(25) + 97));
				break;
			default:
				break;
			}
		}
		return uid.toString().toUpperCase();
	}

	public static String genKey() {
		Random random = new Random();
		String result = "";
		for (int i = 0; i < 8; i++) {
			result += (random.nextInt(9) + 1);
		}
		return result;
	}
	
	/**
	 * 生成6位随机数
	 */
	public static int randomCode() {
	    return (int) ((Math.random() * 9 + 1) * 100000);
	}
	
	public static void main(String[] args) {
		for(int i=0;i<1000;i++) {
			System.out.println(UuidUtil.genKey());
		}
	}

}
