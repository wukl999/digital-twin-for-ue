# DigitalTwinForUE

#### 介绍

UE像素流发布管理平台

#### 安装教程

1.  JDK1.8
2.  VFCloud/MasterAndAgent/VFCloud.bat
 **
只支持ue5 程序包** 

#### 近期开放功能

1、前端交互接口脚本
2、API自定义发布功能模块
3、API测试模块

#### 代码结构

1. VFCloud-Pass 》》 管理端 
     vfcloud.sql 最新数据库脚本（默认安装的脚本不是最新）
2. VFCloud-Rendering 》》 渲染主机节点（部署到GPU服务器）



#### 使用说明



部署包完整结构

- ![输入图片说明](doc/vfcolud.png)

渲染主机节点配置说明
 ![输入图片说明](vfclord_new.png)
C:\VFCloud\RenderingServer\config\server.ini

```
{
	"dispatch": {
		"secretKey": "PI30F8PRX30WAQT7",
		"port": "6666",
		"ip": "127.0.0.1"
	},
	"relay": {
		"turnPort": "19303",
		"password": "1",
		"ip": "127.0.0.1",
		"enabled": "1",
		"stunPort": "19302",
		"username": "1"
	},
	"local": {
		"projectPath": "D:/Projecs",
		"ip": "127.0.0.1",
		"mac": "FA-16-3E-99-E3-72"
	},
	"signalling": {
		"innerPort": 3333,
		"sslCertAbsolutePath": "",
		"port": 3333,
		"ip": "127.0.0.1",
		"sslKeyAbsolutePath": "",
		"innerIp": "127.0.0.1",
		"sslEnable": "0"
	}
}
```

- 链接：https://pan.baidu.com/s/1kMejYkSP8A8Yj6tDhqjh9g  提取码：1111 



### 云服务管理平台

![输入图片说明](doc/login.png)
![输入图片说明](doc/scene.png)
![输入图片说明](doc/project.png)
![输入图片说明](doc/project_Pre.png)
![输入图片说明](doc/project_deg.png)
![输入图片说明](doc/render_manage.png)
![输入图片说明](doc/render_gpu_limit.png)
![输入图片说明](doc/api_edit.png)



```
1、http://localhost:8880/Renderers/Any/order
Method:OPTIONS
Body:{
    "order":"工程ID",
    "width":窗口宽度,
    "height":窗口高度
}

返回:

{
    "code": 200,
    "message": " 成功.",
    "result": {
        "url": "ws://127.0.0.1:9999?EIO=65596748", //用于 app.js 里WebSocket 地址
        "renderingEngine": "",
        "expiresIn": 1659794032677,
        "fitMode": "any",
        "enableMenuBall": false,
        "enableVoiceFn": false,
        "projectId": "763ee35bd6cc47c3973b6a2640bf2d1a",
        "appId": "763ee35bd6cc47c3973b6a2640bf2d1a",
        "artype": "None"
    }
}


```
