/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80011
Source Host           : localhost:3306
Source Database       : micros

Target Server Type    : MYSQL
Target Server Version : 80011
File Encoding         : 65001

Date: 2022-08-11 01:13:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单名称',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `parent_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `prev_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) DEFAULT '1' COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '备注',
  `no_cache` int(11) DEFAULT '0',
  `active_menu` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('0772436ba43b4715b8a43aa594de925f', '样式管理', 'Style', '328191e55199490b98a0d49271cae0e2', '5', '', '/style', '/51world/style/index.vue', null, '1', '0', 'C', '0', '0', '', '', 'system', '2022-04-09 20:44:34', 'system', '2022-05-08 04:04:35', '', '0', '');
INSERT INTO `sys_menu` VALUES ('1', '系统管理', 'System', '0', '999', '', '/system', null, null, '1', '0', 'M', '0', '0', null, 'el-icon-setting', '', '2022-02-15 21:20:05', '', '2022-02-16 22:35:36', '系统管理目录', '0', '');
INSERT INTO `sys_menu` VALUES ('101', '菜单管理', 'Menu', '1', '10', '', 'menu', '/system/menu/index.vue', null, '1', '0', 'C', '0', '0', null, 'el-icon-menu', '', null, '', '2022-02-15 21:20:56', '菜单管理菜单', '0', '');
INSERT INTO `sys_menu` VALUES ('198801d310a8426c988da4ecb5187702', '天气管理', 'Weather', '0', '500', '', '/weather', '', null, '1', '0', 'M', '0', '0', '', 'el-icon-light-rain', 'system', '2022-03-07 16:09:16', 'system', '2022-03-07 16:11:14', '', '0', '');
INSERT INTO `sys_menu` VALUES ('273c4490df5b4b2d89f8ab8c919a8456', '部门管理', 'Dept', '1', '3', '', 'dept', '/system/dept/index.vue', null, '1', '0', 'M', '0', '0', '', 'el-icon-notebook-2', '', '2022-02-17 21:24:03', '', '2022-02-17 21:28:59', '', '0', '');
INSERT INTO `sys_menu` VALUES ('27e867a324834d21a8c16f2365e99a93', '角色管理', 'Role', '1', '2', '', 'role', '/system/role/index.vue', null, '1', '0', 'C', '0', '0', '', 'el-icon-s-custom', '', '2022-02-17 21:22:01', '', '2022-02-17 21:27:56', '', '0', '');
INSERT INTO `sys_menu` VALUES ('28bf753f99a3402cb15245190ccee3ae', '登录日志', 'Login', 'd96ebee03dd64ac9b6b49369e8de5745', '1', '', 'login', '/system/log/login/index.vue', null, '1', '0', 'C', '0', '0', '', '', '', '2022-02-17 21:34:40', '', '2022-02-22 21:18:50', '', '0', '');
INSERT INTO `sys_menu` VALUES ('2b608c6864014193925eccc26e3bf9c9', '模板管理', 'Template', '0', '1', '', '/template', '', null, '1', '0', 'M', '0', '1', '', 'el-icon-tickets', 'system', '2022-03-12 17:08:06', 'system', '2022-05-08 02:42:38', '', '0', '');
INSERT INTO `sys_menu` VALUES ('2cec4b7459e44666b7d54a8505b27e1c', '系统监控', 'Monitor', '0', '1000', '', '/monitor', '', null, '1', '0', 'M', '0', '1', '', 'el-icon-data-line', '', '2022-02-28 09:56:48', '', '2022-02-28 09:57:37', '', '0', '');
INSERT INTO `sys_menu` VALUES ('328191e55199490b98a0d49271cae0e2', '51world', '51world', '0', '1', '', '/51world', '', null, '1', '0', 'M', '0', '0', '', 'el-icon-folder', 'system', '2022-03-05 15:15:08', 'system', '2022-03-05 15:16:51', '', '0', '');
INSERT INTO `sys_menu` VALUES ('4541d053f4d140cdb0b05ff289052f7d', '场景资源', 'Src', '328191e55199490b98a0d49271cae0e2', '2', '', '/src', '/51world/src/index.vue', null, '1', '0', 'C', '0', '0', '', '', 'system', '2022-04-09 17:15:09', 'system', '2022-04-09 17:16:55', '', '0', '');
INSERT INTO `sys_menu` VALUES ('48822a5552c4480fa76e6ae97b914dca', '用户管理', 'User', '1', '1', '', 'user', '/system/user/index.vue', null, '1', '0', 'C', '0', '0', '', 'el-icon-user-solid', '', '2022-02-17 18:59:27', '', '2022-02-17 21:28:53', '', '0', '');
INSERT INTO `sys_menu` VALUES ('50b1ffd8dd9245e0994c16f1b6b5a0b3', '场景图层', 'SceneLayer', '328191e55199490b98a0d49271cae0e2', '3', '', '/scene_layer', '/51world/layer/index.vue', null, '1', '0', 'C', '0', '1', '', '', 'system', '2022-05-09 05:44:31', '', '2022-05-09 05:44:30', '', '0', '');
INSERT INTO `sys_menu` VALUES ('70a494abdaa64931abe2b1307a99c07d', '时时天气', 'Often', '198801d310a8426c988da4ecb5187702', '1', '', 'often', '/weather/often/index.vue', null, '1', '0', 'C', '0', '0', '', '', 'system', '2022-03-07 16:09:56', 'system', '2022-03-07 16:10:16', '', '0', '');
INSERT INTO `sys_menu` VALUES ('a0aed0c603a541d286673010078a8e8d', '在线用户', 'OnlineUser', '2cec4b7459e44666b7d54a8505b27e1c', '1', '', '/onlineUser', '/monitor/user/index.vue', null, '1', '0', 'C', '0', '0', '', 'el-icon-phone-outline', '', '2022-02-28 09:58:22', 'system', '2022-02-28 09:59:05', '', '0', '');
INSERT INTO `sys_menu` VALUES ('a1818b5f41dd48ab854f16934585ce39', '字典管理', 'Dic', '1', '4', '', 'dic', '/system/dic/index.vue', null, '1', '0', 'C', '0', '0', '', 'el-icon-notebook-1', '', '2022-02-17 21:30:12', '', '2022-02-17 21:39:24', '', '0', '');
INSERT INTO `sys_menu` VALUES ('bc03a922b43043a198dc435d1e39137b', '表单模板', 'Form', '2b608c6864014193925eccc26e3bf9c9', '1', '', 'form', '/template/form/index.vue', null, '1', '0', 'C', '0', '0', '', 'el-icon-notebook-1', 'system', '2022-03-12 17:08:32', 'system', '2022-03-12 17:14:51', '', '0', '');
INSERT INTO `sys_menu` VALUES ('c30ed0ba357f4b74ab7657d51d85a1c3', '场景管理', 'Scene', '328191e55199490b98a0d49271cae0e2', '1', '', '/scene', '/51world/scene/index.vue', null, '1', '0', 'C', '0', '0', '', '', 'system', '2022-04-09 16:29:16', 'system', '2022-04-09 16:29:46', '', '0', '');
INSERT INTO `sys_menu` VALUES ('cdf5a8cc05ae41deabfb546f6dca9819', '操作日志', 'Opera', 'd96ebee03dd64ac9b6b49369e8de5745', '2', '', 'opera', '/system/log/opera/index.vue', null, '1', '0', 'C', '0', '0', '', '', '', '2022-02-17 21:35:47', '', '2022-02-22 21:19:09', '', '0', '');
INSERT INTO `sys_menu` VALUES ('cf9b70de16a8436eba3e191bebb1eadc', '场景菜单', 'Scene_Menu', '328191e55199490b98a0d49271cae0e2', '4', '', '/scene_menu', '/51world/menu/index.vue', null, '1', '0', 'C', '0', '0', '', '', 'system', '2022-05-08 04:04:27', 'system', '2022-05-09 05:43:56', '', '0', '');
INSERT INTO `sys_menu` VALUES ('d96ebee03dd64ac9b6b49369e8de5745', '日志管理', 'Log', '1', '5', '', 'log', '/system/log/index.vue', null, '1', '0', 'M', '0', '0', '', 'el-icon-warning', '', '2022-02-17 21:32:08', '', '2022-02-22 21:24:14', '', '0', '');
