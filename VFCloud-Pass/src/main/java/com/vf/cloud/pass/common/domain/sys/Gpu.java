package com.vf.cloud.pass.common.domain.sys;

import com.vf.cloud.pass.common.domain.sys.base.BaseGpu;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Gpu extends BaseGpu<Gpu> {
	public static final Gpu dao = new Gpu().dao();
	public static final String TABLE_NAME = "sys_gpu";
	
	private int free;

	public int getFree() {
		if (super.get("free") == null) {
			super.put("free", this.free);
		}
		return super.get("free");
	}

	public void setFree(int free) {
		super.put("free", free);
	}

	private int used;

	public int getUsed() {
		if (super.get("used") == null) {
			super.put("used", this.used);
		}
		return super.get("used");
	}

	public void setUsed(int used) {
		super.put("used", used);
	}
	
	private int percentage;

	public int getPercentage() {
		if (super.get("percentage") == null) {
			super.put("percentage", this.percentage);
		}
		return super.get("percentage");
	}

	public void setPercentage(int percentage) {
		super.put("percentage", percentage);
	}

}
