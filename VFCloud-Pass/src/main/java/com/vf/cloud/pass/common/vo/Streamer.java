package com.vf.cloud.pass.common.vo;

public class Streamer {
	
	private String mac;
	private String uuid;
	private int index;
	private int block;
	private String sid;
	private String EIO;
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getBlock() {
		return block;
	}
	public void setBlock(int block) {
		this.block = block;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getEIO() {
		return EIO;
	}
	public void setEIO(String eIO) {
		EIO = eIO;
	}
	
	

}
