package com.vf.cloud.pass.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import com.jfinal.kit.StrKit;
import com.vf.cloud.pass.common.constant.SecurityConstant;
import com.vf.cloud.pass.common.service.IToken;
import lombok.RequiredArgsConstructor;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RequiredArgsConstructor
@Configuration
public class WebSecurityConfig implements Filter {

	private final IToken tokenImpl;

	private CorsConfiguration buildConfig() {
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.addAllowedOrigin("*"); // 1允许任何域名使用
		corsConfiguration.addAllowedHeader("*"); // 2允许任何头
		corsConfiguration.addAllowedMethod("*"); // 3允许任何方法（post、get等）
		corsConfiguration.setMaxAge(1800L);// 4.解决跨域请求两次，预检请求的有效期，单位为秒
		return corsConfiguration;
	}

	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", buildConfig()); // 4
		return new CorsFilter(source);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		if (httpRequest.getRequestURI().startsWith("/Renderers/Any") || httpRequest.getRequestURI().startsWith("/login")
				|| httpRequest.getRequestURI().startsWith("/project") || httpRequest.getRequestURI().startsWith("/reg")
				|| httpRequest.getRequestURI().startsWith("/biz/scene/thumbnail")
				|| httpRequest.getRequestURI().startsWith("/updatePwd")
				|| httpRequest.getRequestURI().startsWith("/code")
				|| httpRequest.getRequestURI().startsWith("/down")
				|| httpRequest.getRequestURI().startsWith("/version")
				|| httpRequest.getRequestURI().startsWith("/TokenExpired")
				|| httpRequest.getRequestURI().startsWith("/biz/terminal/scene/syn")
				|| httpRequest.getRequestURI().startsWith("/API/OPEN")
				|| httpRequest.getRequestURI().startsWith("/sys/user/thumbnail")) {
			chain.doFilter(request, response);
		} else {
			String token = httpRequest.getHeader(SecurityConstant.HEADER);
			if (StrKit.isBlank(token)) {
				httpResponse.sendRedirect("/TokenExpired");// 重定向
			} else {
				if(tokenImpl.isExpire(token.replace(SecurityConstant.TOKEN_SPLIT, ""))) {
					httpResponse.sendRedirect("/TokenExpired");// 重定向
				}
			}
			chain.doFilter(request, response);
		}
	}
}