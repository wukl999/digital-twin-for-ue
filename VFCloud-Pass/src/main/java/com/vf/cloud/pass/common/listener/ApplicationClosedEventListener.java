package com.vf.cloud.pass.common.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

import com.vf.cloud.pass.common.util.ProssUtil;
import com.vf.cloud.server.nginx.NginxServer;


@Component
public class ApplicationClosedEventListener implements ApplicationListener<ContextClosedEvent>{
	@Override
	public void onApplicationEvent(ContextClosedEvent event) {
		NginxServer.getInstance().stop();
		ProssUtil.stop();
		
	}


}
