package com.vf.cloud.pass.common.domain.sys;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.cloud.pass.common.domain.sys.base.BaseDocApi;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class DocApi extends BaseDocApi<DocApi> {
	public static final DocApi dao = new DocApi().dao();
	public static final String TABLE_NAME = "doc_api";
	
	
	private List<DocApi> children;

	public List<DocApi> getChildren() {
		if (super.get("children") == null) {
			super.put("children", this.children);
		}
		return super.get("children");
	}

	public void setChildren(List<DocApi> children) {
		super.put("children", children);
	}
	
	private boolean hasChildren;

	public boolean getHasChildren() {
		if (super.get("hasChildren") == null) {
			super.put("hasChildren", this.hasChildren);
		}
		return super.get("hasChildren");
	}

	public void setHasChildren(boolean hasChildren) {
		super.put("hasChildren", hasChildren);
	}
	
	
	private String label;

	public String getLabel() {
		if (super.get("label") == null) {
			super.put("label", this.label);
		}
		return super.get("label");
	}

	public void setLabel(String label) {
		super.put("label", label);
	}
	
	public List<DocApi> getTableTree(String parentId) {
		String sql = "select m.*,(CASE WHEN (select count(id) from doc_api where m.id=parent_id) >0 THEN 1 ELSE 0 END) AS hasChildren from doc_api m where m.parent_id=? order by m.sort asc";
		List<DocApi> DocApis=DocApi.dao.find(sql,parentId);
		for(DocApi DocApi :DocApis) {
			DocApi.setHasChildren(DocApi.getLong("hasChildren")==1);
		}
		return DocApis;
	}
	
	/**
	 * 获取子菜单
	 * @param parentId
	 * @param userId
	 * @return
	 */
	public List<DocApi> getChildrenByPid(String parentId,String type) {
		String sql = "select m.*,m.name label from doc_api m where m.parent_id='" + parentId + "'  ";
		if(StrKit.notBlank(type)) {
			sql = sql + " and m.type like '%"+type+"%'";
		}
		sql = sql + " order by m.sort asc";
		return DocApi.dao.find(sql);
	}
	
	public List<DocApi> getTree(String id,String type) {
		List<DocApi> list = getChildrenByPid(id,type);// 根据id查询孩子
		
		for (DocApi o : list) {
			List<DocApi> children = getTree(o.getId(),type);
			if(children!=null && children.size()>0 ) {
				o.setChildren(children);
			}
			o.setLabel(o.getName());
		}
		return list;
	}
}
