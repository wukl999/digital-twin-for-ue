package com.vf.cloud.pass.common.security.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;


@Aspect
@Component
public class SecurityAop {


    @Pointcut("@annotation(com.vf.cloud.pass.common.security.annotation.Security)")
    private void method() {
    }

    @Around(value = "method()", argNames = "pjp")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        Object target = pjp.getTarget().getClass().getName();
        System.out.println("调用者+"+target);
        Object[] args = pjp.getArgs();//2.传参  
        System.out.println("2.传参:----"+args[0]);  
         for (Object object : args) {
            System.out.println(object instanceof HttpServletRequest);
        }
        return pjp.proceed();
    }

}
