package com.vf.cloud.pass.common.vo;

public class SignallingVo {
	
	private String id;
	private String  signalingIP;
	private String signalingInnerIP;
	private int signalingPort;
	private int signalingInnerPort;
	private String signalingEnableSSL;
	private String signalingSSLKeyAbsolutePath;
	private String signalingSSLCertAbsolutePath;
	private String enbaled;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSignalingIP() {
		return signalingIP;
	}
	public void setSignalingIP(String signalingIP) {
		this.signalingIP = signalingIP;
	}
	public String getSignalingInnerIP() {
		return signalingInnerIP;
	}
	public void setSignalingInnerIP(String signalingInnerIP) {
		this.signalingInnerIP = signalingInnerIP;
	}
	public int getSignalingPort() {
		return signalingPort;
	}
	public void setSignalingPort(int signalingPort) {
		this.signalingPort = signalingPort;
	}
	public int getSignalingInnerPort() {
		return signalingInnerPort;
	}
	public void setSignalingInnerPort(int signalingInnerPort) {
		this.signalingInnerPort = signalingInnerPort;
	}
	public String getSignalingEnableSSL() {
		return signalingEnableSSL;
	}
	public void setSignalingEnableSSL(String signalingEnableSSL) {
		this.signalingEnableSSL = signalingEnableSSL;
	}
	public String getSignalingSSLKeyAbsolutePath() {
		return signalingSSLKeyAbsolutePath;
	}
	public void setSignalingSSLKeyAbsolutePath(String signalingSSLKeyAbsolutePath) {
		this.signalingSSLKeyAbsolutePath = signalingSSLKeyAbsolutePath;
	}
	public String getSignalingSSLCertAbsolutePath() {
		return signalingSSLCertAbsolutePath;
	}
	public void setSignalingSSLCertAbsolutePath(String signalingSSLCertAbsolutePath) {
		this.signalingSSLCertAbsolutePath = signalingSSLCertAbsolutePath;
	}
	public String getEnbaled() {
		return enbaled;
	}
	public void setEnbaled(String enbaled) {
		this.enbaled = enbaled;
	}
	

}
