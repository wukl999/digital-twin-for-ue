package com.vf.cloud.pass.common.vo;

import java.util.List;

public class TerminalConfigVo {
	
	private List<GpuVo> gpus;

	public List<GpuVo> getGpus() {
		return gpus;
	}

	public void setGpus(List<GpuVo> gpus) {
		this.gpus = gpus;
	}
	

}
