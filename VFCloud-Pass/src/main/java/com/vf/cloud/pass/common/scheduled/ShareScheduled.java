package com.vf.cloud.pass.common.scheduled;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.vf.cloud.pass.common.domain.biz.ProjectShare;

@Component
@EnableScheduling
public class ShareScheduled {

	public static void main(String[] args) {
		// 指定一个字符串时间
		String wordGameStartTime = "2022-08-23 00:00:01";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(wordGameStartTime, formatter);
		// 获取当前时间
		LocalDateTime now = LocalDateTime.now(); // 2019-1-31
		int days = (int) Duration.between(dateTime, now).toDays();
		System.out.println(days + 1); // 6
	}

	// @Scheduled(cron = "0 40 23 * * ? ") //每1秒执行一次 //0 40 23 * * ?
	/**
	 * 每5s检测ue客户端是否正常使用
	 * 
	 * @throws Exception
	 */
	@Scheduled(fixedRate = 1000 * 60 * 60)
	@Async // 异步执行
	public void scheduled() throws Exception {
		try {
			List<ProjectShare> sceneShares = ProjectShare.dao.findAll();
			for (ProjectShare ss : sceneShares) {
				LocalDateTime now = LocalDateTime.now(); // 2019-1-31
				int days = (int) Duration.between(ss.getCreateTime(), now).toDays();
				if (days < 0) {
					ss.dao().delete();
				}
			}
		} catch (Exception e) {
		}
	}

}
