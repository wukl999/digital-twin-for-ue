package com.vf.cloud.pass.mvc.biz.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vf.cloud.pass.common.domain.sys.User;
import com.vf.cloud.pass.common.factory.GPUFactory;
import com.vf.cloud.pass.common.util.R;
import com.vf.cloud.pass.common.util.WebUtils;
import com.vf.cloud.pass.common.vo.ProjectRendering;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/biz/ps")
public class PixelStreamingController {
	
	
	@GetMapping(value = { "/list" }, produces = "application/json; charset=utf-8")
	public R<List<ProjectRendering>> findList(HttpServletRequest request,
			@RequestParam(name = "keywords", required = false, defaultValue = "") String keywords,
			@RequestParam(name = "status", required = false, defaultValue = "") String status,
			@RequestParam(name = "cur", required = false, defaultValue = "1") int cur,
			@RequestParam(name = "limit", required = false, defaultValue = "10") int limit) throws Exception {
		
		User user=WebUtils.getCurUser(request);
		
		List<ProjectRendering> list=GPUFactory.getInstance().getList(user);
		return R.ok(list);
	}

}
