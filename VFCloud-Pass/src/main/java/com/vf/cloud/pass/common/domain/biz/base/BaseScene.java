package com.vf.cloud.pass.common.domain.biz.base;

import com.jfinal.plugin.activerecord.Model;

import java.time.LocalDateTime;

import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseScene<M extends BaseScene<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setName(java.lang.String name) {
		set("name", name);
		return (M)this;
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public M setIndustryType(java.lang.String industryType) {
		set("industry_type", industryType);
		return (M)this;
	}
	
	public java.lang.String getIndustryType() {
		return getStr("industry_type");
	}

	public M setSize(java.lang.String size) {
		set("size", size);
		return (M)this;
	}
	
	public java.lang.String getSize() {
		return getStr("size");
	}

	public M setVersion(java.lang.Double version) {
		set("version", version);
		return (M)this;
	}
	
	public java.lang.Double getVersion() {
		return getDouble("version");
	}

	public M setExeFilePath(java.lang.String exeFilePath) {
		set("exe_file_path", exeFilePath);
		return (M)this;
	}
	
	public java.lang.String getExeFilePath() {
		return getStr("exe_file_path");
	}

	public M setZipFile(java.lang.String zipFile) {
		set("zip_file", zipFile);
		return (M)this;
	}
	
	public java.lang.String getZipFile() {
		return getStr("zip_file");
	}

	public M setExeFolder(java.lang.String exeFolder) {
		set("exe_folder", exeFolder);
		return (M)this;
	}
	
	public java.lang.String getExeFolder() {
		return getStr("exe_folder");
	}

	public M setRemark(java.lang.String remark) {
		set("remark", remark);
		return (M)this;
	}
	
	public java.lang.String getRemark() {
		return getStr("remark");
	}

	public M setFrameRate(java.lang.Integer frameRate) {
		set("frame_rate", frameRate);
		return (M)this;
	}
	
	public java.lang.Integer getFrameRate() {
		return getInt("frame_rate");
	}

	public M setIsForceres(java.lang.String isForceres) {
		set("is_forceRes", isForceres);
		return (M)this;
	}
	
	public java.lang.String getIsForceres() {
		return getStr("is_forceRes");
	}

	public M setResX(java.lang.Integer resX) {
		set("resX", resX);
		return (M)this;
	}
	
	public java.lang.Integer getResX() {
		return getInt("resX");
	}

	public M setResY(java.lang.Integer resY) {
		set("resY", resY);
		return (M)this;
	}
	
	public java.lang.Integer getResY() {
		return getInt("resY");
	}

	public M setUpdateBy(java.lang.String updateBy) {
		set("update_by", updateBy);
		return (M)this;
	}
	
	public java.lang.String getUpdateBy() {
		return getStr("update_by");
	}

	public M setUpdateTime(LocalDateTime updateTime) {
		set("update_time", updateTime);
		return (M)this;
	}
	
	public LocalDateTime getUpdateTime() {
		return get("update_time");
	}

	public M setCreateBy(java.lang.String createBy) {
		set("create_by", createBy);
		return (M)this;
	}
	
	public java.lang.String getCreateBy() {
		return getStr("create_by");
	}

	public M setCreateTime(LocalDateTime createTime) {
		set("create_time", createTime);
		return (M)this;
	}
	
	public LocalDateTime getCreateTime() {
		return get("create_time");
	}

	public M setIsRelease(java.lang.String isRelease) {
		set("is_release", isRelease);
		return (M)this;
	}
	
	public java.lang.String getIsRelease() {
		return getStr("is_release");
	}

	public M setParameter(java.lang.String parameter) {
		set("parameter", parameter);
		return (M)this;
	}
	
	public java.lang.String getParameter() {
		return getStr("parameter");
	}

	public M setIsPublic(java.lang.String isPublic) {
		set("is_public", isPublic);
		return (M)this;
	}
	
	public java.lang.String getIsPublic() {
		return getStr("is_public");
	}

}
