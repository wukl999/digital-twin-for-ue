package com.vf.cloud.pass.common.vo;

import java.time.LocalDateTime;
import java.util.Date;

public class ProjectRendering {
	
	private String order;
	private String key;
	private LocalDateTime startTime;
	private boolean isConnected;
	private Date timeout;
	private String projectName;
	private String sceneName;
	private String orgId;
	private String orgName;
	private String terminaName;
	
	private LocalDateTime connectedTime;
	
	
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public LocalDateTime getStartTime() {
		return startTime;
	}
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	public boolean isConnected() {
		return isConnected;
	}
	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}
	public Date getTimeout() {
		return timeout;
	}
	public void setTimeout(Date timeout) {
		this.timeout = timeout;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getSceneName() {
		return sceneName;
	}
	public void setSceneName(String sceneName) {
		this.sceneName = sceneName;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getTerminaName() {
		return terminaName;
	}
	public void setTerminaName(String terminaName) {
		this.terminaName = terminaName;
	}
	public LocalDateTime getConnectedTime() {
		return connectedTime;
	}
	public void setConnectedTime(LocalDateTime connectedTime) {
		this.connectedTime = connectedTime;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
	

}
