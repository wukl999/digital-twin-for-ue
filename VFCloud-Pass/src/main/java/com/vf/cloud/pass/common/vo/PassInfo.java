package com.vf.cloud.pass.common.vo;

public class PassInfo {
	
	private int gpuNumber;
	private int limitNumber;
	private int terminalNumber;
	public int getGpuNumber() {
		return gpuNumber;
	}
	public void setGpuNumber(int gpuNumber) {
		this.gpuNumber = gpuNumber;
	}
	public int getLimitNumber() {
		return limitNumber;
	}
	public void setLimitNumber(int limitNumber) {
		this.limitNumber = limitNumber;
	}
	public int getTerminalNumber() {
		return terminalNumber;
	}
	public void setTerminalNumber(int terminalNumber) {
		this.terminalNumber = terminalNumber;
	}
	

}
