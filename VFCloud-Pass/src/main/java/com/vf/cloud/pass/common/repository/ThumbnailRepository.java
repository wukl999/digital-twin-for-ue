package com.vf.cloud.pass.common.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.vf.cloud.pass.common.repository.base.Thumbnail;

@Repository
public interface ThumbnailRepository extends MongoRepository<Thumbnail, String> {
}