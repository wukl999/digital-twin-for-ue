package com.vf.cloud.pass.mvc.biz.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.vf.cloud.pass.common.domain.biz.Config;
import com.vf.cloud.pass.common.util.R;
import com.vf.cloud.pass.common.util.UuidUtil;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/biz/settings")
public class SettingsController {

	@GetMapping("/getConfig")
	public R<Config> getConfig() {
		Config config = Config.dao.findById("vf_cloud_pass_2022");
		return R.ok(config);
	}

	@RequestMapping(value = "/saveConfig", method = RequestMethod.POST)
	public R<Config> saveConfig(Config config) {
		try {
			config.setId("vf_cloud_pass_2022");

			Config is = Config.dao.findById(config.getId());

			if (is != null) {
				if (config.update()) {
					return R.ok();
				} else {
					return R.failed();
				}
			} else {
				config.setDispatchSecretKey(UuidUtil.genCode());
				if (config.save()) {
					return R.ok();
				} else {
					return R.failed();
				}
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

}
