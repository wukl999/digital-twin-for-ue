package com.vf.cloud.pass.dev;


import java.util.List;

import javax.sql.DataSource;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

public class GeneratorModel {

	public static DataSource getDataSource() {
		DruidPlugin druidPlugin = new DruidPlugin("jdbc:mysql://127.0.0.1:23308/vfcloud?characterEncoding=utf8&useUnicode=true&characterEncoding=utf-8&useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=UTC&nullNamePatternMatchesAll=true&autoReconnect=true", "root", "vfcloud@2022");
		druidPlugin.start();
		return druidPlugin.getDataSource();
	}

	public static void main(String[] args) {
		//genVueForm();
		genSysModel();
	}
	
	
	public static void startPlugin() {
		DruidPlugin druidPlugin = new DruidPlugin("jdbc:mysql://192.168.1.230:3306/micros?characterEncoding=utf8&useUnicode=true&characterEncoding=utf-8&useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=UTC&nullNamePatternMatchesAll=true&autoReconnect=true", "root", "123456");
		ActiveRecordPlugin arp = new ActiveRecordPlugin("mysql",druidPlugin);
		druidPlugin.start();
		arp.start();
	}
	
	public static void genVueForm() {
		startPlugin();
		String sql="select column_name,column_comment from information_schema.`COLUMNS` where table_Name='sys_menu' and table_schema ='micros' ";
		List<Record> list=Db.find(sql);
		for(Record r: list) {
			System.out.println(""+columToJave(r.getStr("COLUMN_NAME"))+":"+"undefined,");
		}
		
	}
	
    /**
	 * 数据字段变成Java属性
	 * 
	 * @param string
	 * @return
	 */
	private static String columToJave(String string) {
		String string2 = string.toLowerCase();
		StringBuilder builder = new StringBuilder(string2);
 
		for (int i = 0; i < builder.length(); i++) {
			if (builder.charAt(i) == '_') {
				// 第一次出现该符号的位置
				char c = builder.charAt(i + 1);
				c = (char) (c - 32);
				StringBuilder replace1 = builder.replace(i + 1, i + 2, c + "");
				builder = replace1.replace(i, i + 1, "");
 
				// 最后一次出现该符号的位置
				int of = builder.lastIndexOf("_", string2.length());
				if (of != -1) {
					char c1 = builder.charAt(of + 1);
					c1 = (char) (c1 - 32);
					StringBuilder replace2 = builder.replace(of + 1, of + 2, c1
							+ "");
					builder = replace2.replace(of, of + 1, "");
					;
				}
 
			}
		}
		return builder.toString();
	}

	
	public static void genSysModel() {
		// base model 所使用的包名
		String baseModelPackageName = "com.vf.cloud.common.domain.base";
		// base model 文件保存路径
		String baseModelOutputDir = "D:\\develop\\vfcloud\\domain";

		// model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = "com.vf.cloud.common.domain";
		// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String modelOutputDir = baseModelOutputDir + "/..";

		// 创建生成器
		Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName,
				modelOutputDir);
		// generator.setMetaBuilder(new _MetaBuilder(getDataSource()));
		// 设置是否生成链式 setter 方法
		generator.setGenerateChainSetter(false);
		// 添加不需要生成的表名
		generator.addExcludedTable("");
		;
		// 设置是否在 Model 中生成 dao 对象
		generator.setGenerateDaoInModel(true);
		// 设置是否生成链式 setter 方法
		generator.setGenerateChainSetter(true);
		// 设置是否生成字典文件
		generator.setGenerateDataDictionary(false);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非
		// OscUser
		generator.setRemovedTableNamePrefixes("sys_");
		// 生成
		generator.generate();
	}
	
}