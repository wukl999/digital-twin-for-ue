package com.vf.cloud.pass.mvc.biz.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.cloud.pass.common.datasource.annotation.Tx;
import com.vf.cloud.pass.common.domain.biz.Terminal;
import com.vf.cloud.pass.common.domain.biz.TerminalGpu;
import com.vf.cloud.pass.common.util.DateUtil;
import com.vf.cloud.pass.common.util.R;
import com.vf.cloud.pass.common.util.UuidUtil;
import com.vf.cloud.pass.common.vo.PassInfo;
import com.vf.cloud.pass.mvc.biz.service.ITerminal;
import com.vf.cloud.pass.mvc.biz.service.IThumbnail;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/biz/terminal")
public class TerminalController {

	@Autowired
	private IThumbnail thumbnailImpl;

	@Autowired
	private ITerminal terminalImpl;

	@GetMapping(value = { "/list" }, produces = "application/json; charset=utf-8")
	public R<Page<Terminal>> findList(
			@RequestParam(name = "keywords", required = false, defaultValue = "") String keywords,
			@RequestParam(name = "status", required = false, defaultValue = "") String status,
			@RequestParam(name = "cur", required = false, defaultValue = "1") int cur,
			@RequestParam(name = "limit", required = false, defaultValue = "10") int limit) throws Exception {

		String sqlExceptSelect = " FROM " + Terminal.TABLE_NAME + " o  WHERE 1=1 ";
		if (!StrKit.isBlank(keywords)) {
			sqlExceptSelect += " AND ( O.NAME LIKE '%" + keywords + "%' ) ";
		}
		sqlExceptSelect += "  ORDER BY   O.UPDATE_TIME asc ";
		Page<Terminal> pages = Terminal.dao.paginate(cur, limit, "select * ", sqlExceptSelect);
		for (Terminal terminal : pages.getList()) {
			if (terminal.getOnlineTime() != null && StrKit.equals(terminal.getStatus(), "1")) {
				terminal.setOnlineTimeDes(DateUtil.calculateTimeDifferenceByPeriod(terminal.getOnlineTime()));
			}
		}
		return R.ok(pages);
	}

	@Tx
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public R<Terminal> save(Terminal model) {
		try {

			if (StrKit.notBlank(model.getId())) {
				if (model.update()) {
					return R.ok(model);
				} else {
					return R.failed("更新失败！");
				}
			} else {
				model.setId(UuidUtil.getUUID());
				if (model.save()) {
					return R.ok(model);
				} else {
					return R.failed("保存失败！");
				}
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@Tx
	@RequestMapping(value = "/deleteById", method = RequestMethod.GET)
	public R<String> deleteById(@RequestParam(name = "id") String id) throws Exception {
		try {
			Terminal terminal = Terminal.dao.findById(id);
			if (terminal == null)
				return R.failed("数据不存在，刷新后尝试!");
			if (terminal.delete()) {
				try {
					thumbnailImpl.deleteById(terminal.getId());
				} catch (Exception e) {
				}
				return R.ok();
			}
			return R.failed("失败！");
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public R<Terminal> getById(@RequestParam(name = "id") String id) throws Exception {
		try {
			Terminal sever = Terminal.dao.findById(id);
			if (sever == null)
				return R.failed("数据不存在，刷新后尝试!");
			return R.ok(sever);
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@RequestMapping(value = "/getRemoteInfo", method = RequestMethod.GET)
	public R<String> getRemoteInfo(@RequestParam(name = "id") String id) {
		return terminalImpl.getRemoteInfo(id);
	}

	@RequestMapping(value = "/getPassInfo", method = RequestMethod.GET)
	public R<PassInfo> getPassInfo() {

		PassInfo passInfo = new PassInfo();
		List<TerminalGpu> list = TerminalGpu.dao.findAll();
		List<Terminal> terminals = Terminal.dao.findAll();
		int limit = 0;
		for (TerminalGpu terminalGpu : list) {
			limit +=terminalGpu.getGpuLimit();
		}
		passInfo.setLimitNumber(limit);
		passInfo.setGpuNumber(list != null ? list.size() : 0);
		passInfo.setTerminalNumber(terminals != null ? terminals.size() : 0);
		return R.ok(passInfo);
	}

}
