package com.vf.cloud.pass.common.scheduled;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.vf.cloud.pass.common.factory.GPUFactory;

@Component
@EnableScheduling
public class GPUScheduled {

	private boolean isStop = true;

	@Scheduled(fixedRate = 5000)
	@Async
	public void scheduled() throws Exception {
		if (isStop) {
			isStop = false;
			GPUFactory.getInstance().check();
			isStop = true;
		}

	}

}
