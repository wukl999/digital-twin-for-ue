package com.vf.cloud.pass.common.domain.biz;

import com.vf.cloud.pass.common.domain.biz.base.BaseSceneShare;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class SceneShare extends BaseSceneShare<SceneShare> {
	public static final SceneShare dao = new SceneShare().dao();
	public static final String TABLE_NAME = "biz_scene_share";
}
