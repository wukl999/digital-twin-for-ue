package com.vf.cloud.pass.mvc.biz.service;

import com.vf.cloud.pass.common.util.R;

public interface ITerminal {
	
	
	public R<String> getRemoteInfo(String id);

}
