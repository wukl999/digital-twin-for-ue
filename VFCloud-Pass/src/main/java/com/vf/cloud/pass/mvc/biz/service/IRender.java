package com.vf.cloud.pass.mvc.biz.service;

import com.vf.cloud.pass.common.util.R;
import com.vf.cloud.pass.common.vo.Order;
import com.vf.cloud.pass.common.vo.req.OrderParam;

public interface IRender {
	
	public R<Order> AnyOrder(OrderParam param);

}
