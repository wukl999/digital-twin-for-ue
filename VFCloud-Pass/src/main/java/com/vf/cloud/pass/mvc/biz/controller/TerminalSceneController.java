package com.vf.cloud.pass.mvc.biz.controller;

import java.util.LinkedList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jfinal.kit.StrKit;
import com.vf.cloud.pass.common.domain.biz.Scene;
import com.vf.cloud.pass.common.domain.biz.Terminal;
import com.vf.cloud.pass.common.domain.biz.TerminalScene;
import com.vf.cloud.pass.common.util.R;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/biz/terminal/scene")
public class TerminalSceneController {

	@GetMapping(value = { "/list" }, produces = "application/json; charset=utf-8")
	public R<List<Terminal>> findList(
			@RequestParam(name = "sceneId", required = false, defaultValue = "") String sceneId) throws Exception {
		List<Terminal> list = Terminal.dao.findAll();

		Scene scene = Scene.dao.findById(sceneId);
		for (Terminal terminal : list) {
			terminal.setScene(scene);
			terminal.setTerminalScene(TerminalScene.dao.findByIds(terminal.getId(), sceneId));
		}
		return R.ok(list);
	}

	@GetMapping(value = { "/syn" }, produces = "application/json; charset=utf-8")
	public R<List<String>> syn(@RequestParam(name = "mac", required = false, defaultValue = "") String mac)
			throws Exception {

		List<String> synScenes = new LinkedList<String>();
		List<Scene> scenes = Scene.dao.findAll();
		for (Scene scene : scenes) {
			TerminalScene terminalScene = TerminalScene.dao.findByIds(mac, scene.getId());
			if (terminalScene != null) {
				if (scene.getVersion() > terminalScene.getVersion()) {
					synScenes.add(scene.getId());
				} else {
					if (!StrKit.equals("2", terminalScene.getDownLoadStatus().trim())
							&& !StrKit.equals("3", terminalScene.getDownLoadStatus().trim())) {
						synScenes.add(scene.getId());
					}
				}
			} else {
				synScenes.add(scene.getId());
			}
		}
		return R.ok(synScenes);
	}

}
