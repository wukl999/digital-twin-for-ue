package com.vf.cloud.pass.common.vo;

public class SceneShare {
	
	private String id;
	private String name;
	private String path;
	private String resX;
	private String resY;
	private String rq;
	private String img;
	private int limitedTime;
	private String url;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getResX() {
		return resX;
	}
	public void setResX(String resX) {
		this.resX = resX;
	}
	public String getResY() {
		return resY;
	}
	public void setResY(String resY) {
		this.resY = resY;
	}
	public String getRq() {
		return rq;
	}
	public void setRq(String rq) {
		this.rq = rq;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public int getLimitedTime() {
		return limitedTime;
	}
	public void setLimitedTime(int limitedTime) {
		this.limitedTime = limitedTime;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	

}
