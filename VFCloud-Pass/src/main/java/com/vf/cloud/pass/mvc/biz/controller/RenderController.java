package com.vf.cloud.pass.mvc.biz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.vf.cloud.pass.common.util.R;
import com.vf.cloud.pass.common.vo.Order;
import com.vf.cloud.pass.common.vo.req.OrderParam;
import com.vf.cloud.pass.mvc.biz.service.IRender;

@RestController
public class RenderController {

	@Autowired
	private IRender iRender;

	@RequestMapping(value = "/Renderers/Any/order", method = RequestMethod.POST)
	public R<Order> AnyOrder(@RequestBody OrderParam param) {
		return iRender.AnyOrder(param);
	}

}
