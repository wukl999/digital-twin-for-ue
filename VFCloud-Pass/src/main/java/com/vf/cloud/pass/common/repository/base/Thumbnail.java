package com.vf.cloud.pass.common.repository.base;

import java.util.Date;

import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Data;

@Data
@Document(collection = "vf_thumbnail")
public class Thumbnail {
	
    @Id  // 主键
    private String id;
    private String name; // 文件名称
    private String contentType; // 文件类型
    private long size;
    private String md5;
    private Binary content; // 文件内容
    private String path; // 文件路径
    private Date uploadDate;
	
}
