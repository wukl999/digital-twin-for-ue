package com.vf.cloud.pass.mvc.biz.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jfinal.plugin.activerecord.Page;
import com.vf.cloud.pass.common.domain.sys.User;
import com.vf.cloud.pass.common.util.R;


public class SceneUserPermissionController {

	@GetMapping(value = { "/list" }, produces = "application/json; charset=utf-8")
	public R<Page<User>> findList(@RequestParam(name = "keywords", required = false, defaultValue = "") String keywords,
			@RequestParam(name = "sceneId", required = true, defaultValue = "") String sceneId,
			@RequestParam(name = "cur", required = false, defaultValue = "1") int cur,
			@RequestParam(name = "limit", required = false, defaultValue = "10") int limit) throws Exception {

		// select a.id,a.phone,a.org_id,a.org_name,(select `limit` from biz_project
		// where org_id=a.id and scene_id='94b2d2b9429a44a6bbf01d6372c7ef04' ) as
		// 'limit' from sys_user a
		String sqlExceptSelect = " from sys_user  a WHERE a.is_sub_account='0' ";
		sqlExceptSelect += "  ORDER BY   b.UPDATE_TIME desc ";
		Page<User> pages = User.dao.paginate(cur, limit,
				"select a.id,a.phone,a.org_id,a.org_name,(select `limit` from  biz_scene_user_permission where org_id=a.id and scene_id='"
						+ sceneId + "' ) as 'limit' ",
				sqlExceptSelect);
		return R.ok(pages);
	}

}
