package com.vf.cloud.pass.common.domain.biz;

import com.vf.cloud.pass.common.domain.biz.base.BaseConfig;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Config extends BaseConfig<Config> {
	public static final Config dao = new Config().dao();
	public static final String TABLE_NAME = "biz_config";
}
