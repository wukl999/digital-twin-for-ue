package com.vf.cloud.pass.common.domain.sys.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseGpu<M extends BaseGpu<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setName(java.lang.String name) {
		set("name", name);
		return (M)this;
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public M setStatus(java.lang.String status) {
		set("status", status);
		return (M)this;
	}
	
	public java.lang.String getStatus() {
		return getStr("status");
	}

	public M setLimit(java.lang.Integer limit) {
		set("limit", limit);
		return (M)this;
	}
	
	public java.lang.Integer getLimit() {
		return getInt("limit");
	}

	public M setEnabled(java.lang.String enabled) {
		set("enabled", enabled);
		return (M)this;
	}
	
	public java.lang.String getEnabled() {
		return getStr("enabled");
	}

	public M setMemoryTotal(java.lang.String memoryTotal) {
		set("memory_total", memoryTotal);
		return (M)this;
	}
	
	public java.lang.String getMemoryTotal() {
		return getStr("memory_total");
	}

	public M setBoardId(java.lang.String boardId) {
		set("board_id", boardId);
		return (M)this;
	}
	
	public java.lang.String getBoardId() {
		return getStr("board_id");
	}

	public M setVbiosVersion(java.lang.String vbiosVersion) {
		set("vbios_version", vbiosVersion);
		return (M)this;
	}
	
	public java.lang.String getVbiosVersion() {
		return getStr("vbios_version");
	}

	public M setMemoryUsed(java.lang.String memoryUsed) {
		set("memory_used", memoryUsed);
		return (M)this;
	}
	
	public java.lang.String getMemoryUsed() {
		return getStr("memory_used");
	}

	public M setMemoryFree(java.lang.String memoryFree) {
		set("memory_free", memoryFree);
		return (M)this;
	}
	
	public java.lang.String getMemoryFree() {
		return getStr("memory_free");
	}

	public M setMemoryReserved(java.lang.String memoryReserved) {
		set("memory_reserved", memoryReserved);
		return (M)this;
	}
	
	public java.lang.String getMemoryReserved() {
		return getStr("memory_reserved");
	}

	public M setIndex(java.lang.Integer index) {
		set("index", index);
		return (M)this;
	}
	
	public java.lang.Integer getIndex() {
		return getInt("index");
	}

}
