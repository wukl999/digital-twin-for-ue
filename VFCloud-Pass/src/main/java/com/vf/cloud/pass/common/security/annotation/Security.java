package com.vf.cloud.pass.common.security.annotation;


import java.lang.annotation.*;

@Inherited
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Security {

}
