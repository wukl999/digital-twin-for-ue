package com.vf.cloud.pass.mvc.biz.service;

import com.vf.cloud.pass.common.repository.base.Thumbnail;

public interface IThumbnail {

	Thumbnail save(Thumbnail entity);

	void deleteById(String id);

	Thumbnail findById(String id);

}
