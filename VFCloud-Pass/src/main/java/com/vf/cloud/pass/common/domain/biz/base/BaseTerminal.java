package com.vf.cloud.pass.common.domain.biz.base;

import com.jfinal.plugin.activerecord.Model;

import java.time.LocalDateTime;

import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseTerminal<M extends BaseTerminal<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setName(java.lang.String name) {
		set("name", name);
		return (M)this;
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public M setIp(java.lang.String ip) {
		set("ip", ip);
		return (M)this;
	}
	
	public java.lang.String getIp() {
		return getStr("ip");
	}

	public M setPort(java.lang.String port) {
		set("port", port);
		return (M)this;
	}
	
	public java.lang.String getPort() {
		return getStr("port");
	}

	public M setStatus(java.lang.String status) {
		set("status", status);
		return (M)this;
	}
	
	public java.lang.String getStatus() {
		return getStr("status");
	}

	public M setProjectPath(java.lang.String projectPath) {
		set("project_path", projectPath);
		return (M)this;
	}
	
	public java.lang.String getProjectPath() {
		return getStr("project_path");
	}

	public M setOs(java.lang.String os) {
		set("os", os);
		return (M)this;
	}
	
	public java.lang.String getOs() {
		return getStr("os");
	}

	public M setMemoryTotal(java.lang.Double memoryTotal) {
		set("memory_total", memoryTotal);
		return (M)this;
	}
	
	public java.lang.Double getMemoryTotal() {
		return getDouble("memory_total");
	}

	public M setMemoryUsed(java.lang.Double memoryUsed) {
		set("memory_used", memoryUsed);
		return (M)this;
	}
	
	public java.lang.Double getMemoryUsed() {
		return getDouble("memory_used");
	}

	public M setMemoryUsedPercentage(java.lang.Double memoryUsedPercentage) {
		set("memory_used_percentage", memoryUsedPercentage);
		return (M)this;
	}
	
	public java.lang.Double getMemoryUsedPercentage() {
		return getDouble("memory_used_percentage");
	}

	public M setGpuModel(java.lang.String gpuModel) {
		set("gpu_model", gpuModel);
		return (M)this;
	}
	
	public java.lang.String getGpuModel() {
		return getStr("gpu_model");
	}

	public M setGpuNum(java.lang.Integer gpuNum) {
		set("gpu_num", gpuNum);
		return (M)this;
	}
	
	public java.lang.Integer getGpuNum() {
		return getInt("gpu_num");
	}

	public M setGpuMemoryUsed(java.lang.Double gpuMemoryUsed) {
		set("gpu_memory_used", gpuMemoryUsed);
		return (M)this;
	}
	
	public java.lang.Double getGpuMemoryUsed() {
		return getDouble("gpu_memory_used");
	}

	public M setGpuMemoryTotal(java.lang.Double gpuMemoryTotal) {
		set("gpu_memory_total", gpuMemoryTotal);
		return (M)this;
	}
	
	public java.lang.Double getGpuMemoryTotal() {
		return getDouble("gpu_memory_total");
	}

	public M setGpuMemoryUsedPercentage(java.lang.Double gpuMemoryUsedPercentage) {
		set("gpu_memory_used_percentage", gpuMemoryUsedPercentage);
		return (M)this;
	}
	
	public java.lang.Double getGpuMemoryUsedPercentage() {
		return getDouble("gpu_memory_used_percentage");
	}

	public M setOnlineTime(LocalDateTime onlineTime) {
		set("online_time", onlineTime);
		return (M)this;
	}
	
	public LocalDateTime getOnlineTime() {
		return get("online_time");
	}

	public M setOfflineTime(LocalDateTime offlineTime) {
		set("offline_time", offlineTime);
		return (M)this;
	}
	
	public LocalDateTime getOfflineTime() {
		return get("offline_time");
	}

	public M setCreateTime(LocalDateTime createTime) {
		set("create_time", createTime);
		return (M)this;
	}
	
	public LocalDateTime getCreateTime() {
		return get("create_time");
	}

	public M setUpdateTime(LocalDateTime updateTime) {
		set("update_time", updateTime);
		return (M)this;
	}
	
	public LocalDateTime getUpdateTime() {
		return get("update_time");
	}

	public M setRemark(java.lang.String remark) {
		set("remark", remark);
		return (M)this;
	}
	
	public java.lang.String getRemark() {
		return getStr("remark");
	}

	public M setIsSupportLimit(java.lang.String isSupportLimit) {
		set("is_support_limit", isSupportLimit);
		return (M)this;
	}
	
	public java.lang.String getIsSupportLimit() {
		return getStr("is_support_limit");
	}

}
