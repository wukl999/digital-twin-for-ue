package com.vf.cloud.pass.mvc.biz.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jfinal.kit.StrKit;
import com.vf.cloud.pass.common.datasource.annotation.Tx;
import com.vf.cloud.pass.common.domain.biz.TerminalGpu;
import com.vf.cloud.pass.common.util.R;
import com.vf.cloud.pass.common.util.UuidUtil;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/biz/terminal/gpu")
public class TerminalGpuController {

	@GetMapping(value = { "/list" }, produces = "application/json; charset=utf-8")
	public R<List<TerminalGpu>> findList(
			@RequestParam(name = "terminalId", required = false, defaultValue = "") String terminalId)
			throws Exception {
		String sqlExceptSelect = " FROM " + TerminalGpu.TABLE_NAME + " o  WHERE terminal_id='" + terminalId
				+ "'  ORDER BY   O.gpu_index asc ";
		List<TerminalGpu> list = TerminalGpu.dao.find("select * " + sqlExceptSelect);
		return R.ok(list);
	}

	@Tx
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public R<TerminalGpu> save(TerminalGpu model) {
		try {

			if (StrKit.notBlank(model.getId())) {
				if (model.update()) {
					return R.ok(model);
				} else {
					return R.failed("更新失败！");
				}
			} else {
				model.setId(UuidUtil.getUUID());
				if (model.save()) {
					return R.ok(model);
				} else {
					return R.failed("保存失败！");
				}
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

}
