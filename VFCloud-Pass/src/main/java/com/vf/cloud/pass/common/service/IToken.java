package com.vf.cloud.pass.common.service;

public interface IToken {
	
	public boolean isExpire(String key);

}
