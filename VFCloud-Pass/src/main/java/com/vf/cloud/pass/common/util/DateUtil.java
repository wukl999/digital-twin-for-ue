package com.vf.cloud.pass.common.util;

import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class DateUtil {

	public static LocalDateTime getLocalDateTime() {
		Instant instant = Instant.ofEpochMilli(System.currentTimeMillis());
		ZoneId zone = ZoneId.systemDefault();
		return LocalDateTime.ofInstant(instant, zone);
	}

	public static LocalDateTime getDateTimeOfTimestamp(long timestamp) {
		Instant instant = Instant.ofEpochMilli(timestamp);
		ZoneId zone = ZoneId.systemDefault();
		return LocalDateTime.ofInstant(instant, zone);
	}

	public static String calculateTimeDifferenceByPeriod(LocalDateTime oldDate) throws ParseException {
		Date old = localDateTimeToDate(oldDate);
		Date curNow = localDateTimeToDate(getLocalDateTime());
		long between = curNow.getTime() - old.getTime();
		long day = between / (24 * 60 * 60 * 1000);
		long hour = (between / (60 * 60 * 1000) - day * 24);
		long min = ((between / (60 * 1000)) - day * 24 * 60 - hour * 60);
		long s = (between / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
		// System.out.println(day + "天" + hour + "小时" + min + "分" + s + "秒");
		StringBuffer sb = new StringBuffer();
		if (day != 0) {
			sb.append(day).append("天").append(" ");
		}
		if (hour != 0) {
			sb.append(hour).append("小时").append(" ");
		}
		sb.append(min).append("分").append(" ");
		sb.append(s).append("秒");
		return sb.toString();
	}

	public static Date localDateTimeToDate(LocalDateTime localDateTime) {
		ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
		Date from = Date.from(zonedDateTime.toInstant());
		return from;
	}

	public static void main(String[] args) {

	}

}
