package com.vf.cloud.pass.common.vo;

public class DescriptorVo {
	
	private String uekey;
	private int type;
	private Object data;
	
	public String getUekey() {
		return uekey;
	}
	public void setUekey(String uekey) {
		this.uekey = uekey;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	

}
