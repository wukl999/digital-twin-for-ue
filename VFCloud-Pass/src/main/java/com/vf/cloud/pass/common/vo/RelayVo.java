package com.vf.cloud.pass.common.vo;

public class RelayVo {

	private String id;
	private String ip;
	private int stunPort;
	private int turnPort;
	private String username;
	private String password;
	private String enabled;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getStunPort() {
		return stunPort;
	}
	public void setStunPort(int stunPort) {
		this.stunPort = stunPort;
	}
	public int getTurnPort() {
		return turnPort;
	}
	public void setTurnPort(int turnPort) {
		this.turnPort = turnPort;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}


}
