package com.vf.cloud.pass.common.util;

import java.text.DecimalFormat;

public class FileUtil {
	
	public static String formatFileSize(long size) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        String wrongSize = "0B";
        if (size == 0) {
            return wrongSize;
        }
        if (size < 1024) {
            fileSizeString = df.format((double) size) + "B";
        } else if (size < 1048576) {
            fileSizeString = df.format((double) size / 1024) + "KB";
        } else if (size < 1073741824) {
            fileSizeString = df.format((double) size / 1048576) + "MB";
        } else if (size < 1099511627776L) {
            fileSizeString = df.format((double) size / 1073741824) + "GB";
        } else if (size < 1125899906842624L) {
            fileSizeString = df.format((double) size / 1099511627776L) + "TB";
        } else {
            fileSizeString = df.format((double) size / 1125899906842624L) + "PB";
        }
        return fileSizeString;
    }

}
