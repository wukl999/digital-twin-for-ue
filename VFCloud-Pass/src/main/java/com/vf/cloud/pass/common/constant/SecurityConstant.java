package com.vf.cloud.pass.common.constant;

public interface SecurityConstant {

	String PROJECT_AUTH_TOKEN = "micros_auth:token:";

	/**
	 * Authorization参数头
	 */
	String HEADER = "Authorization";

	/**
	 * token分割
	 */
	String TOKEN_SPLIT = "Bearer ";

	/**
	 * token中自定义权限标识
	 */
	String AUTHORITIES = "AUTHORITIES_";

	/**
	 * token失效时间
	 */
	Integer TOKEN_EXPIRATION_TIME = 30 * 60 * 100000;

	/**
	 * Token 发行人
	 */
	String TOKEN_ISSUER = "micros";

	/**
	 * JWT签名加密key
	 */
	String TOKEN_SECRET = "micros";

	/**
	 * 刷新Token时间
	 */
	Integer TOKEN_REFRESH_EXPTIME = 720;

	String TOKEN_KEY_PREFIX = "MICROS_KEY_";
	String TOKEN_AUTH_PREFIX = "MICROS_AUTH_";

	/**
	 * 默认登录URL
	 */
	String OAUTH_TOKEN_URL = "/login";

	/**
	 * grant_type
	 */
	String REFRESH_TOKEN = "refresh_token";

	/**
	 * 内部
	 */
	String FROM_IN = "Y";

	/**
	 * 标志
	 */
	String FROM = "from";

	/**
	 * 验证码
	 */
	String VERIFY_CODE_PREFIX = "CODE_";

	Integer ERIFY_CODE_EXPIRATION_TIME = 5 * 60 * 100000;

	/**
	 * 私钥
	 */
	String PRIVATE_KEY = "MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEArKI6Z0t9yul6mnyxtsUUMnjg0wQlQgAb29NwP1L1hj34fwg1PzThjhb96B90F2mbYAxsKz6uFaQRMuM2oolh1wIDAQABAkB5NcKqCjA9i2dhFxI0UPXYZlT89FkwmSD2XOPgImBvquSMJihoXbRY6tEKjQuqC+kVQRjarSIOAOkOWDIhYrlZAiEA6QBMJKNMybA5wO5Jbq+lHDBJjtNxC2BzyC9sxROR3AUCIQC9rIGiiit7/N4+0ihHyCOix8IHjcSyWe6ri6iwidxJKwIgAse8INvJ+MYfikvNmIKEB9gTqJ6hRK978jGOrLNVFDkCICR0Z2YKEFmAPGbulXdWbCL5Kb2x7cApsMmfyghENuaJAiEAg92DKp8pmmPqiwSD0zV3FPIfv3P+SYzxMFENYj9JtaQ=";

	/**
	 * 公钥
	 */
	String PUBLIC_KEY = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKyiOmdLfcrpepp8sbbFFDJ44NMEJUIAG9vTcD9S9YY9+H8INT804Y4W/egfdBdpm2AMbCs+rhWkETLjNqKJYdcCAwEAAQ==";

}
