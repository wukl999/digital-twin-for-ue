package com.vf.cloud.pass.common.domain.biz;

import com.vf.cloud.pass.common.domain.biz.base.BaseTerminalScene;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class TerminalScene extends BaseTerminalScene<TerminalScene> {
	public static final TerminalScene dao = new TerminalScene().dao();
	public static final String TABLE_NAME = "biz_terminal_scene";
}
