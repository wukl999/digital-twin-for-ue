package com.vf.cloud.pass.common.vo;

public class Order {

	private String appId;
	private String EIO;
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getEIO() {
		return EIO;
	}

	public void setEIO(String eIO) {
		EIO = eIO;
	}

}
