package com.vf.cloud.pass.common.datasource.annotation;


import java.lang.annotation.*;

/**
 * @描述  Tx 【Jfinal事物交给spring管理注解 目前只支持用在方法上】
 */
@Inherited
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Tx {

}
