package com.vf.cloud.pass.common.mapping;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.vf.cloud.pass.common.domain.biz.*;

public class _BizMappingKit {

	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping(Scene.TABLE_NAME, "id", Scene.class);
		arp.addMapping(SceneShare.TABLE_NAME, "id", SceneShare.class);
		arp.addMapping("biz_scene_user_permission", "org_id,scene_id", SceneUserPermission.class);
		arp.addMapping(Terminal.TABLE_NAME, "id", Terminal.class);
		arp.addMapping(TerminalGpu.TABLE_NAME, "id", TerminalGpu.class);
		arp.addMapping(Project.TABLE_NAME, "id", Project.class);
		arp.addMapping(ProjectShare.TABLE_NAME, "id", ProjectShare.class);
		arp.addMapping(Config.TABLE_NAME, "id", Config.class);
		arp.addMapping("biz_terminal_scene", "terminal_id,scene_id", TerminalScene.class);
		arp.addMapping("biz_download_log", "terminal_id,scene_id,block,version", DownloadLog.class);


		
	}
}
