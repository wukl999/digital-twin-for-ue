package com.vf.cloud.pass.common.vo;

public class GpuVo {

	private String name;
	private String uuid;
	private String boardId;
	private String vbiosVersion;
	private String memoryTotal;
	private String memoryReserved;
	private String memoryUsed;
	private String memoryFree;
	private int moduleId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getVbiosVersion() {
		return vbiosVersion;
	}

	public void setVbiosVersion(String vbiosVersion) {
		this.vbiosVersion = vbiosVersion;
	}

	public String getMemoryTotal() {
		return memoryTotal;
	}

	public void setMemoryTotal(String memoryTotal) {
		this.memoryTotal = memoryTotal;
	}

	public String getMemoryReserved() {
		return memoryReserved;
	}

	public void setMemoryReserved(String memoryReserved) {
		this.memoryReserved = memoryReserved;
	}

	public String getMemoryUsed() {
		return memoryUsed;
	}

	public void setMemoryUsed(String memoryUsed) {
		this.memoryUsed = memoryUsed;
	}

	public String getMemoryFree() {
		return memoryFree;
	}

	public void setMemoryFree(String memoryFree) {
		this.memoryFree = memoryFree;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

}
