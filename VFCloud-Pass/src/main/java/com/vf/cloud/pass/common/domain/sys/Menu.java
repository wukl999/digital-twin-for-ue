package com.vf.cloud.pass.common.domain.sys;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.cloud.pass.common.domain.sys.base.BaseMenu;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Menu extends BaseMenu<Menu> {
	public static final Menu dao = new Menu().dao();
	public static final String TABLE_NAME = "SYS_MENU";

	private List<Menu> children;

	public List<Menu> getChildren() {
		if (super.get("children") == null) {
			super.put("children", this.children);
		}
		return super.get("children");
	}

	public void setChildren(List<Menu> children) {
		super.put("children", children);
	}
	
	private boolean hasChildren;

	public boolean getHasChildren() {
		if (super.get("hasChildren") == null) {
			super.put("hasChildren", this.hasChildren);
		}
		return super.get("hasChildren");
	}

	public void setHasChildren(boolean hasChildren) {
		super.put("hasChildren", hasChildren);
	}
	
	
	private String label;

	public String getLabel() {
		if (super.get("label") == null) {
			super.put("label", this.label);
		}
		return super.get("label");
	}

	public void setLabel(String label) {
		super.put("label", label);
	}

	/**
	 * 获取所有
	 * @param id
	 * @param userid
	 * @return
	 */
	public List<Menu> getChildrenAll(String id, String userid) {
		List<Menu> result = new ArrayList<Menu>();
		List<Menu> list = getChildrenByPid(id, userid,false);// 根据id查询孩子
		result.addAll(list);
		for (Menu o : list) {
			result.addAll(getChildrenAll(o.getId(), userid));
		}
		return result;
	}
	
	/**
	 * 获取子菜单
	 * @param parentId
	 * @param userId
	 * @return
	 */
	public List<Menu> getChildrenByPid(String parentId, String userId,boolean isRute) {
		String sql = "select * from sys_menu m where m.parent_id='" + parentId + "'  ";
		if (!StrKit.isBlank(userId)) {
			String authSql = " (select b.menu_id from sys_role_menu b where b.role_id in (select role_id from sys_user_role where user_id='"
					+ userId + "')) ";
			sql = sql + " and m.id in  " + authSql + " ";
		}
		
		if(isRute) {
			sql = sql + " and m.type !='F'";
		}
		
		sql = sql + " order by m.order_num asc";
		return Menu.dao.find(sql);
	}
	
	public List<Menu> getTree(String id, String userid) {
		List<Menu> list = getChildrenByPid(id, userid,false);// 根据id查询孩子
		
		for (Menu o : list) {
			o.setChildren(getTree(o.getId(), userid));
			o.setLabel(o.getName());
		}
		return list;
	}

	public List<Menu> getTableTree(String parentId) {
		String sql = "select m.*,(CASE WHEN (select count(id) from sys_menu where m.id=parent_id) >0 THEN 1 ELSE 0 END) AS hasChildren from sys_menu m where m.parent_id=? order by m.order_num asc";
		List<Menu> menus=Menu.dao.find(sql,parentId);
		for(Menu menu :menus) {
			menu.setHasChildren(menu.getLong("hasChildren")==1);
		}
		return menus;
	}

	public List<Menu> getAuthRutes(String id, String userid) {
		List<Menu> list = getChildrenByPid(id, userid,true);// 根据id查询孩子
		for (Menu o : list) {
			o.setChildren(getAuthRutes(o.getId(), userid));
			o.setLabel(o.getName());
		}
		return list;
	}

	public List<Menu> getAuthSelectTree(String roleId) {
		
		
		
		
		// TODO Auto-generated method stub
		return null;
	} 
	
	

//	public List<Menu> getChildrenByPid(String id, String userid) {
//		String sql = "select * from sys_menu m where m.pId='" + id + "'  ";
//		if (userid != null && !StrKit.isBlank(userid)) {
//			String authSql = " (select b.menuId from sys_menu_permission b where b.roleId in (select roleId from sys_user_role where userId='"
//					+ userid + "')) ";
//			sql = sql + " and m.id in  " + authSql + " ";
//		}
//		sql = sql + " order by m.sort";
//		return dao.find(sql);
//	}

}
