package com.vf.cloud.pass.common.vo;

import com.vf.cloud.pass.common.domain.biz.Project;
import com.vf.cloud.pass.common.domain.biz.Scene;

public class ProjectDebug {
	
	private Project project;
	private Scene scene;
	private String url;
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public Scene getScene() {
		return scene;
	}
	public void setScene(Scene scene) {
		this.scene = scene;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
