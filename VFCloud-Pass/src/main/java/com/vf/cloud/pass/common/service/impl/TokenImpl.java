package com.vf.cloud.pass.common.service.impl;

import java.util.Date;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import com.vf.cloud.pass.common.service.IToken;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class TokenImpl implements IToken{
	private final StringRedisTemplate redisTemplate;

	@Override
	public boolean isExpire(String key) {
		if (redisTemplate.getExpire(key) >= new Date(System.currentTimeMillis()).getTime()) {
			redisTemplate.delete(key);
			return true;
		}
		return false;
	}
}
