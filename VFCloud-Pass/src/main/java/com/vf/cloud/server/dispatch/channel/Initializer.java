package com.vf.cloud.server.dispatch.channel;

import java.util.concurrent.TimeUnit;

import com.vf.cloud.server.dispatch.handler.Handler;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;

public class Initializer extends ChannelInitializer<SocketChannel> {

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ch.pipeline()
				// 设置30秒没有读到数据，则触发一个READER_IDLE事件。
				.addLast(new IdleStateHandler(0, 30, 0, TimeUnit.SECONDS))
				// HttpServerCodec：将请求和应答消息解码为HTTP消息
				.addLast(new HttpServerCodec()).addLast(new StringEncoder())// 添加字符串编码器
				// HttpObjectAggregator：将HTTP消息的多个部分合成一条完整的HTTP消息
				.addLast(new HttpObjectAggregator(65536))
				// ChunkedWriteHandler：向客户端发送HTML5文件
				.addLast(new ChunkedWriteHandler())
				// .addLast(new WebSocketServerProtocolHandler("/", "ws", true, 65536 * 10))
				.addLast(new Handler());
	}

}
