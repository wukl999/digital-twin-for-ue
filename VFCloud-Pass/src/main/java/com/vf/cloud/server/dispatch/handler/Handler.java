package com.vf.cloud.server.dispatch.handler;

import java.util.Map;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.vf.cloud.pass.common.constant.Cache;
import com.vf.cloud.pass.common.util.RequestParser;
import com.vf.cloud.server.dispatch.DispatchServer;
import com.vf.cloud.server.dispatch.pool.DispatchPool;
import com.vf.cloud.server.dispatch.util.DispatchUtil;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpMessage;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PingWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Handler extends SimpleChannelInboundHandler<Object> {

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {

		if (2 == DispatchServer.getInstance().getStatus()) {
			log.info(String.format("RenderingServer is connected:" + ctx.channel().remoteAddress()));
		} else {
			log.info(String.format("RenderingServer : " + DispatchServer.getInstance().getStatus()
					+ ",Now close Channel:" + ctx.channel().remoteAddress()));
			ctx.close();
		}

	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		DispatchUtil.disconnected(ctx.channel().id().asLongText());
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		ctx.channel().flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		ctx.close();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
		try {

			if (2 == DispatchServer.getInstance().getStatus()) {
				if (msg instanceof FullHttpRequest) {
					handleHttpRequest(ctx, (FullHttpRequest) msg);
				} else {
					handlerWebSocketFrame(ctx, msg);
				}
			}

		} finally {
		}
	}

	WebSocketServerHandshaker handshaker;

	/**
	 * 处理 FullHttpRequest
	 * 
	 * @param ctx
	 * @param request
	 */
	protected void handleHttpRequest(ChannelHandlerContext ctx, FullHttpRequest req) {
		if (!req.decoderResult().isSuccess() || (!"websocket".equals(req.headers().get("Upgrade")))) {
			sendHttpResponse(ctx, req,
					new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST));
			return;
		}

		String uri = req.uri();
		WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory(
				"ws://" + req.headers().get(HttpHeaderNames.HOST) + uri, null, false);
		handshaker = wsFactory.newHandshaker(req);
		if (handshaker == null) {
			WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
		} else {
			handshaker.handshake(ctx.channel(), req);
		}

		if (uri.contains("?")) {
			log.info("RenderingServer comming!");
			try {
				Map<String, String> param = new RequestParser(req).parse();
				if (!param.containsKey("secretKey") || !param.containsKey("mac")) {
					log.error("口令缺失.");
					ctx.close();
					return;
				}
				String mac = param.get("mac");
				String secretKey = param.get("secretKey");
				if (!StrKit.equals(Cache.dispatch.getSecretKey(), secretKey)) {
					log.error("RenderingServer 口令无效:%s", secretKey);
					ctx.close();
					return;
				}
				DispatchPool.addNode(mac, ctx);
			} catch (Exception e) {
				log.error("非法节点.", e);
				ctx.close();
			}
		} else {
			log.error("非法节点.");
			ctx.close();
		}

	}

	public static void sendHttpResponse(ChannelHandlerContext ctx, FullHttpRequest req, DefaultFullHttpResponse res) {
		if (res.status().code() != 200) {
			ByteBuf buf = Unpooled.copiedBuffer(res.status().code() + "", CharsetUtil.UTF_8);
			res.content().writeBytes(buf);
			buf.release();
		}
		// 如果是非Keep-Alive，关闭连接
		if (!HttpUtil.isKeepAlive((HttpMessage) req.protocolVersion()) || res.status().code() != 200) {
			ctx.channel().writeAndFlush(res).addListener(ChannelFutureListener.CLOSE);
		}
	}

	private void handlerWebSocketFrame(ChannelHandlerContext ctx, Object frame) {
		// 判断是否关闭链路的指令
		if (frame instanceof CloseWebSocketFrame) {
			CloseWebSocketFrame closeWebSocketFrame = (CloseWebSocketFrame) frame;
			handshaker.close(ctx.channel(), (CloseWebSocketFrame) closeWebSocketFrame.retain());
			return;
		}
		// 判断是否ping消息
		if (frame instanceof PingWebSocketFrame) {
			return;
		}

		if (frame instanceof TextWebSocketFrame) {

			TextWebSocketFrame textWebSocketFrame = (TextWebSocketFrame) frame;
			JSONObject rawMsg = null;
			try {
				rawMsg = JSONObject.parseObject(textWebSocketFrame.text());
			} catch (Exception e) {
				log.error(String.format("[%s]>> cannot parse RenderingServer message:%s", 1008, e));
				ctx.close();
			}
			if (rawMsg == null) {
				return;
			}
			

			if (!rawMsg.containsKey("type")) {
				log.error("Invalid RenderingServer message:" + textWebSocketFrame.text());
				return;
			}

			// log.info(String.format(" <- Node【%s】: %s", ctx.channel().remoteAddress(),
			// textWebSocketFrame.text()));
			String type = rawMsg.getString("type");
			if (StrKit.equals(type, "ping")) {
				DispatchUtil.sendPing(ctx, rawMsg.getLong("time"));
				return;
			}

			if (!rawMsg.containsKey("data")) {
				log.error("Invalid RenderingServer message. The message must contain a data field. ");
				return;
			}

			//节点配置
			if (StrKit.equals(type, "renderingConfig")) {
				DispatchUtil.handleConfig(ctx, rawMsg);
			}
			else if(StrKit.equals(type, "GPUs")) {
				DispatchUtil.handleGPUS(ctx, rawMsg);
			}
			else if(StrKit.equals(type, "Streamers")) {
				DispatchUtil.handleStreamers(ctx, rawMsg);
			}
			else if(StrKit.equals(type, "onStreamerConnected")) {
				DispatchUtil.onStreamerConnected(ctx, rawMsg);
			}
			else if(StrKit.equals(type, "onStreamerDisconnected")) {
				DispatchUtil.onStreamerDisconnected(ctx, rawMsg);
			}
			else if(StrKit.equals(type, "onRenderingMonitor")) {
				DispatchUtil.onRenderingMonitor(ctx, rawMsg);
			}
			else if (StrKit.equals(type, "disconnectRenderingServer")) {
			} else {
				log.error("unsupported RenderingServer message type:%s", type);
				ctx.close();
			}

		}
	}

	/**
	 * 向客户端发送心跳
	 * 
	 * @param ctx
	 * @param evt
	 * @throws Exception
	 */
	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		if (evt instanceof IdleStateEvent) {
			IdleStateEvent event = (IdleStateEvent) evt;
			if (event.state().equals(IdleState.READER_IDLE)) {
				DispatchUtil.sendPing(ctx, System.currentTimeMillis());
			} else if (event.state().equals(IdleState.WRITER_IDLE)) {
				DispatchUtil.sendPing(ctx, System.currentTimeMillis());
			} else if (event.state().equals(IdleState.ALL_IDLE)) {
				DispatchUtil.sendPing(ctx, System.currentTimeMillis());
			}
		}
	}

}
