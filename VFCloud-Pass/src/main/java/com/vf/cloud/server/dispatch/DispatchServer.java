package com.vf.cloud.server.dispatch;

import com.jfinal.kit.StrKit;
import com.vf.cloud.pass.common.constant.Cache;
import com.vf.cloud.pass.common.vo.Dispatch;
import com.vf.cloud.server.dispatch.server.Server;

public class DispatchServer {
	private static volatile DispatchServer INSYANCE;

	private Server server;

	private DispatchServer() {
		server = new Server();
		server.reconnect();
	}

	public static DispatchServer getInstance() {
		if (null == INSYANCE) {
			synchronized (DispatchServer.class) {
				if (null == INSYANCE) {
					INSYANCE = new DispatchServer();
				}
			}
		}
		return INSYANCE;
	}

	public void run() {
		if (server != null) {
			server.run();
		}
	}

	public void restart(Dispatch dispatch) {
		if (dispatch != null) {
			Cache.dispatch = dispatch;
			stop();
		}
	}

	public void stop() {
		if (server != null) {
			server.stop();
		}
	}

	public int getStatus() {
		if (server != null) {
			return server.getStatus();
		}
		return -1;
	}

	public void destroy() {
		if (server != null) {
			server.destroy();
		}
	}

	public boolean verify() {
		if (Cache.dispatch == null) {
			return false;
		}
		if (Cache.dispatch.getPort() == 0) {
			return false;
		}
		if (StrKit.isBlank(Cache.dispatch.getSecretKey())) {
			return false;
		}
		return true;
	}
	
	public boolean sendSynScene() {
		return true;
	}
}
