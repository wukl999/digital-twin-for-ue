package com.vf.cloud.server.redis;
import com.vf.cloud.server.redis.server.Server;


public class RedisServer {
	

	private static volatile RedisServer INSYANCE;

	private Server server;

	private RedisServer() {
		server = new Server();
	}

	public static RedisServer getInstance() {
		if (null == INSYANCE) {
			synchronized (RedisServer.class) {
				if (null == INSYANCE) {
					INSYANCE = new RedisServer();
				}
			}
		}
		return INSYANCE;
	}

	public void run() {
		if (server != null) {
			server.run();
		}
	}

	public boolean isRunning() {
		return server.isRunning();
	}

}
