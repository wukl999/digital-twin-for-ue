package com.vf.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.scheduling.annotation.EnableAsync;
import com.vf.cloud.common.constant.SoftPath;
import com.vf.cloud.server.mongodb.MongodbServer;
import com.vf.cloud.server.redis.RedisServer;

import java.io.File;
import java.util.UUID;

@EnableAsync
@SpringBootApplication
public class VfCloudApplication {

	public static void main(String[] args) {
		
		RedisServer.getInstance().run();
		RedisServer.getInstance().isRunning();
		
		MongodbServer.getInstance().run();
		MongodbServer.getInstance().isRunning();

		//getSoftPath(); //发布时启用
		SpringApplication.run(VfCloudApplication.class, args);

	}

	public static void getSoftPath() {
		ApplicationHome ah = new ApplicationHome(VfCloudApplication.class);
		File file = ah.getSource();
		SoftPath.path = file.getParentFile().getParent();
		SoftPath.path = SoftPath.path.replace("\\", "/");
	}

	public static int getOrderNo() {
		Integer uuid = UUID.randomUUID().toString().replaceAll("-", "").hashCode();
		uuid = uuid < 0 ? -uuid : uuid;// String.hashCode() 值会为空

		return uuid;
	}

}
