package com.vf.cloud.common.config;

import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Configuration;

import com.vf.cloud.common.pool.ProssPool;

@Configuration
public class ShutdownConfiguration {

    public ShutdownConfiguration() {
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("==============================");
        ProssPool.stop();
        System.out.println("==============================");
    }

}