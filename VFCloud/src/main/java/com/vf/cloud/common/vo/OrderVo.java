package com.vf.cloud.common.vo;

public class OrderVo {

	private String url;
	private String renderingEngine;
	private String ARType = "None";
	private long expiresIn = 55199534;
	private String fitMode = "any";
	private boolean enableMenuBall;
	private boolean enableVoiceFn = false;
	private String projectId;
	private String appId;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRenderingEngine() {
		return renderingEngine;
	}

	public void setRenderingEngine(String renderingEngine) {
		this.renderingEngine = renderingEngine;
	}

	public String getARType() {
		return ARType;
	}

	public void setARType(String aRType) {
		ARType = aRType;
	}

	public long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getFitMode() {
		return fitMode;
	}

	public void setFitMode(String fitMode) {
		this.fitMode = fitMode;
	}

	public boolean isEnableMenuBall() {
		return enableMenuBall;
	}

	public void setEnableMenuBall(boolean enableMenuBall) {
		this.enableMenuBall = enableMenuBall;
	}

	public boolean isEnableVoiceFn() {
		return enableVoiceFn;
	}

	public void setEnableVoiceFn(boolean enableVoiceFn) {
		this.enableVoiceFn = enableVoiceFn;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

}
