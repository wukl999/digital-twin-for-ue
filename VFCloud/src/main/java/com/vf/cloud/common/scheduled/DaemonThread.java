package com.vf.cloud.common.scheduled;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
@EnableScheduling
public class DaemonThread {
	
    //@Scheduled(cron = "0 40 23 * * ? ")   //每1秒执行一次 //0 40 23 * * ?  
	/**
	 * 每5s检测ue客户端是否正常使用
	 * @throws Exception
	 */
    @Scheduled(fixedRate = 5000)
    @Async //异步执行
    public void scheduled() throws Exception {
     //   CachePool.checkActiveUEClient();
    }

}
