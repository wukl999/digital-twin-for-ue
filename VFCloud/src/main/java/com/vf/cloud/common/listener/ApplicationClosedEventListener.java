package com.vf.cloud.common.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;
import com.vf.cloud.common.pool.ProssPool;
import com.vf.cloud.server.colony.MasterServer;
import com.vf.cloud.server.colony.NodeServer;
import com.vf.cloud.server.nginx.NginxServer;
import com.vf.cloud.server.relay.RelayServer;
import com.vf.cloud.server.signalling.SignallingServer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ApplicationClosedEventListener implements ApplicationListener<ContextClosedEvent>{
	@Override
	public void onApplicationEvent(ContextClosedEvent event) {
		NginxServer.getInstance().stop();
		log.info("NginxServer>>> stop.");
		SignallingServer.getInstance().destroy();
		
		log.info("SignallingServer>>> destroy.");
		NodeServer.getInstance().destroy();		
		MasterServer.getInstance().destroy();
		RelayServer.getInstance().stop();
		log.info("RelayServer>>> stop.");
		
//		ArtemisServer.getInstance().stop();
//		log.info("ArtemisServer>>> stop.");
		
		ProssPool.stop();
		
	}


}
