package com.vf.cloud.common.mapping;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.vf.cloud.common.domain.*;

public class _MappingKit {

	public static void mapping(ActiveRecordPlugin arp) {

		arp.addMapping("sys_dept", "id", Dept.class);
		arp.addMapping("sys_dict", "id", Dict.class);
		arp.addMapping("sys_dict_item", "id", DictItem.class);
		arp.addMapping("sys_log", "id", Log.class);
		arp.addMapping("sys_menu", "id", Menu.class);
		arp.addMapping("sys_role", "id", Role.class);
		// Composite Primary Key order: role_id,menu_id
		arp.addMapping("sys_role_menu", "role_id,menu_id", RoleMenu.class);
		arp.addMapping("sys_user", "id", User.class);
		// Composite Primary Key order: user_id,dept_id
		arp.addMapping("sys_user_dept", "user_id,dept_id", UserDept.class);
		// Composite Primary Key order: user_id,role_id
		arp.addMapping("sys_user_role", "user_id,role_id", UserRole.class);

		arp.addMapping(Server.TABLE_NAME, "id", Server.class);
		arp.addMapping(Gpu.TABLE_NAME, "id", Gpu.class);
		arp.addMapping(Project.TABLE_NAME, "id", Project.class);
		arp.addMapping(Client.TABLE_NAME, "id", Client.class);
		arp.addMapping(Config.TABLE_NAME, "id", Config.class);
		arp.addMapping(DocApi.TABLE_NAME, "id", DocApi.class);
		arp.addMapping(DocApiFile.TABLE_NAME, "id", DocApiFile.class);

	}
}
