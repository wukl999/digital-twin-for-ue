package com.vf.cloud.common.util;


import lombok.*;
import lombok.experimental.Accessors;
import java.io.Serializable;
import javax.servlet.http.HttpServletResponse;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class R<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private int code;

	@Getter
	@Setter
	private String message;

	@Getter
	@Setter
	private T result;

	public static <T> R<T> ok() {
		return restResult(HttpServletResponse.SC_OK, " 成功.", null);
	}

	public static <T> R<T> ok(T data) {
		return restResult(HttpServletResponse.SC_OK, " 成功.", data);
	}

	public static <T> R<T> ok(String msg, T data) {
		return restResult(HttpServletResponse.SC_OK, msg, data);
	}

	public static <T> R<T> failed() {
		return restResult(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "失败.", null);
	}

	public static <T> R<T> failed(String msg) {
		return restResult(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, msg, null);
	}

	public static <T> R<T> failed(T data) {
		return restResult(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "失败.", data);
	}

	public static <T> R<T> failed(String msg, T data) {
		return restResult(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, msg, data);
	}

	public static <T> R<T> failed(int code, String msg) {
		return restResult(code, msg, null);
	}

	public static <T> R<T> failed(int code, String msg, T data) {
		return restResult(code, msg, data);
	}
	
	public static <T> R<T> unauthorized() {
		return restResult(HttpServletResponse.SC_UNAUTHORIZED, "认证失败，请重新登录.", null);
	}
	
	public static <T> R<T> unauthorized(String msg) {
		return restResult(HttpServletResponse.SC_UNAUTHORIZED, msg, null);
	}
	
	public static <T> R<T> noLogin() {
		return restResult(HttpServletResponse.SC_UNAUTHORIZED, "未检测登录信息，请重新登录!", null);
	}
	
	public static <T> R<T> noLogin(String msg) {
		return restResult(HttpServletResponse.SC_UNAUTHORIZED, msg, null);
	}

	public static <T> R<T> restResult(int code, String msg, T data) {
		R<T> apiResult = new R<>();
		apiResult.setCode(code);
		apiResult.setResult(data);
		apiResult.setMessage(msg);
		return apiResult;
	}

}
