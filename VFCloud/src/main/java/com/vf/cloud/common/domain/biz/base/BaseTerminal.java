package com.vf.cloud.common.domain.biz.base;

import com.jfinal.plugin.activerecord.Model;

import java.time.LocalDateTime;

import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseTerminal<M extends BaseTerminal<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setName(java.lang.String name) {
		set("name", name);
		return (M)this;
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public M setIp(java.lang.String ip) {
		set("ip", ip);
		return (M)this;
	}
	
	public java.lang.String getIp() {
		return getStr("ip");
	}

	public M setPort(java.lang.String port) {
		set("port", port);
		return (M)this;
	}
	
	public java.lang.String getPort() {
		return getStr("port");
	}

	public M setScenePath(java.lang.String scenePath) {
		set("scene_path", scenePath);
		return (M)this;
	}
	
	public java.lang.String getScenePath() {
		return getStr("scene_path");
	}

	public M setGpuType(java.lang.String gpuType) {
		set("gpu_type", gpuType);
		return (M)this;
	}
	
	public java.lang.String getGpuType() {
		return getStr("gpu_type");
	}

	public M setGpuMemoryTotal(java.lang.String gpuMemoryTotal) {
		set("gpu_memory_total", gpuMemoryTotal);
		return (M)this;
	}
	
	public java.lang.String getGpuMemoryTotal() {
		return getStr("gpu_memory_total");
	}

	public M setStatus(java.lang.String status) {
		set("status", status);
		return (M)this;
	}
	
	public java.lang.String getStatus() {
		return getStr("status");
	}

	public M setCreateTime(LocalDateTime createTime) {
		set("create_time", createTime);
		return (M)this;
	}
	
	public LocalDateTime getCreateTime() {
		return get("create_time");
	}

	public M setUpdateTime(LocalDateTime updateTime) {
		set("update_time", updateTime);
		return (M)this;
	}
	
	public LocalDateTime getUpdateTime() {
		return get("update_time");
	}

	public M setOnlineTime(java.lang.String onlineTime) {
		set("online_time", onlineTime);
		return (M)this;
	}
	
	public java.lang.String getOnlineTime() {
		return getStr("online_time");
	}

	public M setMac(java.lang.String mac) {
		set("mac", mac);
		return (M)this;
	}
	
	public java.lang.String getMac() {
		return getStr("mac");
	}

	public M setGpuAmount(java.lang.Integer gpuAmount) {
		set("gpu_amount", gpuAmount);
		return (M)this;
	}
	
	public java.lang.Integer getGpuAmount() {
		return getInt("gpu_amount");
	}

	public M setPhysicsMemory(java.lang.String physicsMemory) {
		set("physics_memory", physicsMemory);
		return (M)this;
	}
	
	public java.lang.String getPhysicsMemory() {
		return getStr("physics_memory");
	}

	public M setConfig(java.lang.String config) {
		set("config", config);
		return (M)this;
	}
	
	public java.lang.String getConfig() {
		return getStr("config");
	}

}
