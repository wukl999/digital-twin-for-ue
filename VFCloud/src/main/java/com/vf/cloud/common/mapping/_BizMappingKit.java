package com.vf.cloud.common.mapping;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.vf.cloud.common.domain.biz.*;

public class _BizMappingKit {

	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping(Scene.TABLE_NAME, "id", Scene.class);
		arp.addMapping(Terminal.TABLE_NAME, "id", Terminal.class);
	}
}
