package com.vf.cloud.common.domain.biz;

import com.vf.cloud.common.domain.biz.base.BaseScene;

@SuppressWarnings("serial")
public class Scene extends BaseScene<Scene> {
	public static final Scene dao = new Scene().dao();
	public static final String TABLE_NAME = "biz_scene";
}
