package com.vf.cloud.api.sys.controller;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.jfinal.kit.StrKit;
import com.vf.cloud.common.datasource.annotation.Tx;
import com.vf.cloud.common.domain.Dict;
import com.vf.cloud.common.domain.DictItem;
import com.vf.cloud.common.util.DateUtil;
import com.vf.cloud.common.util.R;
import com.vf.cloud.common.util.UuidUtil;

@RestController
@RequestMapping("/sys/dict")
public class DictController {

	@GetMapping("/list")
	public R<List<Dict>> list(@RequestParam(name = "rootId") String rootId) {
		List<Dict> list = Dict.dao.list(rootId);
		return R.ok(list);
	}

	@Tx
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public R<Dict> save(Dict model) {
		try {
			if (StrKit.isBlank(model.getName())) {
				return R.failed("名称不可以为空！");
			}
			
			//SimpleUser simpleUser=SecurityUtils.getUser();
			if (StrKit.notBlank(model.getId())) {
//				if(simpleUser!=null) {
//					model.setUpdateBy(simpleUser.getUsername());
//				}
				model.setUpdateTime(DateUtil.getLocalDateTime());
				if (model.update()) {
					return R.ok(model);
				} else {
					return R.failed("更新失败！");
				}
			} else {
				model.setId(UuidUtil.getUUID());
//				if(simpleUser!=null) {
//					model.setCreateBy(simpleUser.getUsername());
//				}
				model.setCreateTime(DateUtil.getLocalDateTime());
				if (StrKit.isBlank(model.getParentId()))
					model.setParentId("0");
				if (model.save()) {
					return R.ok(model);
				} else {
					return R.failed("保存失败！");
				}
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@Tx
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public R<String> delete(@RequestParam(name = "id") String id) throws Exception {
		try {
			if (StrKit.equals(id, "101")) {
				return R.failed("系统默认项不可以删除!");
			}
			List<Dict> list = Dict.dao.list(id);
			if (list.size() <= 0) {
				Dict dept = Dict.dao.findById(id);
				if (dept != null) {
					if (dept.delete()) {
						return R.ok();
					} else {
						return R.failed("删除失败！");
					}
				} else {
					return R.failed("数据不存在,请刷新后再试!");
				}
			} else {
				return R.failed("有子节点,不允许删除!");
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@RequestMapping(value = "/getSelectTree", method = RequestMethod.GET)
	public R<List<Dict>> getSelectTree() {
		return R.ok(Dict.dao.getTree("0"));
	}
	
	
	@GetMapping("/findDataList")
	public R<List<DictItem>> findDataList(@RequestParam(name = "dictId") String dictId) {
		return R.ok(DictItem.dao.find("SELECT * FROM sys_dict_item WHERE dict_id=? order by sort asc ",dictId));
	}
	
	
	@Tx
	@RequestMapping(value = "/saveData", method = RequestMethod.POST)
	public R<DictItem> saveData(DictItem model) {
		try {
			if (StrKit.isBlank(model.getLabel())) {
				return R.failed("标签不可以为空！");
			}
			if (StrKit.notBlank(model.getId())) {
				if (model.update()) {
					return R.ok(model);
				} else {
					return R.failed("失败！");
				}
			} else {
				model.setId(UuidUtil.getUUID());
				if (model.save()) {
					return R.ok(model);
				} else {
					return R.failed("失败！");
				}
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}
	

	@Tx
	@RequestMapping(value = "/deleteData", method = RequestMethod.GET)
	public R<String> deleteData(@RequestParam(name = "id") String id) throws Exception {
		try {
			if (DictItem.dao.deleteById(id)) {
				return R.ok();
			} else {
				return R.failed("失败！");
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/findItems", method = RequestMethod.GET)
	public R<Dict> findItems(@RequestParam(name = "dictCode") String dictCode){
		Dict dict = Dict.dao.findByCode(dictCode);
		if (dict != null) {
			List<DictItem> items = DictItem.dao.find("select * from " + DictItem.TABLE_NAME + " where DICT_ID=? order by sort asc ",dict.getId());
			dict.setItems(items);
			return R.ok(dict);
		}else {
			return R.failed();
		}
	}
	
}
