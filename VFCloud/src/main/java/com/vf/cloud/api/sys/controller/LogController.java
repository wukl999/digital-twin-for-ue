package com.vf.cloud.api.sys.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.cloud.common.domain.Log;
import com.vf.cloud.common.util.R;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/log")
public class LogController {

	@PostMapping
	public R<Boolean> save(@RequestBody Log sysLog) {
		if (sysLog.save()) {
			return R.ok();
		} else {
			return R.failed();
		}
	}

	@GetMapping
	public R<Page<Log>> listDatas(@RequestBody Log sysLog) {
		return null;
	}

	// @SysLog(value = "查询日志列表")
	@GetMapping(value = { "/findList" }, produces = "application/json; charset=utf-8")
	public R<Page<Log>> findList(@RequestParam(name = "keywords", required = false, defaultValue = "") String keywords,
			@RequestParam(name = "username", required = false, defaultValue = "") String username,
			@RequestParam(name = "type", required = false, defaultValue = "") String type,
			@RequestParam(name = "datetimerange", required = false, defaultValue = "") String datetimerange,
			@RequestParam(name = "remoteIp", required = false, defaultValue = "") String remoteIp,
			@RequestParam(name = "cur", required = false, defaultValue = "1") int cur,
			@RequestParam(name = "limit", required = false, defaultValue = "10") int limit) throws Exception {

		String sqlExceptSelect = " FROM " + Log.TABLE_NAME + " o  WHERE del_flag='0' ";

		if (!StrKit.isBlank(username)) {
			sqlExceptSelect += " AND O.CREATE_BY LIKE '%" + username + "%' ";
		}

		if (!StrKit.isBlank(type)) {
			sqlExceptSelect += " AND O.TYPE='" + type + "' ";
		}

		if (!StrKit.isBlank(remoteIp)) {
			sqlExceptSelect += " AND O.REMOTE_IP='" + remoteIp + "' ";
		}

		if (!StrKit.isBlank(datetimerange)) {
			String[] datetimeranges = datetimerange.split(",");
			if (datetimeranges != null && datetimeranges.length == 2) {
				sqlExceptSelect += "  and  (  DATE_FORMAT('" + datetimeranges[0]
						+ "','%Y%m%d%H%i') <= DATE_FORMAT(O.CREATE_TIME,'%Y%m%d%H%i')  and  DATE_FORMAT('"
						+ datetimeranges[1] + "','%Y%m%d%H%i') >= DATE_FORMAT(O.CREATE_TIME,'%Y%m%d%H%i') )  ";
			}
		}

		sqlExceptSelect += "  ORDER BY   O.CREATE_TIME desc ";
		Page<Log> roles = Log.dao.paginate(cur, limit,
				"select id,type,title,method,request_uri,user_agent,response_code,response_message,time,create_time,create_by,update_by,remote_ip ",
				sqlExceptSelect);
		return R.ok(roles);
	}

	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public R<Log> getById(@RequestParam(name = "id") String id) {
		Log log = Log.dao.findById(id);
		if (log != null) {
			return R.ok(log);
		} else {
			return R.failed("未查询到数据，请刷新后尝试!");
		}
	}

}
