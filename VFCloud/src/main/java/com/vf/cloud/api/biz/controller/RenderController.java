package com.vf.cloud.api.biz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.vf.cloud.api.biz.service.IRender;
import com.vf.cloud.common.util.R;
import com.vf.cloud.common.vo.OrderParamVo;
import com.vf.cloud.common.vo.OrderVo;

@RestController
public class RenderController {

	@Autowired
	private IRender iRender;

	/**
	 * 集群模式请求资源
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/Renderers/Colony/order", method = RequestMethod.POST)
	public R<OrderVo> ColonyOrder(@RequestBody OrderParamVo param) {
		return iRender.ColonyOrder(param);
	}

	/**
	 * 单机模式请求资源
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/Renderers/Any/order", method = RequestMethod.POST)
	public R<OrderVo> AnyOrder(@RequestBody OrderParamVo param) {
		return iRender.AnyOrder(param);
	}
}
