package com.vf.cloud.api.biz.service;

import com.vf.cloud.common.repository.base.Thumbnail;

public interface IThumbnail {

	Thumbnail save(Thumbnail entity);

	void deleteById(String id);

	Thumbnail findById(String id);

}
