package com.vf.cloud.api.biz.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vf.cloud.api.biz.service.IThumbnail;
import com.vf.cloud.common.repository.ThumbnailRepository;
import com.vf.cloud.common.repository.base.Thumbnail;

@Service
public class ThumbnailImpl implements IThumbnail {

	@Autowired
	private ThumbnailRepository thumbnailRepository;

	@Override
	public Thumbnail save(Thumbnail entity) {
		return thumbnailRepository.save(entity);
	}

	@Override
	public void deleteById(String id) {
		thumbnailRepository.deleteById(id);
	}

	@Override
	public Thumbnail findById(String id) {
		Optional<Thumbnail> optional = thumbnailRepository.findById(id);
		if (optional.isPresent())
			return optional.get();
		return null;
	}

}
