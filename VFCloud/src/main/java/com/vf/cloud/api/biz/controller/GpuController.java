package com.vf.cloud.api.biz.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.cloud.common.datasource.annotation.Tx;
import com.vf.cloud.common.domain.Gpu;
import com.vf.cloud.common.util.R;
import com.vf.cloud.common.util.UuidUtil;
import com.vf.cloud.server.adapter.AdapterServer;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/gpu")
public class GpuController {

	@GetMapping(value = { "/list" }, produces = "application/json; charset=utf-8")
	public R<Page<Gpu>> findList(@RequestParam(name = "keywords", required = false, defaultValue = "") String keywords,
			@RequestParam(name = "status", required = false, defaultValue = "") String status,
			@RequestParam(name = "cur", required = false, defaultValue = "1") int cur,
			@RequestParam(name = "limit", required = false, defaultValue = "50") int limit) throws Exception {

		String sqlExceptSelect = " FROM " + Gpu.TABLE_NAME + " o  WHERE 1=1 ";
		if (!StrKit.isBlank(keywords)) {
			sqlExceptSelect += " AND ( O.NAME LIKE '%" + keywords + "%' ) ";
		}
		sqlExceptSelect += "  ORDER BY   O.index asc ";
		Page<Gpu> pages = Gpu.dao.paginate(cur, limit, "select * ", sqlExceptSelect);
		AdapterServer.getInstance().pages(pages);
		return R.ok(pages);
	}

	@Tx
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public R<Gpu> save(Gpu model) {
		try {
			if (AdapterServer.getInstance().isRestart()) {
				return R.failed("服务重启中，请稍后再试！");
			}

			if (StrKit.notBlank(model.getId())) {
				if (model.update()) {
					return R.ok(model);
				} else {
					return R.failed("更新失败！");
				}
			} else {
				model.setId(UuidUtil.getUUID());
				if (model.save()) {
					return R.ok(model);
				} else {
					return R.failed("保存失败！");
				}
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@Tx
	@RequestMapping(value = "/deleteById", method = RequestMethod.GET)
	public R<String> deleteById(@RequestParam(name = "id") String id) throws Exception {
		try {

			if (AdapterServer.getInstance().isRestart()) {
				return R.failed("服务重启中，请稍后再试！");
			}

			Gpu role = Gpu.dao.findById(id);
			if (role == null)
				return R.failed("数据不存在，刷新后尝试!");
			if (role.delete()) {
				AdapterServer.getInstance().restart();
				return R.ok("删除成功，已自动重启GPU，请稍后.");
			}
			return R.failed("失败！");
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public R<Gpu> getById(@RequestParam(name = "id") String id) throws Exception {
		try {
			Gpu gpu = Gpu.dao.findById(id);
			if (gpu == null)
				return R.failed("数据不存在，刷新后尝试!");
			return R.ok(gpu);
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@RequestMapping(value = "/restart", method = RequestMethod.GET)
	public R<String> restart() throws Exception {
		try {
			AdapterServer.getInstance().restart();
			return R.ok("发送指令成功，请稍后.");
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

}
