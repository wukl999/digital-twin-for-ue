package com.vf.cloud.api.ue.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.vf.cloud.common.util.R;
import com.vf.cloud.common.vo.DescriptorVo;

@RestController
@RequestMapping("/emit")
public class EmitController {
	
	@RequestMapping(value = "/descriptor", method = RequestMethod.POST)
	public R<String> descriptor(@RequestBody DescriptorVo descriptorVo) {
		try {
			
			if(descriptorVo==null) {
				return R.failed("参数必填.");
			}
			
			
			if(50!=descriptorVo.getType()) {
				return R.failed("Type 暂时只支持50.");
			}
			
			
			
			
			
		 // A generic message has a type and a descriptor.
//		    function emitDescriptor(messageType, descriptor) {
//		        // Convert the dscriptor object into a JSON string.
//		        let descriptorAsString = JSON.stringify(descriptor);
//
//		        // Add the UTF-16 JSON string to the array byte buffer, going two bytes at
//		        // a time.
//		        let data = new DataView(new ArrayBuffer(1 + 2 + 2 * descriptorAsString.length));
//		        let byteIdx = 0;
//		        data.setUint8(byteIdx, messageType);
//		        byteIdx++;
//		        data.setUint16(byteIdx, descriptorAsString.length, true);
//		        byteIdx += 2;
//		        for (i = 0; i < descriptorAsString.length; i++) {
//		            data.setUint16(byteIdx, descriptorAsString.charCodeAt(i), true);
//		            byteIdx += 2;
//		        }
//		        sendInputData(data.buffer);
//		    }
//			
			
			
			
			
			
			
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
		return null;
	}

}
