package com.vf.cloud.api.biz.service;

import com.vf.cloud.common.util.R;
import com.vf.cloud.common.vo.OrderParamVo;
import com.vf.cloud.common.vo.OrderVo;

public interface IRender {
	
	public R<OrderVo> ColonyOrder(OrderParamVo param);
	
	public R<OrderVo> AnyOrder(OrderParamVo param);
	
	
	

}
