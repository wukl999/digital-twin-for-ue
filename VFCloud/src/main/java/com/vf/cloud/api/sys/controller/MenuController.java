package com.vf.cloud.api.sys.controller;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.jfinal.kit.StrKit;
import com.vf.cloud.common.datasource.annotation.Tx;
import com.vf.cloud.common.domain.Menu;
import com.vf.cloud.common.domain.User;
import com.vf.cloud.common.util.DateUtil;
import com.vf.cloud.common.util.R;
import com.vf.cloud.common.util.UuidUtil;

@RestController
@RequestMapping("/sys/menu")
public class MenuController {

	@GetMapping("/findList")
	public R<List<Menu>> findList(@RequestParam(name = "rootId") String rootId) {
		List<Menu> menus = Menu.dao.getTableTree(rootId);
		return R.ok(menus);
	}
	

	@Tx
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public R<Menu> save(Menu menu) {
		try {
			if (StrKit.isBlank(menu.getName())) {
				return R.failed("名称不可以为空！");
			}
			
			//SimpleUser simpleUser=SecurityUtils.getUser();
			
			if (StrKit.notBlank(menu.getId())) {
//				if(simpleUser!=null) {
//					menu.setUpdateBy(simpleUser.getUsername());
//				}
				menu.setUpdateTime(DateUtil.getLocalDateTime());
				if (menu.update()) {
					return R.ok(menu);
				} else {
					return R.failed("更新失败！");
				}
			} else {
				menu.setId(UuidUtil.getUUID());
//				if(simpleUser!=null) {
//					menu.setCreateBy(simpleUser.getUsername());
//				}
				menu.setCreateTime(DateUtil.getLocalDateTime());
				if (StrKit.isBlank(menu.getParentId()))
					menu.setParentId("0");
				if (menu.save()) {
					return R.ok(menu);
				} else {
					return R.failed("保存失败！");
				}
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@Tx
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public R<String> delete(@RequestParam(name = "id") String id) throws Exception {
		try {
			if (StrKit.equals(id, "101")) {
				return R.failed("系统默认项不可以删除!");
			}
			List<Menu> list = Menu.dao.getChildrenByPid(id, "",false);
			if (list.size() <= 0) {
				Menu menu = Menu.dao.findById(id);
				if (menu != null) {
					if (menu.delete()) {
						return R.ok();
					} else {
						return R.failed("删除失败！");
					}
				} else {
					return R.failed("数据不存在,请刷新后再试!");
				}
			} else {
				return R.failed("有子节点,不允许删除!");
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/getByUserId", method = RequestMethod.GET)
    public R<List<Menu>> getByUserId(@RequestParam(name = "userId") String userId){
    	List<Menu> menus;
    	
    	User user=User.dao.findById(userId);
    	if(user!=null && StrKit.equals(user.getUsername(),"admin")) {
    		menus = Menu.dao.getAuthRutes("0",null);
    	}
    	else {
    		menus = Menu.dao.getAuthRutes("0",userId);
    	}
    	return R.ok(menus);
    }
	
	@RequestMapping(value = "/getOptionsTree", method = RequestMethod.GET)
    public R<List<Menu>> getOptionsTree(){
    	return R.ok(Menu.dao.getTree("0", null));
    }
	
	
}
