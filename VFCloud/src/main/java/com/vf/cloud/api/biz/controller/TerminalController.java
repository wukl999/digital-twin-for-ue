package com.vf.cloud.api.biz.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.cloud.api.biz.service.IThumbnail;
import com.vf.cloud.common.datasource.annotation.Tx;
import com.vf.cloud.common.domain.biz.Terminal;
import com.vf.cloud.common.util.GpuUtil;
import com.vf.cloud.common.util.R;
import com.vf.cloud.common.util.UuidUtil;
import com.vf.cloud.common.vo.GpuVo;
import com.vf.cloud.common.vo.TerminalConfigVo;

import cn.hutool.core.net.NetUtil;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/biz/terminal")
public class TerminalController {

	@Autowired
	private IThumbnail thumbnailImpl;

	@GetMapping(value = { "/list" }, produces = "application/json; charset=utf-8")
	public R<Page<Terminal>> findList(
			@RequestParam(name = "keywords", required = false, defaultValue = "") String keywords,
			@RequestParam(name = "status", required = false, defaultValue = "") String status,
			@RequestParam(name = "cur", required = false, defaultValue = "1") int cur,
			@RequestParam(name = "limit", required = false, defaultValue = "10") int limit) throws Exception {

		String sqlExceptSelect = " FROM " + Terminal.TABLE_NAME + " o  WHERE 1=1 ";
		if (!StrKit.isBlank(keywords)) {
			sqlExceptSelect += " AND ( O.NAME LIKE '%" + keywords + "%' ) ";
		}
		sqlExceptSelect += "  ORDER BY   O.UPDATE_TIME asc ";
		Page<Terminal> pages = Terminal.dao.paginate(cur, limit, "select * ", sqlExceptSelect);
		return R.ok(pages);
	}

	@Tx
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public R<Terminal> save(Terminal model) {
		try {

			if (StrKit.notBlank(model.getId())) {
				if (model.update()) {
					return R.ok(model);
				} else {
					return R.failed("更新失败！");
				}
			} else {
				model.setId(UuidUtil.getUUID());
				if (model.save()) {
					return R.ok(model);
				} else {
					return R.failed("保存失败！");
				}
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@Tx
	@RequestMapping(value = "/deleteById", method = RequestMethod.GET)
	public R<String> deleteById(@RequestParam(name = "id") String id) throws Exception {
		try {
			Terminal terminal = Terminal.dao.findById(id);
			if (terminal == null)
				return R.failed("数据不存在，刷新后尝试!");
			if (terminal.delete()) {
				try {
					thumbnailImpl.deleteById(terminal.getId());
				} catch (Exception e) {
				}
				return R.ok();
			}
			return R.failed("失败！");
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public R<Terminal> getById(@RequestParam(name = "id") String id) throws Exception {
		try {
			Terminal sever = Terminal.dao.findById(id);
			if (sever == null)
				return R.failed("数据不存在，刷新后尝试!");
			return R.ok(sever);
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	@RequestMapping(value = "/init", method = RequestMethod.GET)
	public R<Terminal> init() throws Exception {
		try {

			Terminal terminal = Terminal.dao.findById("vfcloud_terminal");

			if (terminal != null) {
				terminal.delete();
			}
			terminal = new Terminal();
			terminal.setId("vfcloud_terminal");

			InetAddress inetAddress = InetAddress.getLocalHost();
			String localMac = NetUtil.getMacAddress(inetAddress);

			List<GpuVo> gpus = GpuUtil.getGPUs();
			terminal.setGpuAmount(gpus.size());
			terminal.setMac(localMac);

			TerminalConfigVo config = new TerminalConfigVo();
			config.setGpus(gpus);

			terminal.setConfig(JsonKit.toJson(config));

			if (terminal.save()) {
				return R.ok(terminal);
			} else {
				return R.failed();
			}
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	public static void main(String[] args) throws UnknownHostException {
		InetAddress inetAddress = InetAddress.getLocalHost();
		// 第二种方式：利用hutool工具类中的封装方法获取本机mac地址
		String localMacAddress2 = NetUtil.getMacAddress(inetAddress);
		System.out.println("localMacAddress2 = " + localMacAddress2);
	}

}
