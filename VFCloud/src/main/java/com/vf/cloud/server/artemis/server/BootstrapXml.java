package com.vf.cloud.server.artemis.server;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import com.jfinal.kit.StrKit;
import com.vf.cloud.common.constant.SoftPath;

public class BootstrapXml {

	public static void main(String[] args) throws IOException {
		try {
			update();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void update() throws DocumentException, IOException {

		// 创建SAXReader对象
		SAXReader sr = new SAXReader();
		Document document = sr
				.read(String.format("%s/artemis/instance/etc/bootstrap.xml",SoftPath.path));
		Element root = document.getRootElement();
		String file = String.format("file:/%s/artemis/instance/etc//broker.xml",SoftPath.path);
		Element server = root.element("server");
		if (server != null) {
			Attribute configurationAttribute = server.attribute("configuration");
			if (StrKit.equals(file, configurationAttribute.getText())) {
				return;
			} else {
				configurationAttribute.setText(file);
			}
		}
		Writer osWrite = new OutputStreamWriter(new FileOutputStream(String.format("%s/artemis/instance/etc/bootstrap.xml",  SoftPath.path)));
		OutputFormat format = OutputFormat.createPrettyPrint();
		format.setEncoding("UTF-8");
		XMLWriter writer = new XMLWriter(osWrite, format);
		writer.write(document);
		writer.close();

	}

}
