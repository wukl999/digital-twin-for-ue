package com.vf.cloud.server.artemis;

import com.vf.cloud.server.artemis.server.Server;

public class ArtemisServer {

	private static volatile ArtemisServer INSYANCE;

	private Server server;

	private ArtemisServer() {
		server = new Server();
	}

	public static ArtemisServer getInstance() {
		if (null == INSYANCE) {
			synchronized (ArtemisServer.class) {
				if (null == INSYANCE) {
					INSYANCE = new ArtemisServer();
				}
			}
		}
		return INSYANCE;
	}

	public void run() {
		if (server != null) {
			server.run();
		}
	}

	public boolean isRunning() {
		return server.isRunning();
	}

	public void stop() {
		if (server != null) {
			server.stop();
		}
	}

}
