package com.vf.cloud.server.mongodb;

import com.vf.cloud.server.mongodb.server.Server;

public class MongodbServer {

	private static volatile MongodbServer INSYANCE;

	private Server mongodbServer;

	private MongodbServer() {
		mongodbServer = new Server();
	}

	public static MongodbServer getInstance() {
		if (null == INSYANCE) {
			synchronized (MongodbServer.class) {
				if (null == INSYANCE) {
					INSYANCE = new MongodbServer();
				}
			}
		}
		return INSYANCE;
	}

	public void run() {
		if (mongodbServer != null) {
			mongodbServer.run();
		}
	}

	public boolean isRunning() {
		return mongodbServer.isRunning();
	}


}
