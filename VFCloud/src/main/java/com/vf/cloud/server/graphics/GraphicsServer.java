package com.vf.cloud.server.graphics;

import com.vf.cloud.server.graphics.server.Server;

public class GraphicsServer {

	private static volatile GraphicsServer INSYANCE;

	private Server server;

	private GraphicsServer() {
		server = new Server();
	}

	public static GraphicsServer getInstance() {
		if (null == INSYANCE) {
			synchronized (GraphicsServer.class) {
				if (null == INSYANCE) {
					INSYANCE = new GraphicsServer();
				}
			}
		}
		return INSYANCE;
	}

	public void getGPU() {
		if (server != null) {
			server.getGPU();
		}
	}

}
