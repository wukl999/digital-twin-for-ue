package com.vf.cloud.server.relay.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import org.springframework.stereotype.Service;

import com.jfinal.kit.StrKit;
import com.vf.cloud.common.constant.Cache;
import com.vf.cloud.common.constant.SoftPath;
import com.vf.cloud.common.pool.ProssPool;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("stunServer")
public class StunServer implements Runnable{

	@Override
	public void run() {
		start();
	}
	
	public void start() {
		
        ExecutorService executor = Executors.newFixedThreadPool(2);
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
            	log.info("STUNServer >>> 正在运行.");
                if(!StrKit.isBlank(ProssPool.getPid(Cache.relay.getStunPort()+""))) {
					log.info("STUNServer >>> 正在运行.");
					return 0;
				}
				
				if (!genStunserverStartBat()) {
					log.error("Stunserver生成启动bat文件失败，请联系管理员.");
					return -1;
				}
				executeCmd(String.format("%s/Relay/Start_STUNServer.bat", SoftPath.path));
                return 0;
            }
        }, executor);
        future.thenAccept(e -> {
        	System.out.println(e);
			File f=new File(String.format("%s/Relay/Start_STUNServer.bat", SoftPath.path));
			if(f.exists()) f.delete();
        });
	}
	
	public void stop() {
		
		if (!genStunserverStopBat()) {
			log.error("Stunserver生成停止bat文件失败，请联系管理员.");
			return;
		}
		int status = executeCmd(String.format("%s/Relay/Stop_STUNServer.bat", SoftPath.path));
		File f=new File(String.format("%s/Relay/Stop_STUNServer.bat", SoftPath.path));
		if (0 != status) {
			log.error("停止STUNServer失败，请联系管理员.");
			f.delete();
			return;
		}
		f.delete();
		log.info("STUNServer >>> 已停止运行.");
	}
	
	private int executeCmd(String command) {
		try {
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec(command);
			new Thread(new ConsoleThread(new BufferedReader(new InputStreamReader(process.getInputStream(), "GBK"))))
					.start();
			return process.waitFor();
		} catch (InterruptedException | IOException e) {
		}
		return -1;
	}

	class ConsoleThread implements Runnable {
		private BufferedReader bfr = null;

		public ConsoleThread(BufferedReader bfr) {
			this.bfr = bfr;
		}

		@Override
		public void run() {
			String line = null;
			try {
				while ((line = bfr.readLine()) != null) {
					log.info(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean genStunserverStartBat() {
		BufferedWriter out = null;
		try {
			try {
				File f = new File(String.format("%s/Relay/Start_STUNServer.bat", SoftPath.path));
				if (f.exists()) {
					f.delete();
				}
				out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, true)));
				out.write("@echo off \r\n");
				out.write(String.format("%s \r\n", SoftPath.path.substring(0, 2)));
				out.write(String.format("cd %s/Relay \r\n", SoftPath.path));
				out.write(String.format("stunserver.exe 0.0.0.0:%s \r\n", Cache.relay.getStunPort()));
				out.write("echo \"startStunserver1999\" \r\n");
				out.write("exit");
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null)
					out.close();
			} catch (IOException e) {
			}
		}
		return false;
	}
	
	
	public boolean genStunserverStopBat() {
		BufferedWriter out = null;
		try {
			try {
				File f = new File(String.format("%s/Relay/Stop_STUNServer.bat", SoftPath.path));
				if (f.exists()) {
					f.delete();
				}
				out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, true)));

				out.write("@echo off \r\n");
				out.write(String.format("%s \r\n", SoftPath.path.substring(0, 2)));
				out.write(String.format("cd %s/Relay \r\n", SoftPath.path));
				out.write("tasklist | findstr /i \"stunserver.exe\" \r\n");
				out.write("@echo \"stunserver is running, stopping...\"  \r\n");
				out.write("TASKKILL /F /IM stunserver.exe /T \r\n");
				out.write("echo \"stopStunserver1999\" \r\n");
				out.write("exit");
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null)
					out.close();
			} catch (IOException e) {
			}
		}
		return false;
	}
	

}
