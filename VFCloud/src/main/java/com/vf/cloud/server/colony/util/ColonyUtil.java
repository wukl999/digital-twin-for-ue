package com.vf.cloud.server.colony.util;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.vf.cloud.common.constant.Cache;
import com.vf.cloud.common.domain.Client;
import com.vf.cloud.common.vo.GPUSynVo;
import com.vf.cloud.server.colony.pool.NodePool;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public class ColonyUtil {

	public static void disconnected(String channelId) {
		NodePool.removeNode(channelId);
	}

	public static void sendPing(ChannelHandlerContext ctx, long time) {
		Map<String, Object> clientConfig = new HashMap<String, Object>();
		clientConfig.put("type", "pong");
		clientConfig.put("time", time);
		send(ctx, JsonKit.toJson(clientConfig));
	}
	
	
	public static void sendPingForNode(ChannelHandlerContext ctx, long time) {
		Map<String, Object> clientConfig = new HashMap<String, Object>();
		clientConfig.put("type", "ping");
		clientConfig.put("time", time);
		send(ctx, JsonKit.toJson(clientConfig));
	}
	

	public static void sendSuccess(ChannelHandlerContext ctx, long time) {
		Map<String, Object> success = new HashMap<String, Object>();
		success.put("type", "event");

		Map<String, Object> data = new HashMap<String, Object>();
		data.put("func_name", "OnSuccess");
		data.put("time", time);
		success.put("data", data);
		send(ctx, JsonKit.toJson(success));
	}

	public static void sendFail(ChannelHandlerContext ctx, long time, String message) {
		Map<String, Object> failed = new HashMap<String, Object>();
		failed.put("type", "event");
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("func_name", "OnFailed");
		data.put("time", time);
		data.put("message", message);
		failed.put("data", data);
		send(ctx, JsonKit.toJson(failed));
	}

	public static void send(ChannelHandlerContext ctx, String json) {
		if (ctx != null) {
			ctx.channel().writeAndFlush(new TextWebSocketFrame(json)).addListener(new ChannelFutureListener() {
				@Override
				public void operationComplete(ChannelFuture future) throws Exception {
					if (future.isSuccess()) {
					} else {
					}
				}
			});
		}
	}

	public static void send(ChannelHandlerContext ctx, String json, long time) {
		if (ctx != null) {
			ctx.channel().writeAndFlush(new TextWebSocketFrame(json)).addListener(new ChannelFutureListener() {
				@Override
				public void operationComplete(ChannelFuture future) throws Exception {
					if (future.isSuccess()) {
						sendSuccess(ctx, time);
					} else {
					}
				}
			});
		}
	}

	/**
	 * 处理同步消息
	 * 
	 * @param rawMsg
	 */
	public static void handleSyn(ChannelHandlerContext ctx, JSONObject rawMsg) {
	}

	public static void handleGPUState(ChannelHandlerContext ctx, JSONObject rawMsg) {
		
		
		GPUSynVo vo=JSONObject.parseObject(rawMsg.getString("data"), GPUSynVo.class);
		
		if (vo==null) {
			sendFail(ctx, rawMsg.getLong("time"), "无效消息.");
			return;
		}
		Client client = Client.dao.findById(vo.getKey());
		if (client == null) {
			client = new Client();
			client.setId(vo.getKey());
			client.setName(vo.getIp());
			
			if(StrKit.equals(vo.getKey(),Cache.config.getLocalPwd())) {
				client.setType("1");
			}else {
				client.setType("0");
			}
			
			client.setPassword(vo.getKey());
			client.setIp(vo.getIp());
			client.setPort(vo.getPort()+"");
			client.setLimit(vo.getLimit());
			client.setSurplus(vo.getFree());
			client.setUsed(vo.getUsed());
			if (client.save()) {
				sendSuccess(ctx, rawMsg.getLong("time"));
			} else {
				sendFail(ctx, rawMsg.getLong("time"), "同步失败.");
			}
			return;
		}
		client.setName(vo.getIp());
		if(StrKit.equals(vo.getKey(),Cache.config.getLocalPwd())) {
			client.setType("1");
		}else {
			client.setType("0");
		}
		client.setPassword(vo.getKey());
		client.setIp(vo.getIp());
		client.setPort(vo.getPort()+"");
		client.setLimit(vo.getLimit());
		client.setSurplus(vo.getFree());
		client.setUsed(vo.getUsed());
		if (client.update()) {
			sendSuccess(ctx, rawMsg.getLong("time"));
		} else {
			sendFail(ctx, rawMsg.getLong("time"), "同步失败.");
		}
	}

}
