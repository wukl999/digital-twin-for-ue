package com.vf.cloud.server.nginx.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import com.vf.cloud.common.constant.SoftPath;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Server implements Runnable {

	/**
	 * 0-停止 1-正在启动 2-运行中 3-停止中
	 * 
	 */
	private int status = 0;

	@Override
	public void run() {
		start();
	}

	public boolean stop() {
		status = 3;
		if (!genStopBat()) {
			log.error("生成启动必须文件失败，请联系管理员.");
			return false;
		}
		isCmd = false;
		int status = executeCmd(String.format("%s/nginx/stop.bat", SoftPath.path));
		File f = new File(String.format("%s/nginx/stop.bat", SoftPath.path));
		if (0 != status) {
			f.delete();
			log.error("停止nginx失败，请联系管理员.");
			return false;
		}
		isCmdComplete();
		f.delete();
		status = 0;
		return true;
	}

	public void start() {
		stop();

		status = 1;
		if (!genStartBat()) {
			log.error("生成启动bat文件失败，请联系管理员.");
			return;
		}

		isCmd = false;
		int status = executeCmd(String.format("%s/nginx/start.bat", SoftPath.path));
		File f = new File(String.format("%s/nginx/start.bat", SoftPath.path));
		if (0 != status) {
			log.error("启动nginx失败，请联系管理员.");
			f.delete();
			return;
		}
		isCmdComplete();
		f.delete();
		status = 2;
		log.info("NginxServer >>> 启动成功.");
	}

	private int executeCmd(String command) {
		try {
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec("cmd /c start /b  " + command);
			new Thread(new ConsoleThread(new BufferedReader(new InputStreamReader(process.getInputStream(), "GBK"))))
					.start();
			return process.waitFor();
		} catch (InterruptedException | IOException e) {
		}
		return -1;
	}

	class ConsoleThread implements Runnable {

		private BufferedReader bfr = null;

		public ConsoleThread(BufferedReader bfr) {
			this.bfr = bfr;
		}

		@Override
		public void run() {
			String line = null;
			try {
				while ((line = bfr.readLine()) != null) {
					log.info(line);
					if (line.contains("startNginx1999")) {
						status = 2;
						break;
					} else if (line.contains("stopNginx1999")) {
						status = 0;
						break;
					}
				}
				isCmd = true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private boolean isCmd = false;

	public void isCmdComplete() {
		while (true) {
			try {
				if (isCmd) {
					break;
				}
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 写入配置文件
	 */
	public boolean genStartBat() {
		BufferedWriter out = null;
		try {
			try {
				File f = new File(String.format("%s/nginx/start.bat", SoftPath.path));
				if (f.exists()) {
					f.delete();
				}
				out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, true)));
				out.write("@echo off \r\n");
				out.write(String.format("%s \r\n", SoftPath.path.substring(0, 2)));
				out.write(String.format("cd %s/nginx \r\n", SoftPath.path));
				out.write("start nginx \r\n");
				out.write("echo \"startNginx1999\" \r\n");
				out.write("exit");
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null)
					out.close();
			} catch (IOException e) {
			}
		}
		return false;
	}

	public boolean genStopBat() {
		BufferedWriter out = null;
		try {
			try {
				File f = new File(String.format("%s/nginx/stop.bat", SoftPath.path));
				if (f.exists()) {
					f.delete();
				}
				out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, true)));

				out.write("@echo off \r\n");
				out.write(String.format("set nginx_path=%s/nginx \r\n", SoftPath.path));
				out.write("echo nginxpath: %nginx_path% \r\n");
				out.write("cd %nginx_path% \r\n");
				out.write("tasklist | findstr /i \"nginx.exe\" \r\n");
				out.write("@echo \"nginx is running, stopping...\"  \r\n");
				out.write("rem nginx -s stop \r\n");
				out.write("TASKKILL /F /IM nginx.exe /T \r\n");
				out.write("echo \"stopNginx1999\" \r\n");
				out.write("exit");

				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null)
					out.close();
			} catch (IOException e) {
			}
		}
		return false;
	}

	public int getStatus() {
		return status;
	}

}
