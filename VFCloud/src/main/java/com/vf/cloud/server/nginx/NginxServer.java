package com.vf.cloud.server.nginx;

import com.vf.cloud.server.nginx.server.Server;

public class NginxServer {

	private static volatile NginxServer INSYANCE;

	private Server server;

	private NginxServer() {
		server = new Server();
	}

	public static NginxServer getInstance() {
		if (null == INSYANCE) {
			synchronized (NginxServer.class) {
				if (null == INSYANCE) {
					INSYANCE = new NginxServer();
				}
			}
		}
		return INSYANCE;
	}

	public void run() {
		if (server != null) {
			server.run();
		}
	}

	public boolean stop() {
		return server.stop();
	}

	public int getStatus() {
		return server.getStatus();
	}
	
}
