package com.vf.cloud.server.colony;

import com.jfinal.kit.StrKit;
import com.vf.cloud.common.constant.Cache;
import com.vf.cloud.common.domain.Config;
import com.vf.cloud.server.colony.server.MServer;

public class MasterServer {
	
	private static volatile MasterServer INSYANCE;

	private MServer server;

	private MasterServer() {
		server = new MServer();
		server.reconnect();
	}

	public static MasterServer getInstance() {
		if (null == INSYANCE) {
			synchronized (MasterServer.class) {
				if (null == INSYANCE) {
					INSYANCE = new MasterServer();
				}
			}
		}
		return INSYANCE;
	}

	public void run() {
		if (server != null) {
			server.run();
		}
	}

	public void restart() {
		if (server != null) {
			server.restart();
		}
	}
	
	public void restart(Config config) {
		if (config != null) {
			Cache.config = config;
			stop();
		}
	}
	

	public void stop() {
		if (server != null) {
			server.stop();
		}
	}

	public int getStatus() {
		if (server != null) {
			return server.getStatus();
		}
		return -1;
	}
	
	public void destroy() {
		if (server != null) {
			server.destroy();
		}
	}

	public boolean verify() {
		if (Cache.config == null) {
			return false;
		}
		if (StrKit.isBlank(Cache.config.getMasterIp())) {
			return false;
		}
		if (StrKit.isBlank(Cache.config.getMasterPort())) {
			return false;
		}
		if (StrKit.isBlank(Cache.config.getMasterPwd())) {
			return false;
		}
		return true;
	}

}
