package com.vf.cloud.server.adapter;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class AdapterThread {

	/**
	 * 每5s检测ue客户端是否正常使用
	 * 
	 * @throws Exception
	 */
	@Scheduled(fixedRate = 5000)
	@Async // 异步执行
	public void scheduled() throws Exception {
		AdapterServer.getInstance().check();
	}

}
