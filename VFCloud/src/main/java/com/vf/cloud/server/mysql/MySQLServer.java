package com.vf.cloud.server.mysql;

import java.sql.Connection;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.vf.cloud.common.mapping._BizMappingKit;
import com.vf.cloud.common.mapping._MappingKit;
import com.vf.cloud.server.mysql.server.Server;

public class MySQLServer {
	
	private static volatile MySQLServer INSYANCE;

	private Server server;

	private MySQLServer() {
		server = new Server();
	}

	public static MySQLServer getInstance() {
		if (null == INSYANCE) {
			synchronized (MySQLServer.class) {
				if (null == INSYANCE) {
					INSYANCE = new MySQLServer();
				}
			}
		}
		return INSYANCE;
	}
	boolean isOk=false;
	public boolean run() {
		if (server != null) {
			server.run();
		}

		isOk=server.isRunning();
		if(isOk) {
			DruidPlugin druidPlugin = new DruidPlugin("jdbc:mysql://127.0.0.1:23308/vfcloud?characterEncoding=utf8&useUnicode=true&characterEncoding=utf-8&useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=UTC&nullNamePatternMatchesAll=true&autoReconnect=true","root","vfcloud@2022");
			druidPlugin.set(5, 5, 100);			
			ActiveRecordPlugin arp = new ActiveRecordPlugin("vfcloud", druidPlugin);
			arp.setShowSql(false);
			arp.setTransactionLevel(Connection.TRANSACTION_READ_COMMITTED);
			arp.getEngine().setToClassPathSourceFactory();
			arp.setDialect(new MysqlDialect());
			_MappingKit.mapping(arp);
			_BizMappingKit.mapping(arp);
			// arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));//false
			// 是大写, true是小写, 不写是区分大小写
			druidPlugin.start();
			arp.start();
		}
		return isOk;
	}

	public boolean isRunning() {
		return server.isRunning();
	}
	
	public boolean isOk() {
		return isOk;
	}


}
