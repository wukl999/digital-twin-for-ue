package com.vf.cloud.server.signalling.pool;

import java.net.InetSocketAddress;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import com.jfinal.kit.StrKit;
import com.vf.cloud.common.pool.ProssPool;
import com.vf.cloud.server.adapter.AdapterServer;

import io.netty.channel.ChannelHandlerContext;

public class CachePool {

	private static ConcurrentHashMap<String, ChannelHandlerContext> PLAYER_CLINET = new ConcurrentHashMap<String, ChannelHandlerContext>();
	private static ConcurrentHashMap<String, ChannelHandlerContext> UE_CLINET = new ConcurrentHashMap<String, ChannelHandlerContext>();
	private static ConcurrentHashMap<String, String> BRIDGE = new ConcurrentHashMap<String, String>();
	private static ConcurrentHashMap<String, String> CLIENT_TYPE = new ConcurrentHashMap<String, String>();
	
	private static ConcurrentHashMap<String, String> CLIENT_SHARE = new ConcurrentHashMap<String, String>();
	


	/**
	 * 添加玩家到线程池
	 * 
	 * @param playerId
	 * @param ctx
	 */
	public static void addPlayer(String playerId, ChannelHandlerContext ctx) {

		// 玩家ID 对应信道
		PLAYER_CLINET.put(playerId, ctx);
		// 信道 对应 玩家ID
		BRIDGE.put(ctx.channel().id().asLongText(), playerId);
		// 信道 对应 客户端类型
		CLIENT_TYPE.put(ctx.channel().id().asLongText(), "EIO");

	}

	public static void addUE(String playerId, ChannelHandlerContext ctx) {
		// UE 对应信道
		UE_CLINET.put(playerId, ctx);
		// 信道 对应 UE_ID
		BRIDGE.put(ctx.channel().id().asLongText(), playerId);
		// 信道 对应 客户端类型
		CLIENT_TYPE.put(ctx.channel().id().asLongText(), "UE");
	}

	public static boolean existUE(String playerId) {
		return UE_CLINET.containsKey(playerId) && PLAYER_CLINET.containsKey(playerId);
	}

	/**
	 * 获取信道对于的客户端ID
	 * 
	 * @param channelId
	 * @return
	 */
	public static String getId(String channelId) {
		if (BRIDGE.containsKey(channelId))
			return BRIDGE.get(channelId);
		return "";
	}

	/**
	 * 根据信道ID判断端类型
	 * 
	 * @param channelId
	 * @return
	 */
	public static String getType(String channelId) {
		if (CLIENT_TYPE.containsKey(channelId)) {
			return CLIENT_TYPE.get(channelId);
		}
		return "";
	}

	public static ChannelHandlerContext getUEById(String playerId) {
		return UE_CLINET.get(playerId);
	}

	public static ChannelHandlerContext getPlayerById(String playerId) {
		return PLAYER_CLINET.get(playerId);
	}

	public static void removeUECache(String channelId) {
		if (BRIDGE.containsKey(channelId)) {
			killUEByPlayerId(BRIDGE.get(channelId));
			UE_CLINET.remove(BRIDGE.get(channelId));
			BRIDGE.remove(channelId);
		}
		CLIENT_TYPE.remove(channelId);
	}

	public static void removePlayerCache(String channelId) {
		if (BRIDGE.containsKey(channelId)) {
			PLAYER_CLINET.remove(BRIDGE.get(channelId));
			BRIDGE.remove(channelId);
		}
		CLIENT_TYPE.remove(channelId);
	}

	/**
	 * KillUEByPlayerChannelId
	 * @param playerChannelId
	 */
	public static void killUEByPlayerId(String playerId) {
		ChannelHandlerContext ctx = UE_CLINET.get(playerId);
		if (ctx != null) {
			InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
			String clientPort = String.valueOf(insocket.getPort());
			String pid = ProssPool.getPid(clientPort);
			if (!StrKit.isBlank(pid)) {
				ProssPool.kill(playerId, pid);
			}
		}
		AdapterServer.getInstance().reset(playerId);
	}

	public static void KillAllUeClient() {
		Iterator<String> UE_IT = UE_CLINET.keySet().iterator();
		while (UE_IT.hasNext()) {
			String playerId=UE_IT.next();
			killUEByPlayerId(playerId);
		}
	}
	
	
	public static boolean isEnabled(String playerId) {
		if(!PLAYER_CLINET.containsKey(playerId)) {
			return true;
		}
		if(CLIENT_SHARE.containsKey(playerId)) {
			return true;
		}
		return false;
	}
	

//	/**
//	 * 添加流端到线程池
//	 * 
//	 * @param playerId
//	 * @param ctx
//	 */
//	public static void addUE(String playerId, String playerSid, ChannelHandlerContext ctx) {
//		CHANNEL_UE_TO_PLAYER.put(ctx.channel().id().asLongText(), playerSid);
//
//		CHANNEL_PLAYER_TO_UE.put(playerSid, ctx.channel().id().asLongText());
//		CHANNEL.put(ctx.channel().id().asLongText(), ctx);
//
//		// 玩家ID 对应信道
//		UE_CLINET.put(playerId, ctx);
//
//		// 信道 对应 玩家ID
//		BRIDGE.put(ctx.channel().id().asLongText(), playerId);
//
//		// 信道 对应 客户端类型
//		CLIENT_TYPE.put(ctx.channel().id().asLongText(), "UE");
//	}
//
//	/**
//	 * KillUEByPlayerChannelId
//	 * 
//	 * @param playerChannelId
//	 */
//	public static void killUEByPlayerChannelId(String playerId, String playerChannelId) {
//
//		if (StrKit.isBlank(playerId)) {
//			if (BRIDGE.containsKey(playerChannelId)) {
//				playerId = BRIDGE.get(playerChannelId);
//			}
//		}
//
//		if (!StrKit.isBlank(playerId) && PLAYER_CLINET.containsKey(playerId)) {
//			if (StrKit.equals(PLAYER_CLINET.get(playerId).channel().id().asLongText(), playerChannelId)) {
//				PLAYER_CLINET.remove(playerId);
//			}
//		}
//
//		if (CHANNEL_PLAYER_TO_UE.containsKey(playerChannelId)) {
//			String ueChannelId = CHANNEL_PLAYER_TO_UE.get(playerChannelId);
//			if (CHANNEL.containsKey(ueChannelId)) {
//				ChannelHandlerContext UE = CHANNEL.get(ueChannelId);
//				InetSocketAddress insocket = (InetSocketAddress) UE.channel().remoteAddress();
//				String clientPort = String.valueOf(insocket.getPort());
//				String pid = ProssPool.getPid(clientPort);
//				if (!StrKit.isBlank(pid)) {
//					ProssPool.kill(ueChannelId, pid);
//				}
//
//				CHANNEL.remove(ueChannelId);
//			}
//
//			if (BRIDGE.containsKey(ueChannelId)) {
//				BRIDGE.remove(ueChannelId);
//			}
//			if (CLIENT_TYPE.containsKey(ueChannelId)) {
//				CLIENT_TYPE.remove(ueChannelId);
//			}
//
//			if (CHANNEL_UE_TO_PLAYER.containsKey(ueChannelId)) {
//				CHANNEL_UE_TO_PLAYER.remove(ueChannelId);
//			}
//			CHANNEL_PLAYER_TO_UE.remove(playerChannelId);
//		}
//	}
//
//	public static void removeUECache(String channelId) {
//		if (CHANNEL.containsKey(channelId)) {
//			ChannelHandlerContext UE = CHANNEL.get(channelId);
//			if (UE != null && !UE.isRemoved()) {
//				UE.close();
//			}
//			CHANNEL.remove(channelId);
//		}
//
//		if (BRIDGE.containsKey(channelId)) {
//			BRIDGE.remove(channelId);
//		}
//		if (CLIENT_TYPE.containsKey(channelId)) {
//			CLIENT_TYPE.remove(channelId);
//		}
//
//		if (CHANNEL_UE_TO_PLAYER.containsKey(channelId)) {
//			CHANNEL_UE_TO_PLAYER.remove(channelId);
//		}
//
//		if (CHANNEL_UE_TO_PLAYER.containsKey(channelId)) {
//			CHANNEL_UE_TO_PLAYER.remove(channelId);
//		}
//		CHANNEL_PLAYER_TO_UE.remove(channelId);
//	}
//
//	/**
//	 * KillPlayerByUEChannelId
//	 * 
//	 * @param ueChannelId
//	 */
//	public static void killPlayerByUEChannelId(String playerId, String ueChannelId) {
//
//		if (StrKit.isBlank(playerId)) {
//			if (BRIDGE.containsKey(ueChannelId)) {
//				playerId = BRIDGE.get(ueChannelId);
//			}
//		}
//
//		if (!StrKit.isBlank(playerId) && UE_CLINET.containsKey(playerId)) {
//			if (StrKit.equals(UE_CLINET.get(playerId).channel().id().asLongText(), ueChannelId)) {
//				UE_CLINET.remove(playerId);
//			}
//		}
//
//		if (CHANNEL_UE_TO_PLAYER.containsKey(ueChannelId)) {
//			String playerChannelId = CHANNEL_UE_TO_PLAYER.get(ueChannelId);
//			if (CHANNEL.containsKey(playerChannelId)) {
//				ChannelHandlerContext player = CHANNEL.get(playerChannelId);
//				if (player != null && !player.isRemoved()) {
//					player.close();
//				}
//				CHANNEL.remove(ueChannelId);
//
//				if (BRIDGE.containsKey(playerChannelId)) {
//					BRIDGE.remove(playerChannelId);
//				}
//				if (CLIENT_TYPE.containsKey(playerChannelId)) {
//					CLIENT_TYPE.remove(playerChannelId);
//				}
//			}
//		}
//
//	}
//
//	public static void removePlayerCache(String channelId) {
//		if (CHANNEL.containsKey(channelId)) {
//			ChannelHandlerContext player = CHANNEL.get(channelId);
//			if (player != null && !player.isRemoved()) {
//				player.close();
//			}
//			CHANNEL.remove(channelId);
//		}
//
//		if (BRIDGE.containsKey(channelId)) {
//			BRIDGE.remove(channelId);
//		}
//		if (CLIENT_TYPE.containsKey(channelId)) {
//			CLIENT_TYPE.remove(channelId);
//		}
//
//		if (CHANNEL_UE_TO_PLAYER.containsKey(channelId)) {
//			CHANNEL_UE_TO_PLAYER.remove(channelId);
//		}
//
//	}
//
//	public static void checkActiveUEClient() {
//		Iterator<String> it = CHANNEL_UE_TO_PLAYER.keySet().iterator();
//		String ueChannelId;
//		String playerChannelId;
//		while (it.hasNext()) {
//			ueChannelId = it.next();
//			playerChannelId = CHANNEL_UE_TO_PLAYER.get(ueChannelId);
//			if (!CHANNEL.containsKey(playerChannelId)) {
//				killUEByPlayerChannelId("", playerChannelId);
//			}
//		}
//	}
//
//	/**
//	 * 移除Payle
//	 * 
//	 * @param playerId
//	 */
//	public static void killPayler(String playerId) {
//		if (PLAYER_CLINET.containsKey(playerId)) {
//			ChannelHandlerContext playerChannel = PLAYER_CLINET.get(playerId);
//			if (playerChannel != null) {
//				if (BRIDGE.containsKey(playerChannel.channel().id().asLongText())) {
//					BRIDGE.remove(playerChannel.channel().id().asLongText());
//				}
//				if (CLIENT_TYPE.containsKey(playerChannel.channel().id().asLongText())) {
//					CLIENT_TYPE.remove(playerChannel.channel().id().asLongText());
//				}
//				if (!playerChannel.isRemoved()) {
//					playerChannel.close();
//				}
//			}
//		}
//	}
//
//	/**
//	 * 生成玩家ID
//	 * 
//	 * @return
//	 */
//	public static long genOrder() {
//		long format;
//		while (true) {
//			int hashCode = java.util.UUID.randomUUID().toString().hashCode();
//			if (hashCode < 0) {
//				hashCode = -hashCode;
//			}
//			format = Long.parseLong(String.format("%010d", hashCode).substring(0, 10));
//			if (!PLAYER_CLINET.containsKey(format + "")) {
//				break;
//			}
//		}
//		return format;
//	}
//
//	public static int getCurrServerTotal() {
//		return CHANNEL_UE_TO_PLAYER.size();
//	}
//
//	/**
//	 * 根据信道ID判断端类型
//	 * 
//	 * @param channelId
//	 * @return
//	 */
//	public static String getType(String channelId) {
//		if (CLIENT_TYPE.containsKey(channelId)) {
//			return CLIENT_TYPE.get(channelId);
//		}
//		return "";
//	}
//
//
//
//	public static boolean existPlayer(String channelId) {
//		return CHANNEL.containsKey(channelId);
//	}
//
//	public static ChannelHandlerContext getPlayerById(String playerId) {
//		if (PLAYER_CLINET != null && PLAYER_CLINET.containsKey(playerId)) {
//			return PLAYER_CLINET.get(playerId);
//		}
//		return null;
//	}
//

//
//	public static void clearAll() {
//		Iterator<String> it = CHANNEL_UE_TO_PLAYER.keySet().iterator();
//		String ueChannelId;
//		String playerChannelId;
//		while (it.hasNext()) {
//			ueChannelId = it.next();
//			playerChannelId = CHANNEL_UE_TO_PLAYER.get(ueChannelId);
//			if (!CHANNEL.containsKey(playerChannelId)) {
//				killUEByPlayerChannelId("", playerChannelId);
//			}
//		}
//
//	}

}
