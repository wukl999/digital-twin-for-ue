package com.vf.cloud.server.colony.pool;

import java.util.concurrent.ConcurrentHashMap;

import com.vf.cloud.common.domain.Client;

import io.netty.channel.ChannelHandlerContext;

public class NodePool {

	private static ConcurrentHashMap<String, ChannelHandlerContext> NODE_CLINET = new ConcurrentHashMap<String, ChannelHandlerContext>();
	private static ConcurrentHashMap<String, String> BRIDGE = new ConcurrentHashMap<String, String>();

	public static void addNode(String key, ChannelHandlerContext ctx) {
		NODE_CLINET.put(key, ctx);
		BRIDGE.put(ctx.channel().id().asLongText(), key);
	}

	public static void removeNode(String channelId) {
		if (BRIDGE.containsKey(channelId)) {
			NODE_CLINET.remove(BRIDGE.get(channelId));
			Client.dao.deleteById(BRIDGE.get(channelId));
			BRIDGE.remove(channelId);
		}
	}

}
