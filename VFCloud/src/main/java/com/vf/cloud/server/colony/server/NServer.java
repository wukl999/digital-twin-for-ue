package com.vf.cloud.server.colony.server;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.vf.cloud.common.constant.Cache;
import com.vf.cloud.server.adapter.AdapterServer;
import com.vf.cloud.server.colony.NodeServer;
import com.vf.cloud.server.colony.handler.NodeHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshakerFactory;
import io.netty.handler.codec.http.websocketx.WebSocketVersion;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NServer implements Runnable {

	private EventLoopGroup group;
	private ExecutorService executor;
	private Channel channel;

	/**
	 * 0-停止 1-正在启动 2-运行中 3-停止中
	 */
	private int status = 0;

	public void run() {
		executor = Executors.newFixedThreadPool(1);
		try {
			executor.submit(new Callable<String>() {
				@Override
				public String call() throws Exception {
					start();
					return "";
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void start() {
		try {
			status = 1;
			log.info(">>>正在启动NodeServer");
			NodeHandler handler = new NodeHandler();
			Bootstrap bootstrap = new Bootstrap();
			group = new NioEventLoopGroup();
			bootstrap.group(group);
			bootstrap.channel(NioSocketChannel.class).option(ChannelOption.TCP_NODELAY, true)
					.option(ChannelOption.SO_KEEPALIVE, true).handler(new ChannelInitializer<SocketChannel>() {
						@Override
						protected void initChannel(SocketChannel ch) throws Exception {
							ChannelPipeline pipeline = ch.pipeline();
							pipeline.addLast(new IdleStateHandler(0, 30, 0, TimeUnit.SECONDS));
							// 添加一个http的编解码器
							pipeline.addLast(new HttpClientCodec());
							// 添加一个用于支持大数据流的支持
							pipeline.addLast(new ChunkedWriteHandler());
							// 添加一个聚合器，这个聚合器主要是将HttpMessage聚合成FullHttpRequest/Response
							pipeline.addLast(new HttpObjectAggregator(1024 * 64));
							pipeline.addLast(handler);

						}
					});

			URI websocketURI = new URI(
					String.format("ws://%s:%s?key=%s", Cache.config.getMasterIp(), 6666, Cache.config.getMasterPwd()));
			HttpHeaders httpHeaders = new DefaultHttpHeaders();
			// 进行握手
			WebSocketClientHandshaker handshaker = WebSocketClientHandshakerFactory.newHandshaker(websocketURI,
					WebSocketVersion.V13, (String) null, true, httpHeaders);
			channel = bootstrap.connect(websocketURI.getHost(), websocketURI.getPort()).sync().channel();
			handler.setHandshaker(handshaker);
			handshaker.handshake(channel);
			// 阻塞等待是否握手成功
			handler.handshakeFuture().sync();

			log.info(">>>NodeServer启动成功：" + channel);
			status = 2;
			AdapterServer.getInstance().onChange();
			channel.closeFuture().sync();

		} catch (InterruptedException | URISyntaxException e) {
			e.printStackTrace();
			status = 3;
			log.error(">>>NodeServer启动异常：", e);
			stop();
		} finally {
			group.shutdownGracefully();
			if (executor != null)
				executor.shutdownNow();
			log.info(">>>NodeServer关闭" + channel);
			status = 0;
		}
	}

	public void stop() {
		status = 3;
		if (channel != null) {
			channel.close();
			channel=null;
		}else {
			status = 0;	
		}
	}

	public void restart() {
		stop();
	}

	public int getStatus() {
		return status;
	}

	private ScheduledExecutorService reconnectScheduledExecutor;

	public void reconnect() {
		reconnectScheduledExecutor = new ScheduledThreadPoolExecutor(1);
		reconnectScheduledExecutor.scheduleWithFixedDelay(new TimerTask() {
			@Override
			public void run() {
				
				if (Cache.config!=null &&  StrKit.equals("1", Cache.config.getEnabled()) && NodeServer.getInstance().verify()) {
					if (status == 0) {
						NodeServer.getInstance().run();
					}
					else if(status==2){
					}else {
//						log.info(String.format("重连中，当前服务状态:%s", status));
					}
				}
			}
		}, 0, 5, TimeUnit.SECONDS);
	}

	public void send(Map<String, Object> messag) {
		if (channel != null && channel.isOpen() && channel.isActive()) {
			channel.writeAndFlush(new TextWebSocketFrame(JsonKit.toJson(messag)))
					.addListener(new ChannelFutureListener() {
						@Override
						public void operationComplete(ChannelFuture future) throws Exception {
							if (future.isSuccess()) {
							} else {
							}
						}
					});
		}
	}

	public void destroy() {
		if(reconnectScheduledExecutor!=null) {
			reconnectScheduledExecutor.shutdown();
		}
		stop();
	}

}
