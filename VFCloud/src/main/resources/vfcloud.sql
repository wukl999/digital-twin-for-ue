/*
Navicat MySQL Data Transfer

Source Server         : vfcloud
Source Server Version : 80011
Source Host           : localhost:23308
Source Database       : vfcloud

Target Server Type    : MYSQL
Target Server Version : 80011
File Encoding         : 65001

Date: 2022-08-17 18:45:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for biz_config
-- ----------------------------
DROP TABLE IF EXISTS `biz_config`;
CREATE TABLE `biz_config` (
  `id` varchar(40) NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `dispatch_port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `dispatch_secret_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of biz_config
-- ----------------------------
INSERT INTO `biz_config` VALUES ('vf_cloud_pass_2022', '127.0.0.1', '8880', '6666', 'PI30F8PRX30WAQT7');

-- ----------------------------
-- Table structure for biz_project
-- ----------------------------
DROP TABLE IF EXISTS `biz_project`;
CREATE TABLE `biz_project` (
  `id` varchar(40) NOT NULL,
  `org_id` varchar(40) DEFAULT NULL,
  `scene_id` varchar(40) DEFAULT NULL,
  `limit` int(11) DEFAULT '0',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新人',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建者',
  `name` varchar(255) DEFAULT '',
  `org_name` varchar(255) DEFAULT '',
  `status` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of biz_project
-- ----------------------------
INSERT INTO `biz_project` VALUES ('145819d5361b490489bff6bc84e74e52', '054d5f415c4d487c82458dd846cdfa49', '94b2d2b9429a44a6bbf01d6372c7ef04', '7', '2022-08-16 15:15:23', '2022-08-16 15:15:23', '', null, '', '', '0');
INSERT INTO `biz_project` VALUES ('b9f736fd6fca4a4fb4348db875595473', '8d876b021af646dba30c43a04f2283a6', '94b2d2b9429a44a6bbf01d6372c7ef04', '5', '2022-08-17 14:28:39', '2022-08-17 14:28:39', '', null, '', '', '0');
INSERT INTO `biz_project` VALUES ('e1e7348582bd4f1fa6f3fd35cafeaf1b', '7ac49150f0794337b28ecc1056d1aa0d', '94b2d2b9429a44a6bbf01d6372c7ef04', '2', '2022-08-16 15:15:26', '2022-08-16 15:15:26', '', null, '', '', '1');

-- ----------------------------
-- Table structure for biz_scene
-- ----------------------------
DROP TABLE IF EXISTS `biz_scene`;
CREATE TABLE `biz_scene` (
  `id` varchar(40) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `industry_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `size` double(255,2) DEFAULT '0.00',
  `version` double(255,2) DEFAULT '0.00',
  `exe_file_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `frame_rate` int(11) DEFAULT '30',
  `is_forceRes` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1',
  `resX` int(255) DEFAULT '1920',
  `resY` int(255) DEFAULT '1080',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新人',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_release` varchar(5) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of biz_scene
-- ----------------------------
INSERT INTO `biz_scene` VALUES ('94b2d2b9429a44a6bbf01d6372c7ef04', '蒙自智慧城市', null, '0.00', '0.20', 'D:\\Pack\\Windows\\MenZiQu\\Binaries\\Win64\\MenZiQu.exe', '', '24', '1', '1920', '1080', '', '2022-08-15 18:29:40', null, '2022-08-15 18:29:40', '0');

-- ----------------------------
-- Table structure for biz_scene_share
-- ----------------------------
DROP TABLE IF EXISTS `biz_scene_share`;
CREATE TABLE `biz_scene_share` (
  `id` varchar(40) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `scene_id` varchar(40) DEFAULT NULL,
  `resX` int(11) DEFAULT '1920',
  `resY` int(11) DEFAULT '1080',
  `limitedTime` int(11) DEFAULT '1',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of biz_scene_share
-- ----------------------------
INSERT INTO `biz_scene_share` VALUES ('2229c5b9c470406cb824e776fcc0c5ef', '蒙自智慧城市', '2229c5b9c470406cb824e776fcc0c5ef', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 21:52:27');
INSERT INTO `biz_scene_share` VALUES ('29d821cdc1804d41a89a3514d44f4fd5', '蒙自智慧城市', '29d821cdc1804d41a89a3514d44f4fd5', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 21:34:20');
INSERT INTO `biz_scene_share` VALUES ('3e9f9caff3864a9184e937d50f93f8c3', '蒙自智慧城市', '3e9f9caff3864a9184e937d50f93f8c3', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-16 00:36:00');
INSERT INTO `biz_scene_share` VALUES ('40c5b0fe2f13442a965dc5dabffa003b', '蒙自智慧城市', '40c5b0fe2f13442a965dc5dabffa003b', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 22:13:52');
INSERT INTO `biz_scene_share` VALUES ('48605f1c761d49e296384f010ec4c36c', '蒙自智慧城市', '48605f1c761d49e296384f010ec4c36c', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-16 15:15:19');
INSERT INTO `biz_scene_share` VALUES ('79a5e8503609474bbf563e87a3900c5b', '蒙自智慧城市', '79a5e8503609474bbf563e87a3900c5b', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 21:09:43');
INSERT INTO `biz_scene_share` VALUES ('91abb8af13484afe868ad18c57592ae2', '蒙自智慧城市', '91abb8af13484afe868ad18c57592ae2', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 21:49:53');
INSERT INTO `biz_scene_share` VALUES ('9b9d1d29c2c946f498310e319bc4f449', '蒙自智慧城市', '9b9d1d29c2c946f498310e319bc4f449', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 21:11:27');
INSERT INTO `biz_scene_share` VALUES ('aab319e012934c068d0a1fcb47170170', '蒙自智慧城市', 'aab319e012934c068d0a1fcb47170170', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 22:12:28');
INSERT INTO `biz_scene_share` VALUES ('b353e776fa4d40cdb4a337eb5b55fc2f', '蒙自智慧城市', 'b353e776fa4d40cdb4a337eb5b55fc2f', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 21:43:49');
INSERT INTO `biz_scene_share` VALUES ('b65111aa44ec47de802a917217ff1c28', '蒙自智慧城市', 'b65111aa44ec47de802a917217ff1c28', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-16 09:50:37');
INSERT INTO `biz_scene_share` VALUES ('b73d4ae9097541f4b2409ef648d7e73c', '蒙自智慧城市', 'b73d4ae9097541f4b2409ef648d7e73c', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 21:52:58');
INSERT INTO `biz_scene_share` VALUES ('c984c9a31fef411a9e7a6a0e891c9ea2', '蒙自智慧城市', 'c984c9a31fef411a9e7a6a0e891c9ea2', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 21:04:29');
INSERT INTO `biz_scene_share` VALUES ('e27d4c56b8654c7fadaa1d7c7afaf052', '蒙自智慧城市', 'e27d4c56b8654c7fadaa1d7c7afaf052', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 21:06:40');
INSERT INTO `biz_scene_share` VALUES ('f9e9937a70534d2c97c4f3f7fe5272ef', '蒙自智慧城市', 'f9e9937a70534d2c97c4f3f7fe5272ef', '94b2d2b9429a44a6bbf01d6372c7ef04', '1920', '1080', '1', '2022-08-15 21:48:22');

-- ----------------------------
-- Table structure for biz_scene_user
-- ----------------------------
DROP TABLE IF EXISTS `biz_scene_user`;
CREATE TABLE `biz_scene_user` (
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `scene_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `limit` int(11) DEFAULT '0',
  PRIMARY KEY (`user_id`,`scene_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of biz_scene_user
-- ----------------------------

-- ----------------------------
-- Table structure for biz_scene_user_permission
-- ----------------------------
DROP TABLE IF EXISTS `biz_scene_user_permission`;
CREATE TABLE `biz_scene_user_permission` (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `org_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `scene_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `limit` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of biz_scene_user_permission
-- ----------------------------

-- ----------------------------
-- Table structure for biz_terminal
-- ----------------------------
DROP TABLE IF EXISTS `biz_terminal`;
CREATE TABLE `biz_terminal` (
  `id` varchar(40) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '-1 - 未授权\r\n0-停止\r\n1-正在运行\r\n',
  `project_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `os` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `memory_total` double(40,2) DEFAULT '0.00',
  `memory_used` double(40,2) DEFAULT '0.00',
  `memory_used_percentage` double(255,1) DEFAULT '0.0',
  `gpu_model` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `gpu_num` int(11) DEFAULT '0',
  `gpu_memory_used` double(40,2) DEFAULT '0.00',
  `gpu_memory_total` double(40,2) DEFAULT '0.00',
  `gpu_memory_used_percentage` double(255,1) DEFAULT '0.0',
  `online_time` datetime DEFAULT NULL COMMENT '上线时间',
  `offline_time` datetime DEFAULT NULL COMMENT '离线时间',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `remark` varchar(255) DEFAULT '',
  `is_support_limit` varchar(5) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of biz_terminal
-- ----------------------------
INSERT INTO `biz_terminal` VALUES ('30-B4-9E-67-57-56', '127.0.0.1', '127.0.0.1', '8881', '0', 'd:/Projecs', 'Windows 10', '31.90', '12.00', '38.0', 'NVIDIA GeForce RTX 3090', '1', '2.60', '24.00', '10.0', '2022-08-17 16:12:07', '2022-08-17 18:45:30', '2022-08-17 18:45:30', '2022-08-17 18:45:30', '', '0');

-- ----------------------------
-- Table structure for biz_terminal_gpu
-- ----------------------------
DROP TABLE IF EXISTS `biz_terminal_gpu`;
CREATE TABLE `biz_terminal_gpu` (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `terminal_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `gpu_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `gpu_memory` double(255,2) DEFAULT '0.00',
  `gpu_status` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `gpu_index` int(11) DEFAULT '0',
  `gpu_limit` int(11) DEFAULT '0',
  `gpu_used` int(11) DEFAULT '0',
  `gpu_util` varchar(255) DEFAULT '',
  `memory_util` varchar(255) DEFAULT '',
  `encoder_util` varchar(255) DEFAULT '',
  `decoder_util` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of biz_terminal_gpu
-- ----------------------------
INSERT INTO `biz_terminal_gpu` VALUES ('GPU-c82b1007-1830-21ec-4e16-016913644cd6', '30-B4-9E-67-57-56', 'NVIDIA GeForce RTX 3090', '24.00', '0', '0', '0', '0', '4 %', '9 %', '0 %', '0 %');

-- ----------------------------
-- Table structure for doc_api
-- ----------------------------
DROP TABLE IF EXISTS `doc_api`;
CREATE TABLE `doc_api` (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `parent_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `version` double(10,2) DEFAULT NULL,
  `json` json DEFAULT NULL,
  `desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `deschtml` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `sort` int(10) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of doc_api
-- ----------------------------

-- ----------------------------
-- Table structure for doc_api_file
-- ----------------------------
DROP TABLE IF EXISTS `doc_api_file`;
CREATE TABLE `doc_api_file` (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `apiId` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of doc_api_file
-- ----------------------------

-- ----------------------------
-- Table structure for sys_client
-- ----------------------------
DROP TABLE IF EXISTS `sys_client`;
CREATE TABLE `sys_client` (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '0-node\r\n1-master',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `limit` int(11) DEFAULT '0',
  `surplus` int(11) DEFAULT '0',
  `used` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_client
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `master_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `master_port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `master_pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `enabled` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `local_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `local_port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `local_pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `is_master` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `is_master_enabled` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('VFCloud_2022', '127.0.0.1', '8880', '0XEECT9DFUMT7VDE', '1', '127.0.0.1', '8880', '0XEECT9DFUMT7VDE', '1', '1');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '部门名称',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  `parent_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='部门管理';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '字典主键',
  `parent_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '0',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '字典名称',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `order_num` int(11) DEFAULT '0',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dict_type` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('11c7de5fa54441349786acc4fd9e3a4a', '0', '行业字典', 'Industry', '0', '1', null, '', '2022-08-11 10:40:59', '', '2022-08-11 10:41:14');
INSERT INTO `sys_dict` VALUES ('63e5389062234ecf8d21f12037c73617', '0', '主机系统', 'HOST_OS', '0', '2', null, '', '2022-08-13 02:31:08', '', null);

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `dict_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '字典标签',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '字典编码',
  `value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '字典键值',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '字典类型',
  `sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES ('6d1b8869656549579c83d2ff08803924', '11c7de5fa54441349786acc4fd9e3a4a', '智慧楼宇', 'ZHI_HUI_LOU_YU', 'ZHI_HUI_LOU_YU', '', '1', '0', '', null, '', null, null);
INSERT INTO `sys_dict_item` VALUES ('a58592bac7b1443a9e48271a3f0bc685', '63e5389062234ecf8d21f12037c73617', 'Windows', 'win', 'win', '', '1', '0', '', null, '', null, null);
INSERT INTO `sys_dict_item` VALUES ('b4f1e7dade4240cfa192c2b6d015f247', '11c7de5fa54441349786acc4fd9e3a4a', '智慧医院', 'ZHI_HUI_YI_YUAN', 'ZHI_HUI_YI_YUAN', '', '2', '0', '', null, '', null, null);

-- ----------------------------
-- Table structure for sys_gpu
-- ----------------------------
DROP TABLE IF EXISTS `sys_gpu`;
CREATE TABLE `sys_gpu` (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `limit` int(11) DEFAULT '1',
  `enabled` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `memory_total` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `board_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `vbios_version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `memory_used` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `memory_free` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `memory_reserved` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `index` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_gpu
-- ----------------------------
INSERT INTO `sys_gpu` VALUES ('GPU-870f0361-958c-91bf-9615-cf899e83484b', 'NVIDIA GeForce GTX 1660 Ti', '0', '1', '0', '6144 MiB', '0x100', '90.16.29.00.dd', '', '', '', '0');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '1' COMMENT '日志类型',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '日志标题',
  `user_agent` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户代理',
  `time` bigint(20) DEFAULT NULL COMMENT '执行时间',
  `service_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '服务ID',
  `request_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '请求URI',
  `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作方式',
  `request_params` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `response_code` int(11) DEFAULT NULL COMMENT '请求响应码',
  `response_message` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '请求响应信息',
  `response_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '请求响内容',
  `exception` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '异常信息',
  `remote_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作IP地址',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标记',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`),
  KEY `sys_log_create_by` (`create_by`),
  KEY `sys_log_request_uri` (`request_uri`),
  KEY `sys_log_type` (`type`),
  KEY `sys_log_create_date` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='日志表';

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单名称',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `parent_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `prev_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) DEFAULT '1' COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '备注',
  `no_cache` int(11) DEFAULT '0',
  `active_menu` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', 'System', '0', '999', '', '/system', null, null, '1', '0', 'M', '0', '0', null, 'el-icon-setting', '', '2022-02-15 21:20:05', '', '2022-02-16 22:35:36', '系统管理目录', '0', '');
INSERT INTO `sys_menu` VALUES ('101', '菜单管理', 'Menu', '1', '10', '', 'menu', '/system/menu/index.vue', null, '1', '0', 'C', '0', '0', null, 'el-icon-menu', '', null, '', '2022-02-15 21:20:56', '菜单管理菜单', '0', '');
INSERT INTO `sys_menu` VALUES ('1027a2debfd14c999a2d7175dd402fe0', '项目管理', 'Project-F', '0', '2', '', '/project', '', null, '1', '0', 'M', '0', '0', '', 'el-icon-collection', '', '2022-08-12 22:26:15', '', '2022-08-12 22:34:37', '', '0', '');
INSERT INTO `sys_menu` VALUES ('18796657c331407e8052e7603a606b97', '个人中心', 'Profile', '1d3a3f334f574be48cfe5cf6d881d1d9', '1', '', 'profile', '/system/profile/index', null, '1', '0', 'C', '0', '1', '', 'el-icon-user-solid', '', '2022-08-12 22:31:48', '', '2022-08-12 22:31:47', '', '0', '');
INSERT INTO `sys_menu` VALUES ('1d3a3f334f574be48cfe5cf6d881d1d9', '个人中心', 'Profile-F', '0', '20', '', '/profile', '', null, '1', '0', 'M', '0', '0', '', '', '', '2022-08-12 22:30:40', '', '2022-08-12 22:34:22', '', '0', '');
INSERT INTO `sys_menu` VALUES ('1e7cd22213674be391fb68b5b94d36de', '主机管理', 'Terminal', '4301229191db414c815a9ec3c677b08b', '1', '', 'terminal', '/biz/terminal/index', null, '1', '0', 'C', '0', '1', '', '', '', '2022-08-12 22:33:41', '', '2022-08-12 22:33:40', '', '0', '');
INSERT INTO `sys_menu` VALUES ('2067e8c962eb4fb686372757cb687f34', 'SuperAPI', 'Superapi', 'fd96ecdcc72c4c0b8e65cd20b94e86bf', '1', '', '/superapi', '/cloud/docApi/index', null, '1', '0', 'C', '0', '0', '', '', '', '2022-08-12 22:38:28', '', '2022-08-12 22:39:28', '', '0', '');
INSERT INTO `sys_menu` VALUES ('273c4490df5b4b2d89f8ab8c919a8456', '部门管理', 'Dept', '1', '3', '', 'dept', '/system/dept/index.vue', null, '1', '0', 'M', '0', '0', '', 'el-icon-notebook-2', '', '2022-02-17 21:24:03', '', '2022-02-17 21:28:59', '', '0', '');
INSERT INTO `sys_menu` VALUES ('27e867a324834d21a8c16f2365e99a93', '角色管理', 'Role', '1', '2', '', 'role', '/system/role/index.vue', null, '1', '0', 'C', '0', '0', '', 'el-icon-s-custom', '', '2022-02-17 21:22:01', '', '2022-02-17 21:27:56', '', '0', '');
INSERT INTO `sys_menu` VALUES ('3f572284e94b4b56812532c771826b7b', '系统设置', 'Settings', 'cc272da1b0bc4db5a6806bc3a023047f', '1', '', 'settings', '/biz/settings/index', null, '1', '0', 'C', '0', '0', '', 'el-icon-s-tools', '', '2022-08-16 14:16:17', '', '2022-08-16 14:18:16', '', '0', '');
INSERT INTO `sys_menu` VALUES ('4301229191db414c815a9ec3c677b08b', '主机管理', 'Terminal-F', '0', '3', '', '/terminal', '', null, '1', '0', 'M', '0', '0', '', 'el-icon-s-platform', '', '2022-08-12 22:27:08', '', '2022-08-12 22:34:08', '', '0', '');
INSERT INTO `sys_menu` VALUES ('480c683c342e4467b258de344a488c31', '资源管理', 'PixelStreaming', 'a69cc88066c446319f3b16b3e5f513e6', '1', '', 'pixelStreaming', '/biz/pixelStreaming/index', null, '1', '0', 'C', '0', '0', '', '', '', '2022-08-14 02:50:35', '', '2022-08-14 02:51:31', '', '0', '');
INSERT INTO `sys_menu` VALUES ('530484840f924dd5b15e31de6126b9e3', '客户管理', 'User', 'fd4d5f1ed4004c46a5744170bc733d97', '1', '', 'user', '/system/user/index.vue', null, '1', '0', 'C', '0', '1', '', '', '', '2022-08-12 22:32:41', '', '2022-08-12 22:32:41', '', '0', '');
INSERT INTO `sys_menu` VALUES ('876fcd0d4e5848dfb0bec12fd4edcea4', '场景管理', 'Scene-F', '0', '1', '', '/scene', '', null, '1', '0', 'M', '0', '0', '', 'el-icon-notebook-1', '', '2022-08-12 22:24:52', '', '2022-08-12 22:35:14', '', '0', '');
INSERT INTO `sys_menu` VALUES ('89342a4ef5eb48f8bb2a642623ce4d71', '项目管理', 'Project', '1027a2debfd14c999a2d7175dd402fe0', '1', '', 'project', '/biz/project/index', null, '1', '0', 'C', '0', '0', '', '', '', '2022-08-12 22:34:52', '', '2022-08-13 00:38:02', '', '0', '');
INSERT INTO `sys_menu` VALUES ('a1818b5f41dd48ab854f16934585ce39', '字典管理', 'Dic', '1', '4', '', 'dic', '/system/dic/index.vue', null, '1', '0', 'C', '0', '0', '', 'el-icon-notebook-1', '', '2022-02-17 21:30:12', '', '2022-02-17 21:39:24', '', '0', '');
INSERT INTO `sys_menu` VALUES ('a69cc88066c446319f3b16b3e5f513e6', '资源管理', 'Resource', '0', '4', '', '/resource', '', null, '1', '0', 'M', '0', '0', '', 'el-icon-film', '', '2022-08-14 02:48:56', '', '2022-08-14 02:50:09', '', '0', '');
INSERT INTO `sys_menu` VALUES ('cc272da1b0bc4db5a6806bc3a023047f', '系统设置', 'Settings', '0', '22', '', '/settings', '', null, '1', '0', 'M', '0', '0', '', 'el-icon-s-tools', '', '2022-08-16 14:15:45', '', '2022-08-16 14:18:11', '', '0', '');
INSERT INTO `sys_menu` VALUES ('dd383fe28c3e44f0aae80ddbd9d61d9d', '场景管理', 'Scene', '876fcd0d4e5848dfb0bec12fd4edcea4', '1', '', 'scene', '/biz/scene/index', null, '1', '0', 'C', '0', '1', '', '', '', '2022-08-12 22:35:29', '', '2022-08-12 22:35:28', '', '0', '');
INSERT INTO `sys_menu` VALUES ('fd4d5f1ed4004c46a5744170bc733d97', '客户管理', 'User-F', '0', '9', '', '/user', '', null, '1', '0', 'M', '0', '0', '', 'el-icon-office-building', '', '2022-08-12 22:28:18', '', '2022-08-14 02:54:57', '', '0', '');
INSERT INTO `sys_menu` VALUES ('fd96ecdcc72c4c0b8e65cd20b94e86bf', '文档管理', 'Doc-F', '0', '66', '', '/superapi', '', null, '1', '0', 'M', '0', '0', '', 'el-icon-document', '', '2022-08-12 22:35:54', '', '2022-08-12 22:38:45', '', '0', '');

-- ----------------------------
-- Table structure for sys_project
-- ----------------------------
DROP TABLE IF EXISTS `sys_project`;
CREATE TABLE `sys_project` (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `exeFilePath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `frameRate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `isPublic` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `resX` int(11) DEFAULT '1920',
  `resY` int(11) DEFAULT '1080',
  `isLog` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `isRenderOFFscreen` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `rendering_engine` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `startup_parameter` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `industry_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `version` varchar(255) DEFAULT '',
  `down_status` varchar(255) DEFAULT '' COMMENT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_project
-- ----------------------------
INSERT INTO `sys_project` VALUES ('da4f8d5651c04d3192a22cb919a66fba', null, '尘世', 'null', 'G:\\Pack\\Windows\\MenZiQu\\Binaries\\Win64\\MenZiQu.exe', '24', null, null, null, '0', '200', '200', '0', '0', '', '', 'ZHI_HUI_LOU_YU', '', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标识（0-正常,1-删除）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `create_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `order_num` int(11) DEFAULT '0' COMMENT '显示顺序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_idx1_role_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('0025b753a7194567b9ae4b059bd68480', 'AA', 'aa', '', '1', '2022-08-11 11:24:14', null, null, null, '1', '2');
INSERT INTO `sys_role` VALUES ('57106e3822d5486b82283dd679b68878', '超级管理员', 'Admin', '', '1', '2022-08-11 10:33:19', null, null, null, '1', '2');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `menu_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='角色菜单表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_server
-- ----------------------------
DROP TABLE IF EXISTS `sys_server`;
CREATE TABLE `sys_server` (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `config` json DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `enabled` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_server
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `org_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `org_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '简介',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像',
  `lock_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '0-正常，9-锁定',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '0-正常，1-删除',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新人',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_sub_account` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_idx1_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('054d5f415c4d487c82458dd846cdfa49', '054d5f415c4d487c82458dd846cdfa49', '', '1', 'Test', '{bcrypt}$2a$10$/CwQmP.2p8sfm0P28rXLleATE4WGZS9LzExmrxgg308t5rsymqpwG', '12345678901', null, '9', '1', null, '', null, null, '0');
INSERT INTO `sys_user` VALUES ('7ac49150f0794337b28ecc1056d1aa0d', '7ac49150f0794337b28ecc1056d1aa0d', '', '超级管理员', 'admin', '{bcrypt}$2a$10$DyrgTqSPqug44IwhBtfwfOnz5SY/cc5WC4A0.PX0/wHZt/VsVfzfu', null, null, '0', '0', null, '', null, null, '0');
INSERT INTO `sys_user` VALUES ('8d876b021af646dba30c43a04f2283a6', '8d876b021af646dba30c43a04f2283a6', '云南赢中吉洋智能技术有限公司', '周松', '15887026253', '{bcrypt}$2a$10$6.2icmowDrDOtFv3GgiM3.cxPHVq8hakriYY8iiiLwOYAby2cSCee', '15887026253', null, '0', '0', null, '', null, null, '0');

-- ----------------------------
-- Table structure for sys_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_dept`;
CREATE TABLE `sys_user_dept` (
  `user_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `dept_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`user_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户部门表';

-- ----------------------------
-- Records of sys_user_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `role_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('054d5f415c4d487c82458dd846cdfa49', '0025b753a7194567b9ae4b059bd68480');
