export function objectToFormData(obj) {
  if (obj instanceof FormData) {
    return obj
  }
  const data = new FormData()

  for (const key in obj) {
    data.set(key, obj[key])
  }
  return data
}


/* import { objectToFormData } from '@/utils/form'


export function saveSceneRange(params) {
  return request({
    url: '/51world/scene/src/range/save',
    method: 'post',
    data: objectToFormData(params)
  })
} */
