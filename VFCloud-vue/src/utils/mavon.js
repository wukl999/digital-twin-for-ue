export function addTips(e, type) {
  if (type === 'success') {
    e.insertText(e.getTextareaDom(), {
      prefix: '::: success\n\n',
      subfix: '\n\n:::\n',
      str: 'success'
    });
  } else if (type === 'info') {
    e.insertText(e.getTextareaDom(), {
      prefix: '::: info\n\n',
      subfix: '\n\n:::\n',
      str: 'info'
    });
  } else if (type === 'warning') {
    e.insertText(e.getTextareaDom(), {
      prefix: '::: warning\n\n',
      subfix: '\n\n:::\n',
      str: 'warning'
    });
  } else if (type === 'danger') {
    e.insertText(e.getTextareaDom(), {
      prefix: '::: danger\n\n',
      subfix: '\n\n:::\n',
      str: 'danger'
    });
  } else {
    e.insertText(e.getTextareaDom(), {
      prefix: '::: success\n\n',
      subfix: '\n\n:::\n',
      str: 'success'
    });
  }
}
