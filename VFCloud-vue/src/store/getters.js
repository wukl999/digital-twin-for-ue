const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  id: state => state.user.id,
  isSubAccount: state => state.user.isSubAccount,
  username: state => state.user.username,
  name: state => state.user.name,
  isLogin: state => state.user.isLogin,
  roles: state => state.user.roles,
  permission_routes: state => state.permission.routes,
  errorLogs: state => state.errorLog.logs,
  sceneInfo: state => state.scene.sceneInfo,
  iframeShow:state => state.scene.iframeShow
}
export default getters
