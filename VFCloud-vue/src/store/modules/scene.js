const state = {
  sceneInfo:{
    resX: 0,
    resY: 0,
    sceneId: "",
    url: "",
    appId:"",
    handler:""
  },
  iframeShow:false
}

const mutations = {
  setSceneInfo:(state,param) => {
    // console.log("info",param)
    state.sceneInfo.resX = param.resX;
    state.sceneInfo.resY = param.resY;
    state.sceneInfo.sceneId = param.sceneId;
    state.sceneInfo.url = param.url;
    state.sceneInfo.appId = param.id;
    state.sceneInfo.handler = param.handler;
  },
  setIframeShow:(state,param) => {
    state.iframeShow = param
  }
}

export default {
  namespaced: true,
  state,
  mutations
}
