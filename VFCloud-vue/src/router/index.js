import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Layout from '@/layout'

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/system/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/system/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'dashboard',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  }
]


/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

function _import(componentPath) {
  return resolve => require([`@/views${componentPath}`], resolve)
}

function isBackendTrue(value) {
  return ['0', true].includes(value)
}


const hiddenRoutes = []

export function makeMenuRoutes(authorizedMenuList, parent = null) {
  const menuRoutes = []
  authorizedMenuList.forEach(menu => {
    const isHidden = !isBackendTrue(menu.visible)
    const _route={}
    if('0'==menu.parentId){
      _route.path= menu.path
      _route.component= Layout
      _route.name= menu.code
      _route.hidden= isHidden
      _route.meta= {
        title: menu.name,
        icon: menu.icon
      }

      if(menu.children && menu.children.length > 0){
        _route.redirect= menu.path+'/'+menu.children[0].path;
        _route.children=[];
        _route.children.push(...makeMenuRoutes(menu.children,_route))
      }else{
        const child= createChildren(menu)
        if(child){
           _route.redirect= menu.path+'/'+child.path;
        }
      }
    }else{


      _route.path= menu.path
      _route.hidden= isHidden

      if(isHidden){
        _route.component= _import(menu.component)
        _route.name= menu.code
        _route.meta= {
          title: menu.name,
          icon: menu.icon,
          noCache: true
          //activeMenu: '@/views/test/a'
        }
      }else{
          _route.name= menu.code
          _route.meta= {
            title: menu.name,
            icon: menu.icon
          }
          _route.component= _import(menu.component)
        if(menu.children && menu.children.length > 0){
          _route.redirect= parent.path+'/'+menu.path+'/'+menu.children[0].path;
          _route.children=[];
          _route.children.push(...makeMenuRoutes(menu.children,_route))
        }
      }
    }
    // if(!isHidden){
    //   menuRoutes.push(_route)
    // }else{
    //   _route.hidden= false
    //    constantRoutes.push(_route);
    // }

menuRoutes.push(_route)


    // if (_route.meta) {
    //   if (_catchedViews.includes(menu.code)) {
    //     _route.meta.keepAlive = true
    //   } else {
    //     _route.meta.keepAlive = false
    //   }
    // }
  })
  return menuRoutes
}


export function getChildren(childrens) {
  childrens.forEach(menu => {
    if(isBackendTrue(menu.visible)){
      return menu;
    }
  })
}


export function createChildren(menu) {
  console.log(menu)
    if(menu.component.lastIndexOf('/')!=-1){
      return {
        path: menu.component.substring(menu.component.lastIndexOf('/')+1,menu.component.length),
        component: _import(menu.component),
        name: menu.code,
        meta: { title: menu.name, icon: menu.icon}
      }
    }else{
      console.log(menu)
      console.log(menu.component);
    }
}

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export function makeMenuRoutesKeepAlive(routes) {
  const keepAliveRouteNames = ['Log', 'User', 'Department', 'SupplierAccountList']

  routes.forEach(route => {
    if (keepAliveRouteNames.includes(route.name)) {
      if (route.meta) {
        route.meta.keepAlive = true
      }
    }

    if (route.children && route.children.length > 0) {
      makeMenuRoutesKeepAlive(route.children)
    }
  })
}


export default router
