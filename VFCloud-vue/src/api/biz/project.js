import request from '@/utils/request'
import { objectToFormData} from '@/utils/form'


export function findProjectList(params) {
  return request({
    url: '/biz/project/list',
    method: 'get',
    params:params
  })
}

export function saveProject(params) {
  return request({
    url: '/biz/project/save',
    method: 'post',
    data: objectToFormData(params)
  })
}


export function deleteProject(params) {
  return request({
    url: '/biz/project/deleteById',
    method: 'get',
    params:params
  })
}

export function getThumbnail(id) {
  return request({
    url: '/biz/scene/thumbnail/id',
    method: 'get',
    responseType:'stream'
  })
}

export function getProjectTypes() {
  return request({
    url: '/biz/project/getTypes',
    method: 'get'
  })
}


export function shareProject(params) {
  return request({
    url: '/biz/project/share',
    method: 'get',
    params:params
  })
}

export function updateProjectShare(data) {
  return request({
    url: '/biz/project/updateShare',
    method: 'post',
    params:data
  })
}

export function previewProject(params) {
  return request({
    url: '/biz/project/preview',
    method: 'get',
    params:params
  })
}
export function debugProject(params) {
  return request({
    url: '/biz/project/debug',
    method: 'get',
    params:params
  })
}


