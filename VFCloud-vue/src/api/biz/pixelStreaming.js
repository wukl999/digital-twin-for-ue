import request from '@/utils/request'
import { objectToFormData} from '@/utils/form'


export function findPSList(params) {
  return request({
    url: '/biz/ps/list',
    method: 'get',
    params:params
  })
}
