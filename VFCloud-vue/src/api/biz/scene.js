import request from '@/utils/request'
import { objectToFormData} from '@/utils/form'


export function findSceneList(params) {
  return request({
    url: '/biz/scene/list',
    method: 'get',
    params:params
  })
}

export function saveScene(params) {
  return request({
    url: '/biz/scene/save',
    method: 'post',
    data: objectToFormData(params)
  })
}


export function deleteScene(params) {
  return request({
    url: '/biz/scene/deleteById',
    method: 'get',
    params:params
  })
}

export function getThumbnail(id) {
  return request({
    url: '/biz/scene/thumbnail/id',
    method: 'get',
    responseType:'stream'
  })
}

export function getSceneTypes() {
  return request({
    url: '/biz/scene/getTypes',
    method: 'get'
  })
}

export function shareScene(params) {
  return request({
    url: '/biz/scene/share',
    method: 'get',
    params:params
  })
}

export function updateSceneShare(data) {
  return request({
    url: '/biz/scene/updateShare',
    method: 'post',
    params:data
  })
}

export function getOrgOptions(data) {
  return request({
    url: '/sys/user/getOrgOptions',
    method: 'get',
    params:data
  })
}


export function findScenePerms(params) {
  return request({
    url: '/biz/scene/perms',
    method: 'get',
    params:params
  })
}

export function saveScenePerms(params) {
  return request({
    url: '/biz/scene/savePerms',
    method: 'post',
    data: objectToFormData(params)
  })
}

export function findMySceneList(params) {
  return request({
    url: '/biz/scene/my/list',
    method: 'get',
    params:params
  })
}


export function findSceneMarketList(params) {
  return request({
    url: '/biz/scene/market/list',
    method: 'get',
    params:params
  })
}

export function publishScene(params) {
  return request({
    url: '/biz/scene/publish',
    method: 'post',
    data: objectToFormData(params)
  })
}
