import request from '@/utils/request'
import { objectToFormData} from '@/utils/form'


export function getSettingsConfig() {
  return request({
    url: '/biz/settings/getConfig',
    method: 'get',
  })
}

export function saveSettingsConfig(data) {
  return request({
    url: '/biz/settings/saveConfig',
    method: 'post',
    params:data
  })
}
