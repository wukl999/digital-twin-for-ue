import request from '@/utils/request'
import { objectToFormData} from '@/utils/form'


export function findTerminalList(params) {
  return request({
    url: '/biz/terminal/list',
    method: 'get',
    params:params
  })
}

export function saveTerminal(params) {
  return request({
    url: '/biz/terminal/save',
    method: 'post',
    data: objectToFormData(params)
  })
}


export function deleteTerminal(params) {
  return request({
    url: '/biz/terminal/deleteById',
    method: 'get',
    params:params
  })
}

export function getRemoteInfo(params) {
  return request({
    url: '/biz/terminal/getRemoteInfo',
    method: 'get',
    params:params
  })
}


export function getTerminalGPUList(params) {
  return request({
    url: '/biz/terminal/gpu/list',
    method: 'get',
    params:params
  })
}

export function saveTerminalGPUConfig(params) {
  return request({
    url: '/biz/terminal/gpu/save',
    method: 'post',
    params:params
  })
}


export function findTerminalProjectList(params) {
  return request({
    url: '/biz/terminal/scene/list',
    method: 'get',
    params:params
  })
}

export function getPassInfo() {
  return request({
    url: '/biz/terminal/getPassInfo',
    method: 'get',
  })
}
