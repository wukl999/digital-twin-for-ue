import request from '@/utils/request'

export function loadDicts(data) {
  return request({
    url: '/sys/dict/list',
    method: 'get',
    params:data
  })
}

export function getSelectTree() {
  return request({
    url: '/sys/dict/getSelectTree',
    method: 'get',
  })
}

export function saveDict(data) {
  return request({
    url: '/sys/dict/save',
    method: 'post',
    params:data
  })
}

export function deleteDict(data) {
  return request({
    url: '/sys/dict/delete',
    method: 'get',
    params:data
  })
}

export function findDictDataList(data) {
  return request({
    url: '/sys/dict/findDataList',
    method: 'get',
    params:data
  })
}

export function saveDictData(data) {
  return request({
    url: '/sys/dict/saveData',
    method: 'post',
    params:data
  })
}


export function deleteDictData(data) {
  return request({
    url: '/sys/dict/deleteData',
    method: 'get',
    params:data
  })
}
