import request from '@/utils/request'
export function getAuthMenus(data) {
  return request({
    url: '/sys/menu/getByUserId',
    method: 'get',
    params:data
  })
}

export function loadMenus(data) {
  return request({
    url: '/sys/menu/findList',
    method: 'get',
    params:data
  })
}

export function getMenuOptionsTree() {
  return request({
    url: '/sys/menu/getOptionsTree',
    method: 'get',
  })
}

export function saveMenu(data) {
  return request({
    url: '/sys/menu/save',
    method: 'post',
    params:data
  })
}

export function deleteMenu(data) {
  return request({
    url: '/sys/menu/delete',
    method: 'get',
    params:data
  })
}
