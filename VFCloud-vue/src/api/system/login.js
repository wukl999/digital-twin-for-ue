import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/logout',
    method: 'get'
  })
}

export function updatePwd(data) {
  return request({
    url: '/updatePwd',
    method: 'post',
    data
  })
}


export function regUser(data) {
  return request({
    url: '/reg',
    method: 'post',
    data
  })
}

export function getCode(phone) {
  return request({
    url: '/code/'+phone,
    method: 'get'
  })
}
