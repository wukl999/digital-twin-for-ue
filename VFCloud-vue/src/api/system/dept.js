import request from '@/utils/request'

export function findDeptList(data) {
  return request({
    url: '/sys/dept/findList',
    method: 'get',
    params:data
  })
}

export function getDeptOptionsTree() {
  return request({
    url: '/sys/dept/getOptionsTree',
    method: 'get',
  })
}


export function saveDept(data) {
  return request({
    url: '/sys/dept/save',
    method: 'post',
    params:data
  })
}

export function deleteDept(data) {
  return request({
    url: '/sys/dept/delete',
    method: 'get',
    params:data
  })
}
