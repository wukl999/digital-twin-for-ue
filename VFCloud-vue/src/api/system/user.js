import request from '@/utils/request'
import { objectToFormData} from '@/utils/form'
export function getInfo() {
  return request({
    url: '/info',
    method: 'get',
  })
}

export function findUserList(params) {
  return request({
    url: '/sys/user/findList',
    method: 'get',
    params:params
  })
}

export function findSubList(params) {
  return request({
    url: '/sys/user/findSubList',
    method: 'get',
    params:params
  })
}

export function saveUser(params) {
  return request({
    url: '/sys/user/save',
    method: 'post',
    params:params
  })
}

export function deleteUser(params) {
  return request({
    url: '/sys/user/delete',
    method: 'get',
    params:params
  })
}

export function listRoleUser(params) {
  return request({
    url: '/sys/user/listForRole',
    method: 'get',
    params:params
  })
}

export function getUserDeptIdsById(params) {
  return request({
    url: '/sys/user/getDeptIdsById',
    method: 'get',
    params:params
  })
}

export function getUserRoleIdsById(params) {
  return request({
    url: '/sys/user/getRoleIdsById',
    method: 'get',
    params:params
  })
}


export function getOnlineUsers(params) {
  return request({
    url: '/auth/v1/findList',
    method: 'get',
    params:params
  })
}

export function deleteOnlineUser(params) {
  return request({
    url: '/auth/v1/delete',
    method: 'get',
    params:params
  })
}

export function saveSubUserBatch(params) {
  return request({
    url: '/sys/user/saveSubUserBatch',
    method: 'post',
    data:params
  })
}

export function updateUser(params) {
  return request({
    url: '/sys/user/update',
    method: 'post',
    params:params
  })
}

export function findByUsername(username) {
  return request({
    url: '/sys/user/findByUsername/'+username,
    method: 'get'
  })
}

export function updatePwd(data) {
  return request({
    url: '/sys/user/updatePwd',
    method: 'post',
    data
  })
}

export function uploadImage(params) {
  return request({
    url: '/sys/user/uploadImage',
    method: 'post',
    //data:data
    headers :{
       'Content-Type': 'multipart/form-data;',
    },
    data: objectToFormData(params)
  })
}

export function deleteImage(id) {
  return request({
    url: '/sys/user/deleteImage/'+id,
    method: 'get'
  })
}
