import request from '@/utils/request'

export function findRoleList(params) {
  return request({
    url: '/sys/role/findList',
    method: 'get',
    params:params
  })
}

export function saveRole(params) {
  return request({
    url: '/sys/role/save',
    method: 'post',
    params:params
  })
}

export function deleteRole(params) {
  return request({
    url: '/sys/role/deleteById',
    method: 'get',
    params:params
  })
}

export function saveRoleUsers(data) {
  return request({
    url: '/sys/role/saveRoleUsers',
    method: 'post',
    data
  })
}

export function getMenuIdsByRoleId(params) {
  return request({
    url: '/sys/role/getMenuIdsById',
    method: 'get',
    params:params
  })
}

export function saveRoleMenus(params) {
  return request({
    url: '/sys/role/saveMenus',
    method: 'post',
    params:params
  })
}

export function getRoleOptions(params) {
  return request({
    url: '/sys/role/getOptions',
    method: 'get',
    params:params
  })
}

export function findRoleUsers(params) {
  return request({
    url: '/sys/role/findUsers',
    method: 'get',
    params:params
  })
}
