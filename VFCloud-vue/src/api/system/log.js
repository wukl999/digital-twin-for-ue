import request from '@/utils/request'

export function findLogList(params) {
  return request({
    url: '/sys/log/findList',
    method: 'get',
    params:params
  })
}

export function getLogById(params) {
  return request({
    url: '/sys/log/getById',
    method: 'get',
    params:params
  })
}




