import request from '@/utils/request'

export function findGPUList(params) {
  return request({
    url: '/gpu/list',
    method: 'get',
    params:params
  })
}


export function deleteServer(params) {
  return request({
    url: '/server/deleteById',
    method: 'get',
    params:params
  })
}

export function initGPU(params) {
  return request({
    url: '/gpu/init',
    method: 'get',
    params:params
  })
}

export function saveGPU(params) {
  return request({
    url: '/gpu/save',
    method: 'post',
    params:params
  })
}


export function restartGPU(params) {
  return request({
    url: '/gpu/restart',
    method: 'get',
    params:params
  })
}

export function checkGPU(params) {
  return request({
    url: '/gpu/check',
    method: 'get',
    params:params
  })
}
