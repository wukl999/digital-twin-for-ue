import request from '@/utils/request'

export function getOrder(params) {
  return request({
    url: '/render/order',
    method: 'get',
    params:params
  })
}
