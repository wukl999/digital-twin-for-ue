import request from '@/utils/request'


export function findClientList(params) {
  return request({
    url: '/client/list',
    method: 'get',
    params:params
  })
}

export function saveClient(params) {
  return request({
    url: '/client/save',
    method: 'post',
    params:params
  })
}

export function deleteClient(params) {
  return request({
    url: '/client/deleteById',
    method: 'get',
    params:params
  })
}
