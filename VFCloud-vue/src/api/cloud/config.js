import request from '@/utils/request'

export function saveConfig(params) {
  return request({
    url: '/config/save',
    method: 'post',
    params:params
  })
}

export function getConfig(params) {
  return request({
    url: '/config/getConfig',
    method: 'get',
    params:params
  })
}

export function restartConfig(params) {
  return request({
    url: '/config/restart',
    method: 'get',
    params:params
  })
}
