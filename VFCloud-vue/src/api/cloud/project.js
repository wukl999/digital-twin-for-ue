import request from '@/utils/request'
import { objectToFormData} from '@/utils/form'


export function findProjectList(params) {
  return request({
    url: '/project/list',
    method: 'get',
    params:params
  })
}

export function saveProject(params) {
  return request({
    url: '/project/save',
    method: 'post',
    headers :{
       'Content-Type': 'multipart/form-data;',
    },
    data: objectToFormData(params)
  })
}


export function deleteProject(params) {
  return request({
    url: '/project/deleteById',
    method: 'get',
    params:params
  })
}

export function getThumbnail(id) {
  return request({
    url: '/project/thumbnail/id',
    method: 'get',
    responseType:'stream'
  })
}


export function getProjectTypes() {
  return request({
    url: '/project/getTypes',
    method: 'get'
  })
}
