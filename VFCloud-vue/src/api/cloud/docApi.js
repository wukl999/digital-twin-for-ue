import request from '@/utils/request'
import { objectToFormData} from '@/utils/form'

export function loadAPIs(data) {
  return request({
    url: '/API/getAllTree',
    method: 'get',
    params:data
  })
}

export function loadList(data) {
  return request({
    url: '/API/list',
    method: 'get',
    params:data
  })
}

export function loadSubList(data) {
  return request({
    url: '/API/subList',
    method: 'get',
    params:data
  })
}

export function saveAPI(data) {
  return request({
    url: '/API/save',
    method: 'post',
    data:data
  })
}

export function deleteAPI(data) {
  return request({
    url: '/API/delete',
    method: 'get',
    params:data
  })
}

export function deleteFolders(data) {
  return request({
    url: '/API/deleteFolders',
    method: 'get',
    params:data
  })
}

export function loadFolders(data) {
  return request({
    url: '/API/folderList',
    method: 'get',
    params:data
  })
}

export function uploadImage(params) {
  return request({
    url: '/API/uploadImage',
    method: 'post',
    //data:data
    headers :{
       'Content-Type': 'multipart/form-data;',
    },
    data: objectToFormData(params)
  })
}

export function deleteImages(data) {
  return request({
    url: '/API/deleteImages',
    method: 'get',
    params:data
  })
}

export function getImages(id) {
  return request({
    url: '/project/thumbnail/'+id,
    method: 'get',
    responseType:'stream'
  })
}
