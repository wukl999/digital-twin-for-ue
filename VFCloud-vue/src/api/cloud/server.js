import request from '@/utils/request'


export function findServerList(params) {
  return request({
    url: '/server/list',
    method: 'get',
    params:params
  })
}

export function saveServer(params) {
  return request({
    url: '/server/save',
    method: 'post',
    params:params
  })
}

export function deleteServer(params) {
  return request({
    url: '/server/deleteById',
    method: 'get',
    params:params
  })
}

export function getServerById(params) {
  return request({
    url: '/server/getById',
    method: 'get',
    params:params
  })
}

export function createSignalingServer(params) {
  return request({
    url: '/server/createSignalingServer',
    method: 'get',
    params:params
  })
}

export function createRelayServer(params) {
  return request({
    url: '/server/createRelayServer',
    method: 'get',
    params:params
  })
}


export function restartServer(params) {
  return request({
    url: '/server/restartServer',
    method: 'get',
    params:params
  })
}

export function stopServer(params) {
  return request({
    url: '/server/stop',
    method: 'get',
    params:params
  })
}

export function restartSignaling(params) {
  return request({
    url: '/server/restartSignaling',
    method: 'get',
    params:params
  })
}

export function restartRelay(params) {
  return request({
    url: '/server/restartRelay',
    method: 'get',
    params:params
  })
}

export function restartGPU(params) {
  return request({
    url: '/server/restartGPU',
    method: 'get',
    params:params
  })
}
